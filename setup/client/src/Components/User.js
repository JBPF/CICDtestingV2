// // UserRoleDetailsModal.js

// import React, { useEffect, useState } from 'react';
// import Modal from 'react-modal';
// import { Button } from '@mui/material';
// import { fetchUsers, fetchRoleById } from '../services/userService';
// import EditIcon from '@mui/icons-material/Edit';
// import UserRoleAssignmentForm from '/roleAssingnmentForm';

// let userRolesData = [];
// const [assignModalIsOpen, setAssignModalIsOpen] = useState(false);
// const openAssignModal = (role) => {
//   setSelectedRole(role);
//   setAssignModalIsOpen(true);
// };
// const closeAssignModal = () => {
//   setSelectedRole(null);
//   setAssignModalIsOpen(false);
// };

// export const initializeUserRoles = async () => {
//   try {
//     userRolesData = await fetchUserRoles();
//   } catch (error) {
//     // Handle the error if necessary
//     console.error('Failed to initialize user roles:', error);
//   }
// };

// export const fetchUserRoleById = (roleId) => {
//   const userRole = userRolesData.find((role) => role._id === roleId);

//   if (userRole) {
//     return userRole;
//   } else {
//     throw new Error(`User role with ID ${roleId} not found.`);
//   }
// };

// const UserRoleModal = ({ isOpen, handleClose, selectedUser }) => {
//   const [userRolesDetails, setUserRolesDetails] = useState([]);

//   useEffect(() => {
//     const fetchDetails = async () => {
//       const rolesDetails = await Promise.all(
//         selectedUser.userRoles.map(async (roleId) => {
//           try {
//             const userRoleDetails = await fetchUserRoleById(roleId);
//             const roleDetails = await fetchRoleById(userRoleDetails.role);

//             return roleDetails.name;
//           } catch (error) {
//             console.error(`Error fetching role details: ${error.message}`);
//             return null;
//           }
//         })
//       );

//       setUserRolesDetails(rolesDetails.filter((role) => role !== null));
//     };

//     if (isOpen) {
//       fetchDetails();
//     }
//   }, [isOpen, selectedUser.userRoles]);

//   return (
//     <Modal isOpen={isOpen} onRequestClose={handleClose} contentLabel="User Role Details">
//       <div>
//         <h2>User Role Details</h2>
//         <h3>User: {selectedUser.email}</h3>
//         <h4>Roles: </h4>
//         <ul>
//           {userRolesDetails.map((role, index) => (
//             <li key={index}>{role}</li>
//           ))}
//         </ul>
//         <Button variant="contained" color="primary" onClick={handleClose}>
//           Close
//         </Button>
//         <button className="CreateButtonAdmin" onClick={() => openAssignModal(role)}>
//                         Assign new role                           
//                          <EditIcon />
//           </button>
        
//       </div>
//       {selectedRole && (
//                 <Modal isOpen={assignModalIsOpen} onRequestClose={closeAssignModal} contentLabel="Assign Role">
//                     <UserRoleAssignmentForm roleData={selectedRole} closeModal={closeAssignModal} />
//                 </Modal>
//             )}
//     </Modal>
//   );
// };

// export default UserRoleModal;
