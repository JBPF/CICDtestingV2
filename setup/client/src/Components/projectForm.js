    import React, { useState, useEffect } from 'react';
    import { Form, Input, Button, Steps, Select, message, Upload, DatePicker, List,InputNumber } from 'antd';
    import { InboxOutlined, PlusOutlined, MinusCircleOutlined } from '@ant-design/icons';
    import { fetchRoles } from '../services/roleService'; // Adjust the path as needed
    import axios from 'axios';

    const { Step } = Steps;
    const { Option } = Select;
    const { TextArea } = Input;
    const Dragger = Upload.Dragger;

    const initialProjectDataState = {
        title: '',
        description: '',
        collaborators: [],
        roles: [],
        articles: [],
        inclusionCriteria: [{id:1,value:''}],
        exclusionCriteria: [{id:1,value:''}],
        extractionVariables: [{id:1,value:''}],
        reviewersPerArticle: 2,
        acceptanceRate: 2,
        reviewStageConfig: 'three-stage',
    };

    const ProjectForm = () => {
        const [currentStep, setCurrentStep] = useState(0);
        const [projectData, setProjectData] = useState(initialProjectDataState);
        const [users, setUsers] = useState([]);
        const [roles, setRoles] = useState([]);
        const [usersMap, setUsersMap] = useState({});
        const [loading, setLoading] = useState(false);
        const [form] = Form.useForm();
        const [studies, setStudies] = useState([]);
        const [projects, setProjects] = useState([]);

        const handleCriteriaChange = (criteriaType, id, value) => {
            const updatedCriteria = projectData[criteriaType].map(criterion => {
                if (criterion.id === id) {
                    return { ...criterion, value };
                }
                return criterion;
            });
            setProjectData({ ...projectData, [criteriaType]: updatedCriteria });
        };

        const addNewCriterion = (criteriaType) => {
            const newId = Math.max(...projectData[criteriaType].map(c => c.id)) + 1;
            const newCriterion = { id: newId, value: '' };
            setProjectData({ ...projectData, [criteriaType]: [...projectData[criteriaType], newCriterion] });
        };

        const removeCriterion = (criteriaType, id) => {
            const updatedCriteria = projectData[criteriaType].filter(criterion => criterion.id !== id);
            setProjectData({ ...projectData, [criteriaType]: updatedCriteria });
        }



        useEffect(() => {
            const fetchEmployeesAndRoles = async () => {
                try {
                    const [usersResponse, rolesResponse] = await Promise.all([
                        axios.get('http://localhost:3000/users/users'),
                        fetchRoles()
                    ]);

                    const userOptions = usersResponse.data.map(user => ({
                        value: user._id,
                        label: user.name
                    }));

                    setUsers(userOptions);
                    setUsersMap(usersResponse.data.reduce((map, user) => ({...map, [user._id]: user.name}), {}));
                    setRoles(rolesResponse);
                } catch (error) {
                    console.error('Error fetching data:', error);
                }
            };

            fetchEmployeesAndRoles();
        }, []);

        useEffect(() => {
            const fetchStudiesAndProjects = async () => {
                try {
                    const studiesResponse = await axios.get('http://localhost:3000/api/projects/:projectId/articles');
                    setStudies(studiesResponse.data);

                    const projectsResponse = await axios.get('http://localhost:3000/api/projects/display');
                    setProjects(projectsResponse.data);
                } catch (error) {
                    console.error('Error fetching studies and projects:', error);
                }
            };

            fetchStudiesAndProjects();
        }, []);



        const handleChange = (key, value) => {
            setProjectData({ ...projectData, [key]: value });
        };

        const handleFileChange = info => {
            let fileList = [...info.fileList];
            handleChange('articles', fileList);
        };

        const handleSubmit = async () => {
            setLoading(true);

            try {
                const projectResponse = await axios.post('http://localhost:3000/api/projects', {
                    title: projectData.title,
                    description: projectData.description,
                    startDate: projectData.startDate,
                    collaborators: projectData.collaborators,
                    inclusionCriteria: projectData.inclusionCriteria,
                    exclusionCriteria: projectData.exclusionCriteria,
                    extractionVariables: projectData.extractionVariables,
                    reviewersPerArticle: projectData.reviewersPerArticle,
                    acceptanceRate: projectData.acceptanceRate,
                    reviewStageConfig: projectData.reviewStageConfig,
                });


                const formData = new FormData();
                formData.append('pubmedIds', projectData.pubmedIds);
                projectData.articles.forEach(file => {
                    formData.append('articles', file.originFileObj);
                });

                await Promise.all(projectData.roles.map(async (role) => {
                    await axios.post(`http://localhost:3000/api/assignRole`, {
                        userId: role.userId,
                        roleId: role.roleId,
                        projectId: projectResponse.data.project._id,
                    });
                }));

                await axios.post(`http://localhost:3000/api/projects/${projectResponse.data.project._id}/articles`, formData, {
                    headers: { 'Content-Type': 'multipart/form-data' },
                });



                message.success('Project created successfully!');
                setProjectData(initialProjectDataState);
                setCurrentStep(0);
            } catch (error) {
                console.error('Error creating project:', error);
                message.error('Failed to create project. Please try again.');
            } finally {
                setLoading(false);
            }
        };
        const steps = [
            {
                title: 'Project Details',
                content: (
                    <div>
                        <Form.Item label="Title" name="title" rules={[{ required: true, message: 'Please input the project title!' }]}>
                            <Input value={projectData.title} onChange={e => handleChange('title', e.target.value)} />
                        </Form.Item>
                        <Form.Item label="Description" name="description" rules={[{ required: true, message: 'Please input the project description!' }]}>
                            <TextArea value={projectData.description} onChange={e => handleChange('description', e.target.value)} />
                        </Form.Item>
                        <Form.Item label="Start Date" name="startDate" rules={[{ required: true, message: 'Please select the start date!' }]}>
                            <DatePicker onChange={date => handleChange('startDate', date)} />
                        </Form.Item>
                    </div>
                ),
            },


            {
                title: 'Inclusion/Exclusion Criteria',
                content: (
                    <div>
                        <h4>Inclusion Criteria</h4>
                        {projectData.inclusionCriteria.map((criterion, index) => (
                            <div key={criterion.id} style={{ marginBottom: 8 }}>
                                <Input
                                    placeholder={`Inclusion Criterion #${index + 1}`}
                                    value={criterion.value}
                                    onChange={(e) => handleCriteriaChange('inclusionCriteria', criterion.id, e.target.value)}
                                    style={{ width: '80%', marginRight: 8 }}
                                />
                                <MinusCircleOutlined onClick={() => removeCriterion('inclusionCriteria', criterion.id)} />
                            </div>
                        ))}
                        <Button type="dashed" onClick={() => addNewCriterion('inclusionCriteria')} style={{ width: '80%' }}>
                            <PlusOutlined /> Add Inclusion Criterion
                        </Button>

                        <h4 style={{ marginTop: 16 }}>Exclusion Criteria</h4>
                        {projectData.exclusionCriteria.map((criterion, index) => (
                            <div key={criterion.id} style={{ marginBottom: 8 }}>
                                <Input
                                    placeholder={`Exclusion Criterion #${index + 1}`}
                                    value={criterion.value}
                                    onChange={(e) => handleCriteriaChange('exclusionCriteria', criterion.id, e.target.value)}
                                    style={{ width: '80%', marginRight: 8 }}
                                />
                                <MinusCircleOutlined onClick={() => removeCriterion('exclusionCriteria', criterion.id)} />
                            </div>
                        ))}
                        <Button type="dashed" onClick={() => addNewCriterion('exclusionCriteria')} style={{ width: '80%' }}>
                            <PlusOutlined /> Add Exclusion Criterion
                        </Button>
                    </div>
                ),
            },

            {
                title: 'Extraction Variables',
                content: (
                    <div>
                        <h4>Extraction Variables</h4>
                        {projectData.extractionVariables.map((criterion, index) => (
                            <div key={criterion.id} style={{ marginBottom: 8 }}>
                                <Input
                                    placeholder={`Extraction variable #${index + 1}`}
                                    value={criterion.value}
                                    onChange={(e) => handleCriteriaChange('extractionVariables', criterion.id, e.target.value)}
                                    style={{ width: '80%', marginRight: 8 }}
                                />
                                <MinusCircleOutlined onClick={() => removeCriterion('extractionVariables', criterion.id)} />
                            </div>
                        ))}
                        <Button type="dashed" onClick={() => addNewCriterion('extractionVariables')} style={{ width: '80%' }}>
                            <PlusOutlined /> Add Extraction Variable
                        </Button>
                    </div>
                ),
            },




            {
                title: 'Collaborators',
                content: (
                    <div>
                        <Form.Item label="Collaborators" name="collaborators" rules={[{ required: true, message: 'Please select at least two collaborators!' }]}>
                            <Select
                                mode="multiple"
                                placeholder="Select collaborators"
                                value={projectData.collaborators}
                                onChange={value => handleChange('collaborators', value)}
                            >
                                {users.map(user => (
                                    <Option key={user.value} value={user.value}>{user.label}</Option>
                                ))}
                            </Select>
                        </Form.Item>
                        {projectData.collaborators.map((userId, index) => (
                            <Form.Item
                                label={`Role for ${usersMap[userId]}`}
                                name={`role-${userId}`}
                                rules={[{ required: true, message: 'Please select a role!' }]}
                            >
                                <Select
                                    placeholder="Select role"
                                    onChange={value => {
                                        const newRoles = [...projectData.roles];
                                        newRoles[index] = { userId, roleId: value };
                                        handleChange('roles', newRoles);
                                    }}
                                >
                                    {roles.map(role => (
                                        <Option key={role._id} value={role._id}>{role.name}</Option>
                                    ))}
                                </Select>
                            </Form.Item>

                        ))}
                    </div>
                ),
            },

            {
                title: 'Review Configuration',
                content: (
                    <div>
                        <Form.Item label="Review Stage Configuration" name="reviewStageConfig" rules={[{ required: true, message: 'Please select a review stage configuration!' }]}>
                            <Select defaultValue="three-stage" onChange={value => handleChange('reviewStageConfig', value)}>
                                <Option value="three-stage">Three-Stage Review</Option>
                                <Option value="two-stage">Two-Stage Review</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item label="Reviewers Per Article" name="reviewersPerArticle" rules={[{ required: true, message: 'Please input the number of reviewers per article!' }]}>
                            <InputNumber min={1} defaultValue={2} onChange={value => handleChange('reviewersPerArticle', value)} />
                        </Form.Item>
                        <Form.Item label="Review Threshold Percentage" name="acceptanceRate" rules={[{ required: true, message: 'Please input the review threshold percentage!' }]}>
                            <InputNumber min={2} defaultValue={2} onChange={value => handleChange('acceptanceRate', value)} />
                        </Form.Item>
                    </div>
                ),
            },


            {
                title: 'Upload articles',
                content: (
                    <div>
                        <Form.Item label="PubMed IDs" name="pubmedIds">
                            <Input placeholder="Enter PubMed IDs separated by commas" value={projectData.pubmedIds} onChange={e => handleChange('pubmedIds', e.target.value)} />
                        </Form.Item>
                        <Dragger
                            name="articles"
                            multiple
                            onChange={handleFileChange}
                            beforeUpload={() => false} // Prevent file upload here
                        >
                            <p className="ant-upload-drag-icon">
                                <InboxOutlined />
                            </p>
                            <p className="ant-upload-text">Click or drag file to this area to upload</p>
                            <p className="ant-upload-hint">Support for a single or bulk upload.</p>
                        </Dragger>
                    </div>
                ),
            },

            {
                title: 'Confirmation',
                content: (
                    <div>
                        <h3>Project Title: {projectData.title}</h3>
                        <h3>Project Description:</h3>
                        <p>{projectData.description}</p>
                        <h3>Collaborators:</h3>
                        {projectData.collaborators.map(collaborator => (
                            <p key={collaborator}>{users.find(option => option.value === collaborator)?.label}</p>
                        ))}
                        <h3>Roles:</h3>
                        {projectData.roles.map(role => (
                            <p key={role.roleId}>{`${roles.find(option => option.value === role.roleId)?.label} assigned to ${users.find(option => option.value === role.userId)?.label}`}</p>
                        ))}
                    </div>
                ),
            },
        ];


        return (
            <Form form={form} onFinish={handleSubmit} layout="vertical">
                <Steps current={currentStep}>
                    {steps.map(item => (
                        <Step key={item.title} title={item.title} />
                    ))}
                </Steps>
                <div className="steps-content" style={{ marginTop: '24px' }}>
                    {steps[currentStep].content}
                </div>
                <div className="steps-action" style={{ marginTop: '24px' }}>
                    {currentStep < steps.length - 1 && (
                        <Button type="primary" onClick={() => setCurrentStep(currentStep + 1)} disabled={loading}>
                            Next
                        </Button>
                    )}
                    {currentStep === steps.length - 1 && (
                        <Button type="primary" onClick={handleSubmit} disabled={loading}>
                            Confirm Project
                        </Button>
                    )}
                    {currentStep > 0 && (
                        <Button style={{ margin: '0 8px' }} onClick={() => setCurrentStep(currentStep - 1)} disabled={loading}>
                            Previous
                        </Button>
                    )}
                </div>
            </Form>
        );
    }

    export default ProjectForm;