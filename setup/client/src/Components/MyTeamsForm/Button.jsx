import React from 'react'
import { Button as AntdButton } from 'antd'

const Button = ({ children, style, ...args }) => {
    return (
        <AntdButton style={{
            backgroundColor: 'rgb(23, 23, 30)',
            ...style
        }} {...args} >
            {children}
        </AntdButton>
    )
}

export default Button