import React, { useEffect, useState } from 'react'
import { Modal, Form, Input, Select, Button, Typography, message } from 'antd'
import { Box, Paper } from '@mui/material';
import { grey } from '@mui/material/colors';
import { updateTeam, addMember, deleteTeam, removeMember } from '../../services/teamsService';


const { Option } = Select;


const EditTeamModal = ({ isOpen, onClose, data: {
    users,
    roles,
    selectedTeam
} }) => {

    const [form] = Form.useForm();
    const [isAddMemberModalVisible, setIsAddMemberModalVisible] = useState(false);
    const [newMember, setNewMember] = useState({ userId: '', roleId: '' });
    const [selectedUsers, setSelectedUsers] = useState([]); // State to keep track of selected users and their roles

    const showAddMemberModal = () => {
        setIsAddMemberModalVisible(true);
    };

const handleAddMember = async () => {
    if (!newMember.userId || !newMember.roleId) {
        message.error("User and role must be selected");
        return;
    }

    const isAlreadyMember = selectedUsers.some(member => member.userId === newMember.userId);
    if (isAlreadyMember) {
        message.error('This user is already in the team.');
        return;
    }

    try {
        await addMember(selectedTeam._id, {
            userId: newMember.userId,
            roleId: newMember.roleId,
        })
        .then((data) => {

        // Update the local state to reflect the addition
        setSelectedUsers(prev => [...prev, {
            userId: newMember.userId,
            roleId: newMember.roleId,
            userRole: {
                _id: newMember.roleId,
                name: roles.find(role => role._id === newMember.roleId).name
            }
        }]);
      });
    } catch (error) {
        console.error('Error adding member:', error);
        message.error(error.response?.data?.message || 'Failed to add the member');
    }
};

    
const handleDeleteMember = async (userId) => {
    try {
        const data = await removeMember(selectedTeam._id, userId);
        message.success(data.message || 'Member removed successfully');
        const updatedSelectedUsers = selectedUsers.filter(user => user.userId !== userId);
        setSelectedUsers(updatedSelectedUsers);
    } catch (error) {
        console.error('Error removing member:', error);
        message.error(error.response?.data?.message || 'Failed to remove the member');
    }
};


    const handleDeleteTeam = async () => {
        try {
            await deleteTeam(selectedTeam._id);
            message.success('Team deleted successfully.');
            onClose(); // Close the modal and refresh or navigate away
        } catch (error) {
            console.error('Failed to delete team:', error);
            message.error('Failed to delete the team.');
        }
    };

    

    
    
    



    // Function to handle user selection and initiate their role to null
    const handleUserSelection = (selectedUserIds) => {
        const newSelectedUsers = selectedUserIds.map(id => {
            const existingUser = selectedUsers.find(user => user.userId === id);
            return existingUser || { userId: id, roleId: null };
        });
        setSelectedUsers(newSelectedUsers);
    };


    // Function to update the role for a selected user
    const handleRoleSelection = (userId, roleId) => {
        const updatedSelectedUsers = selectedUsers.map(user =>
            user.userId._id === userId ? {
                ...user, userRole: {
                    _id: roleId,
                    name: roles.find(role => role._id === roleId).name
                }
            } : user
        );
        setSelectedUsers(updatedSelectedUsers);
    };


    useEffect(() => {
        form.setFieldsValue({
            name: selectedTeam?.name,
            description: selectedTeam?.description
        });
        setSelectedUsers(selectedTeam?.members || []);
    }, [selectedTeam]);

    const onFinish = async () => {
        const values = form.getFieldsValue();
        const teamData = {
            name: values.name,
            description: values.description,
            members: selectedUsers,
        };
        try {
            await updateTeam(selectedTeam._id, teamData); // Assuming the team ID is part of selectedTeam
            onClose(); // Close the modal upon successful update
        } catch (error) {
            console.error('Failed to update team:', error);
            // Optionally, handle the error (e.g., show a notification)
        }
    };



    return (

        <>
        <Modal
            title="Edit Team"
            visible={isOpen}
            onCancel={() => onClose()}
            footer={null}>
            <Form form={form} layout="vertical" onFinish={onFinish} autoComplete="off">
                {/* Existing form fields */}
                <Form.Item >
                    <Box gap={"15px"} mt={"15px"} width={"100%"} display={"flex"} justifyContent={"flex-end"}>
                    <Button 
    danger
>
    Delete Team
</Button>
                        <Button type="primary" htmlType="submit">Save</Button>
                    </Box>
                </Form.Item>
            </Form>
        </Modal>
        {/* Rest of your component */}
   
        <Modal
            title="Edit Team"
            visible={isOpen}
            maxHeight={700}
            onCancel={() => onClose()}
            footer={null}>
            <Form form={form} layout="vertical" onFinish={onFinish} autoComplete="off">

                <Form.Item
                    name="name"
                    label="Team Name"
                    rules={[{ required: true, message: 'Please input the team name!' }]}>
                    <Input />
                </Form.Item>
                <Form.Item
                    name="description"
                    label="Team Description"
                    rules={[{ required: true, message: 'Please input the team description!' }]}>
                    <Input.TextArea />
                </Form.Item>
                <Box display={"flex"}
                    justifyContent={"space-between"}
                >
                    <Typography.Title level={4}>Team Members </Typography.Title>
                    <Button type='primary' style={{ width: "120px" }} size="small" onClick={showAddMemberModal}>+ Add Member</Button>

                </Box>




                <Box my={"20px"} display={"flex"} width={"100%"} flexDirection={"column"} gap={"10px"}>
                {selectedUsers.map((user, index) => (
    <UserCard key={index} user={user} handleRoleSelection={handleRoleSelection} roles={roles} handleDeleteMember={() => handleDeleteMember(user.userId)} handleDeleteTeam={handleDeleteTeam} />
))}

                </Box>

                <Box gap={"15px"} mt={"15px"} width={"100%"} display={"flex"} justifyContent={"flex-end"}>
    <Button style={{ width: "100px" }} onClick={handleDeleteTeam}>Delete Team</Button>
    {/* Other buttons */}
</Box>
                <Form.Item >

                    <Box gap={"15px"} mt={"15px"} width={"100%"} display={"flex"} justifyContent={"flex-end"}>
                        <Button style={{
                            width: "100px"
                        }} type="primary" htmlType="submit">Save</Button>
                    </Box>
                </Form.Item>
            </Form>
        </Modal>

        <Modal
    title="Add New Member"
    visible={isAddMemberModalVisible}
    onCancel={() => setIsAddMemberModalVisible(false)}
    footer={[
        <Button key="back" onClick={() => setIsAddMemberModalVisible(false)}>
            Cancel
        </Button>,
        <Button key="submit" type="primary" onClick={handleAddMember}>
            Add Member
        </Button>,
    ]}
>
    <Form layout="vertical">
        <Form.Item label="Select User">
            <Select
                value={newMember.userId}
                onChange={userId => setNewMember(prev => ({ ...prev, userId }))}
            >
                {users.map(user => (
                    <Option key={user._id} value={user._id}>{user.name}</Option>
                ))}
            </Select>
        </Form.Item>
        <Form.Item label="Assign Role">
            <Select
                value={newMember.roleId}
                onChange={roleId => setNewMember(prev => ({ ...prev, roleId }))}
            >
                {roles.map(role => (
                    <Option key={role._id} value={role._id}>{role.name}</Option>
                ))}
            </Select>
        </Form.Item>
    </Form>
</Modal>

</>

        
    )
}


const UserCard = ({ user, handleRoleSelection, roles, handleDeleteMember }) => {

    const [isEditing, setIsEditing] = useState(false);

    const [selectedRoleId, setSelectedRoleId] = useState(user.userRole._id);


    const handleSaveRole = () => {
        handleRoleSelection(user.userId._id, selectedRoleId)
        setIsEditing(false);
    }

    return (
        <Paper
          key={user.userId}
          display={"flex"}
          width={"100%"}
          flexDirection={"column"}
          gap={"10px"}
          sx={{
            padding: "30px",
            display: "flex",
            flexDirection: "column",
            gap: "10px",
            backgroundColor: "white",
            borderRadius: "5px",
            boxShadow: "0 0 5px rgba(0, 0, 0, 0.1)",
          }}
        >
          <Box
            width={"100%"}
            display={"flex"}
            justifyContent={"center"}
            alignItems={"center"}
          >
            <Box
              display={"flex"}
              flexDirection={"column"}
              width={"100%"}
              gap={"0"}
            >
              <Typography.Title
                sx={{
                  color: grey[900], 
                  margin: "10px 0", 
                  fontSize: "20px",
                }}
                level={4}
              >
                {user?.userId?.name}
              </Typography.Title>
              <Typography
                sx={{
                  color: grey[700], 
                  margin: "0",
                  fontSize: "16px",
                }}
              >
                {user?.userRole?.name}
              </Typography>
            </Box>
            {isEditing ? (
              <Box display={"flex"} gap={"10px"}>
                <Button
                  style={{
                    width: "80px",
                  }}
                  onClick={() => handleSaveRole()}
                  type="primary"
                >
                  Save
                </Button>
                <Button
                  style={{
                    width: "80px",
                  }}
                  onClick={() => setIsEditing(false)}
                >
                  Cancel
                </Button>
              </Box>
            ) : (
              <Box style={{ padding: '20px', marginRight: '20px'}} display={"flex"} gap={"10px"}>
                <Button
                  style={{
                    width: "80px",
                  }}
                  onClick={() => setIsEditing(true)}
                  type="primary"
                >
                  Edit
                </Button>
                <Button
                  style={{ width: "80px" }}
                  onClick={() => handleDeleteMember(user.userId)}
                  danger
                >
                  Delete
                </Button>
              </Box>
            )}
          </Box>
          {!isEditing ? null : (
            <Select
              placeholder="Select a role"
              onChange={(roleId) => setSelectedRoleId(roleId)}
              value={selectedRoleId}
            >
              {roles?.map((role) => (
                <Option key={role._id} value={role._id}>
                  {role.name}
                </Option>
              ))}
            </Select>
          )}
        </Paper>
      );
    }


export default EditTeamModal


