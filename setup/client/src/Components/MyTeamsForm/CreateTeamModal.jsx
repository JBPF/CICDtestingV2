import React, { useState, useEffect } from 'react'
import { Modal, Form, Input, Select, Button, message } from 'antd'
import { createTeam } from '../../services/teamsService';
import { Box } from '@mui/material';

const { Option } = Select;


const CreateTeamModal = ({ isOpen, onClose, data }) => {

    const [form] = Form.useForm();



    const [selectedUsers, setSelectedUsers] = useState([]); // State to keep track of selected users and their roles

    // Function to handle user selection and initiate their role to null
    const handleUserSelection = (selectedUserIds) => {
        const newSelectedUsers = selectedUserIds.map(id => {
            const existingUser = selectedUsers.find(user => user.userId === id);
            return existingUser || { userId: id, roleId: null };
        });
        setSelectedUsers(newSelectedUsers);
    };


    // Function to update the role for a selected user
    const handleRoleSelection = (userId, roleId) => {
        const updatedSelectedUsers = selectedUsers.map(user =>
            user.userId === userId ? { ...user, roleId: roleId } : user
        );
        setSelectedUsers(updatedSelectedUsers);
    };


    // Function to handle team creation
    const handleCreateTeam = async (values) => {
        const teamMembers = selectedUsers.map(user => ({ userId: user.userId, roleId: user.roleId }));
        try {
            console.log("Creating team with data:", { name: values.name, description: values.description, members: teamMembers });

            await createTeam({
                name: values.name,
                description: values.description,
                members: teamMembers,
            });
            message.success('Team created successfully!');
            form.resetFields();
            onClose();
            setSelectedUsers([]); // Reset selected users and their roles
        } catch (error) {
            console.error('Failed to create team:', error);
            message.error(`Failed to create team: ${error.message}`);
        }
    };

    return (
        <Modal
            title="Create New Team"
            visible={isOpen}
            onCancel={() => onClose()}
            footer={null}>
            <Form form={form} layout="vertical" onFinish={handleCreateTeam} autoComplete="off">
                <Form.Item
                    name="name"
                    label="Team Name"
                    rules={[{ required: true, message: 'Please input the team name!' }]}>
                    <Input />
                </Form.Item>
                <Form.Item
                    name="description"
                    label="Team Description"
                    rules={[{ required: true, message: 'Please input the team description!' }]}>
                    <Input.TextArea />
                </Form.Item>
                <Form.Item
                    label="Select Team Members and Assign Roles"
                    rules={[{ required: true, message: 'Please select team members and assign roles!' }]}>
                    <Select
                        mode="multiple"
                        placeholder="Select team members"
                        onChange={handleUserSelection}
                        value={selectedUsers.map(user => user.userId)}>
                        {data["users"].map(user => (
                            <Option key={user._id} value={user._id}>{user.name}</Option>
                        ))}
                    </Select>
                </Form.Item>
                {selectedUsers.map((user, index) => (
                    <Form.Item
                        key={user.userId}
                        label={`Role for ${data["users"].find(u => u._id === user.userId)?.name || 'User'}`}>
                        <Select
                            placeholder="Select a role"
                            onChange={(roleId) => handleRoleSelection(user.userId, roleId)}
                            value={user.roleId}>
                            {data["roles"].map(role => (
                                <Option key={role._id} value={role._id}>{role.name}</Option>
                            ))}
                        </Select>
                    </Form.Item>
                ))}
                <Form.Item>
                    <Box gap={"15px"} mt={"15px"} width={"100%"} display={"flex"} justifyContent={"flex-end"}>
                        <Button style={{
                            width: "100px"
                        }} type="primary" htmlType="submit">Create</Button>
                    </Box>
                </Form.Item>
            </Form>
        </Modal>
    )
}

export default CreateTeamModal