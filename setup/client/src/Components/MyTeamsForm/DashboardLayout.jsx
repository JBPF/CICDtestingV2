import { Box } from '@mui/material'
import React from 'react'
import Sidebar from '../Sidebar'

export const DashboardLayout = ({ children }) => {
    return (
        <Box display={"flex"} flexDirection={"row"}>
            <Sidebar />
            <Box pl={"250px"} width={"100%"}>

            {
                children
            }
            </Box>

        </Box>
    )
}
