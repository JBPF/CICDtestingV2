import { Box,  Typography } from '@mui/material'
import { Button } from 'antd'
import React from 'react'

const PageHeader = ({ title, onButtonClick }) => {
    return (
        <Box display="flex" width={"100%"} justifyContent="space-between" alignItems="center" py={2}>
            <Typography variant="h4">{title}</Typography>
            <Box display={"flex"} ml={"auto"}>
                {onButtonClick && <Button style={{
                    fontWeight: 500,
                    marginLeft: '500px' // Add this line
                }} onClick={onButtonClick} ml={"auto"} type={"primary"}>+ Create Team</Button>}
            </Box>
        </Box>
    )
}

export default PageHeader