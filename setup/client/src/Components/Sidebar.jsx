import React from "react";
import { NavLink, useLocation, useNavigate } from "react-router-dom";
import { SidebarData } from "./SidebarData";
import LogoutIcon from '@mui/icons-material/Logout'; 
import LoginIcon from '@mui/icons-material/Login';
import '../App.css';

function Sidebar() {
  const location = useLocation();
  const navigate = useNavigate();

  const handleLogout = () => {
    try {
      localStorage.removeItem('token');
      console.log("Logging out...");
      navigate('/login');
    } catch (error) {
      console.error("Logout failed", error);
    }
  }

  const isAuthenticated = localStorage.getItem('token');  


  return (
    <div className="Sidebar">
      <NavLink to='/dashboard' className="logo">
        <img src="/datasky_logo_white.png" alt="logo"/>
      </NavLink>
      <ul className="SidebarList">
        {SidebarData.map((val, key) => (
          <li key={key} className="row">
            <NavLink
              to={val.link}
              className="nav-link"
              activeClassName="active"
              isActive={() => val.link === location.pathname}
            >
              <div className="nav-icon">{val.icon}</div>
              <div className="nav-title">{val.title}</div>
              
            </NavLink>     
          </li>
        ))}
    {isAuthenticated ? (
          <li className="logout" onClick={handleLogout}>
            <NavLink to="/" className="nav-link">
              <div className="nav-icon"><LogoutIcon /></div>
              <div className="nav-title">Logout</div>
            </NavLink>
          </li>
        ) : (
          <li className="login">
            <NavLink to="/login" className="nav-link"> 
              <div className="nav-icon"><LoginIcon /></div>
              <div className="nav-title">Login</div>
            </NavLink>
          </li>
        )}
      </ul>
    </div>
  );
}


export default Sidebar;

