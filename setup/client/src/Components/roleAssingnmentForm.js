import React, { useState, useEffect } from 'react';
import { Select, Button, message } from 'antd';
import { fetchRoles, assignRoleToUser } from '../services/roleService'; 

const { Option } = Select;

const RoleAssignmentForm = ({ userData, closeModal }) => { 
    const [roles, setRoles] = useState([]); 
    const [selectedRoleId, setSelectedRoleId] = useState(''); 

    useEffect(() => {
        const getRoles = async () => { 
            try {
                const rolesData = await fetchRoles(); 
                setRoles(rolesData); 
            } catch (error) {
                message.error('Failed to fetch roles: ' + error.message);
            }
        };

        getRoles(); 
    }, []);

    const handleAssignRole = async () => {
        if (!selectedRoleId) {
            message.error('Please select a role'); // Update error message
            return;
        }

        try {
            // Update the role assignment data
            const roleAssignmentData = {
                userId: userData._id, // Update property name
                roleId: selectedRoleId,
                // Include projectId and teamId if necessary
            };
            console.log("userid in rokeassignment", roleAssignmentData.userId);
            console.log("selected role in rokeassignment", roleAssignmentData.roleId);



            await assignRoleToUser(roleAssignmentData);

            message.success('Role assigned successfully');
            closeModal();
        } catch (error) {
            message.error('Failed to assign role: ' + error.message);
        }
    };
    const onSearch = (value) => {
        console.log('search:', value);
      };
      const filterOption = (input, option) =>
      option.label.toLowerCase().indexOf(input.toLowerCase()) >= 0;

    return (
        <div>
        <button className="CreateButtonAdmin" onClick={closeModal}>
        Close
        </button>
            <h2>Assign new role</h2>

            <Select
                showSearch
                onSearch={onSearch}
                optionFilterProp="children"
                filterOption={filterOption}
                style={{ width: '100%', marginBottom: '20px' }}
                placeholder="Select a role" // Update placeholder text
                onChange={(value) => setSelectedRoleId(value)} // Update state variable name
                
            >
                {roles.map((role) => (
                    <Option key={role._id} label={role.name} value={role._id}>{role.name}</Option> // Update property names
                ))}
            </Select>
            <button className="CreateButtonAdmin" onClick={handleAssignRole}>
                Assign new role
            </button>
            
        </div>
    );
};

export default RoleAssignmentForm;
