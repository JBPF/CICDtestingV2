import React, { useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { registerUser } from '../../services/userService'; 

const Register = () => {
    const { token } = useParams();
    const navigate = useNavigate();
    const [name, setName] = useState(''); 
    const [password, setPassword] = useState(''); 
    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            await registerUser({ token, name, password });
            navigate('/login');
        } catch (error) {
            console.error('Registration failed:', error);
        }
    };

    return (
        <form onSubmit={handleSubmit}>
            <label>
                Name:
                <input
                    type="text"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    required
                />
            </label>
            <label>
                Password:
                <input
                    type="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </label>
            <button type="submit">Register</button>
        </form>
    );
};

export default Register;
