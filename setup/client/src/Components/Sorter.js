import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import React from "react";

function Sorter({ sortOrder, handleSortChange }) {
  return (
    <div>
      <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
        <InputLabel id="demo-simple-select-standard-label">Sort order</InputLabel>
        <Select
          labelId="demo-simple-select-standard-label"
          id="demo-simple-select-standard"
          value={sortOrder}
          onChange={handleSortChange}
          label="sort"
        >
          <MenuItem value={"asc"}>Ascending</MenuItem>
          <MenuItem value={"desc"}>Descending</MenuItem>
          <MenuItem value={"old"}>Date added (oldest first)</MenuItem>
          <MenuItem value={"new"}>Date added (newest first)</MenuItem>
          <MenuItem value={"custom"}>Custom sort</MenuItem>
        </Select>
      </FormControl>
    </div>
  );
}

export default Sorter;
