import React from 'react';
import { useParams } from 'react-router-dom';
import TitleAbstractScreening from '../Pages/Review/TitleAbstractScreening';
import FullTextReview from '../Pages/Projects/FullTextReview';
import TitleScreening from '../Pages/Review/TitleScreening';
import ExtractionPage from '../Pages/Projects/ExtractionPage';
import AbstractScreening from '../Pages/Review/AbstractScreening';

const DynamicStageComponent = () => {
    const { stage } = useParams();

    const getStageComponent = (stageSlug) => {
        switch (stageSlug) {
            case 'title-abstract-review':
                return <TitleAbstractScreening />;
            case 'full-article-review':
                return <FullTextReview />;
            case 'title-review':
                return <TitleScreening />
            case 'abstract-review':
                return <AbstractScreening />
            case 'extraction':
                return <ExtractionPage />
            default:
                return <div>Unknown stage</div>;
        }
    };

    return getStageComponent(stage);
};

export default DynamicStageComponent;
