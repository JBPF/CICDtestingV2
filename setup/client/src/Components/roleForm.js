
import React, { useState, useEffect } from 'react';
import { Form, Input, Button, Steps, Select, message, Checkbox, Tag} from 'antd';import {createRole, updateRolePermissions, fetchRoles } from '../services/roleService';
import { fetchPermissions } from '../services/permissionService';
import { CloseOutlined } from '@ant-design/icons';

const { Step } = Steps;
const { Option } = Select;
const CheckboxGroup = Checkbox.Group;

const initialRoleDataState = {
    name: '',
    permissions: [],
};
const plainOptions = ['Apple', 'Pear', 'Orange'];
const defaultCheckedList = ['Apple', 'Orange']; 
const RoleForm = ({ roleToEdit, closeModal }) => {
    const [currentStep, setCurrentStep] = useState(0);
    const [roleData, setRoleData] = useState(roleToEdit || initialRoleDataState);
    const [permissions, setPermissions] = useState([]);
    const [selectedPermissionActions, setSelectedPermissionActions] = useState([]);
    const [loading, setLoading] = useState(false);
    const [editMode, setEditMode] = useState(!!roleToEdit);
    const [form] = Form.useForm();
    const [checkedPermissions, setCheckedPermissions] = useState([]);
    const [selectedPermissions, setSelectedPermissions] = useState([]);


    // Add this line
 

    const [checkedList, setCheckedList] = useState(defaultCheckedList);
    const checkAll = plainOptions.length === checkedList.length;
    const indeterminate = checkedList.length > 0 && checkedList.length < plainOptions.length;
    const handleClose = () => {
        form.resetFields();
        closeModal();
    };
    const onPermissionSearch = (value) => {
        // Handle the search logic here
        console.log('search:', value);
        // You can filter the alphabeticalOptions based on the search value and update the state
        // For simplicity, let's assume 'label' is the property to search in the options
        
    };
    const filterOption = (input, option) =>
        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;

    useEffect(() => {
        const initializeForm = async () => {
            try {
                const permissionsData = await fetchPermissions();
                setPermissions(permissionsData);
                if (roleToEdit) {
                    setRoleData(roleToEdit);
                    setEditMode(true);
                }
            } catch (error) {
                message.error('Error fetching data: ' + error.message);
            }
        };
        initializeForm();
    }, [roleToEdit]);
    const handleChange = (key, value) => {
        setRoleData({ ...roleData, [key]: value });
    };
    const handlePermissionSelect = (selectedPermissionIds) => {
        handlePermissionChange(selectedPermissionIds);
    };
    const handlePermissionChange = (selectedPermissionIds) => {
        const selectedActions = selectedPermissionIds.map(id => {
            const permission = permissions.find(p => p._id === id);
            return permission ? permission.action : null;
        }).filter(action => action !== null); // Filter out any nulls in case of not found
        setSelectedPermissionActions(selectedActions);
        handleChange('permissions', selectedPermissionIds)
    };
    const allPermissionIds = permissions.map(permission => permission._id);
        const newCheckedPermissions =
            checkedPermissions.length === allPermissionIds.length ? [] : allPermissionIds;
    const handleToggleAllPermissions = () => {
        const allPermissionIds = permissions.map(permission => permission._id);
        const newCheckedPermissions =
            checkedPermissions.length === allPermissionIds.length ? [] : allPermissionIds;
            setCheckedPermissions(newCheckedPermissions);

             handlePermissionChange(newCheckedPermissions);
    };
    
    
    

    const handleSubmit = async () => {
        setLoading(true);
        try {
            const rolePayload = {
                name: roleData.name,
                actionKeys: selectedPermissionActions,
            };
            if (editMode) {
                await updateRolePermissions(roleData._id, rolePayload);
                message.success('Role updated successfully!');
            } else {
                await createRole(rolePayload);
                message.success('Role created successfully!');
            }
            form.resetFields();
            closeModal();
        } catch (error) {
            message.error('Failed to submit: ' + error.message);
        } finally {
            setLoading(false);
        }
    };
    const steps = [
        {
            title: 'Role Name',
            content: (
                <Form.Item label="Name" name="name" rules={[{ required: true, message: 'Please input the role name!' }]}>
                    <Input value={roleData.name} onChange={e => handleChange('name', e.target.value)} />
                </Form.Item>
            ),
        },
        {
            title: 'Permissions',
            content: (
                <Form.Item label="Permissions" name="permissions" rules={[{ required: true, message: 'Please select at least one permission!' }]}>
                    <div style={{ display: 'flex', flexDirection: 'column', gap: '8px' }}>
                    <Select
    mode="multiple"
    showSearch
    onSearch={onPermissionSearch}
    optionFilterProp="children"
    filterOption={filterOption}
    style={{ width: '100%', marginBottom: '20px' }}
    placeholder="Select permissions"
    onChange={(selectedPermissionIds) => {
        handlePermissionSelect(selectedPermissionIds);
        setSelectedPermissions(selectedPermissionIds); // Update selectedPermissions
    }}
>
    {permissions.map(option => (
        <Option key={option._id} value={option._id}>
            {option.action}
        </Option>
    ))}
</Select>
                    <div style={{ marginBottom: '20px' }}>
                        <h3>Selected Permissions:</h3>
                        {selectedPermissions.map((permissionId) => (
                            <Tag key={permissionId}>
                                {permissions.find((permission) => permission._id === permissionId)?.action}
                            </Tag>
                        ))}
                    </div>
                </div>
                    {/* <div style={{ display: 'flex', flexDirection: 'column', gap: '8px' }}>
                        <Checkbox
                            indeterminate={indeterminate}
                            onChange={handleToggleAllPermissions}
                            checked={checkedPermissions.length === permissions.length}
                            style={{ marginBottom: '8px' }}

                        >
                            Select All
                        </Checkbox>
                    <div style={{ display: 'flex', flexDirection: 'column', columns: 3, width: '100%' , display: 'inline-block'}}
>
                    <Checkbox.Group
                        options={alphabeticalOptions}
                        value={checkedPermissions}
                        onChange={(checkedValues) => {
                            setCheckedPermissions(checkedValues);
                            handlePermissionChange(checkedValues);
                        }}
                    />
                    </div>

                    </div> */}

        </Form.Item>
        
            ),
        },
    ];
    return (
        <Form form={form} layout="vertical">
            <button className="CreateButtonAdmin" onClick={closeModal}>
                Close
            </button>
            <Steps current={currentStep}>
                {steps.map(item => (
                    <Step key={item.title} title={item.title} />
                ))}
            </Steps>
            <div className="steps-content" style={{ marginTop: '24px' }}>
                {steps[currentStep].content}
            </div>
            <div className="steps-action" style={{ marginTop: '24px' }}>
                {currentStep < steps.length - 1 && (
                    <Button type="primary" onClick={() => setCurrentStep(currentStep + 1)} disabled={loading}>
                        Next
                    </Button>
                )}
                {currentStep === steps.length - 1 && (
                    <Button type="primary" onClick={handleSubmit} disabled={loading}>
                        Confirm
                    </Button>
                )}
                {currentStep > 0 && (
                    <Button style={{ margin: '0 8px' }} onClick={() => setCurrentStep(currentStep - 1)} disabled={loading}>
                        Previous
                    </Button>
                )}
            </div>
        </Form>
    );
};
export default RoleForm;
