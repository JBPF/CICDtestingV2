import React from "react";
import DashboardIcon from '@mui/icons-material/Dashboard';
import SettingsIcon from '@mui/icons-material/Settings';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import GroupIcon from '@mui/icons-material/Group';

export const SidebarData = [
  {
    title: "Dashboard",
    icon: <DashboardIcon />,
    link: "/dashboard",
  },
  {
    title: "Profile", 
    icon: <AccountCircleIcon />,
    link: "/profile", 
  },
  {
    title: "Settings",
    icon: <SettingsIcon />,
    link: "/settings",
  },

  {
    title: "My Teams",
    icon: <GroupIcon />,
    link: "/myteams",
  }
];