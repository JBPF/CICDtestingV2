import React, { useEffect, useState } from 'react';
import Modal from 'react-modal';
import { Button } from '@mui/material';
import UserRoleAssignmentForm from './roleAssingnmentForm';
import EditIcon from '@mui/icons-material/Edit';
import { listUserRoles } from '../services/userRoleService';

const UserRoleModal = ({ isOpen, handleClose, selectedUser }) => {
  const [assignModalIsOpen, setAssignModalIsOpen] = useState(false);
  const [userRoles, setUserRoles] = useState([]);
  const [selectedRole, setSelectedRole] = useState(null);


  const openAssignModal = (role) => {
    setSelectedRole(role);
    setAssignModalIsOpen(true);
  };

  const closeAssignModal = () => {
    setSelectedRole(null);
    setAssignModalIsOpen(false);
  };

  useEffect(() => {
    const loadUserRoles = async () => {
      if (selectedUser) {
        listUserRoles(selectedUser._id)
          .then(data => {
            setUserRoles(data);
          })
          .catch(error => {
            console.error("Failed to fetch user Roles");
          });
      }
    };

    loadUserRoles();
  }, [selectedUser]);

  const groupRolesByProject = () => {
    const groupedRoles = {};

    userRoles.forEach((userRole) => {
      const projectName = userRole.projectName;
      if (!groupedRoles[projectName]) {
        groupedRoles[projectName] = [];
      }
      groupedRoles[projectName].push(userRole);
    });

    return groupedRoles;
  };

  return (
    <Modal isOpen={isOpen} onRequestClose={handleClose} contentLabel="User Role Details">
      <div style={{ display: 'flex', justifyContent: 'flex-end', marginTop: '10px' }}>
        <button className="CreateButtonAdmin" onClick={handleClose}>
          Close
        </button>
        <button className="CreateButtonAdmin" onClick={() => openAssignModal(selectedRole)}>
          Assign new role to user
          <EditIcon />
        </button>
      </div>

      <div>
        <h2>User Role Details</h2>
        <h4>User: {selectedUser.email}</h4>

        {Object.entries(groupRolesByProject()).map(([projectName, roles]) => (
          <div key={projectName}>
            <h4>Project: {projectName}</h4>
            <ul>
              {roles.map((userRole, index) => (
                <li key={index}>
                  Team: {userRole.teamName}, Role: {userRole.role}
                </li>
              ))}
            </ul>
          </div>
        ))}
      </div>

      {selectedUser && (
        <Modal isOpen={assignModalIsOpen} onRequestClose={closeAssignModal} contentLabel="Assign Role">
          <UserRoleAssignmentForm userData={selectedUser} closeModal={closeAssignModal} />
        </Modal>
      )}
    </Modal>
  );
};

export default UserRoleModal;
