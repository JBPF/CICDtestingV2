import React, { useState } from 'react';
import { sendInvitation } from '../services/userService';
import SendIcon from '@mui/icons-material/Send';
import { Form, Input } from 'antd';

const InviteUserForm = ({closeModal}) => {
    const [email, setEmail] = useState('');
    const [modalIsOpen, setModalIsOpen] = useState(false);

    const handleClose = () => {
        closeModal();  
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        try {
            console.log('Sending invitation to:', email);
            await sendInvitation({ email });
            alert('Invitation sent successfully!');
            closeModal();
        } catch (error) {
            alert(`Failed to send invitation: ${error.message}`);
        }
    };

    return (

        <div>
             <button className="CreateButtonAdmin" onClick={handleClose}>
                        Close
                    </button>
            <h2>Invite a user</h2>

            <Form.Item label="Email" name="email" rules={[{ required: true, message: 'Please input the email!' }]}>
                    <Input value={email} 
                    onChange={(e) => setEmail(e.target.value)}

                />
                </Form.Item>

            <button className="CreateButtonAdmin" onClick={handleSubmit}>
                        Send invitation
                        <SendIcon />
                    </button>
        </div>
    );
};

export default InviteUserForm;
