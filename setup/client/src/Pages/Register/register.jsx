import React, { useEffect, useState } from 'react';
import { useParams, useNavigate, Link } from 'react-router-dom';
import { registerUser } from '../../services/userService';
import { MDBContainer, MDBRow, MDBCol, MDBInput, MDBBtn } from 'mdb-react-ui-kit';
import '../Login/Login.css';

const Register = () => {
    const { token } = useParams();
    const navigate = useNavigate();
    const [name, setName] = useState(''); 
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState(null);
    const [animate, setAnimate] = useState(false);

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            await registerUser({ token, name, email, password });
            navigate('/login');
        } catch (error) {
            console.error('Registration failed:', error);
            setError(error.message || 'An error occurred during registration.');
        }
    };

    useEffect(() => {
        setAnimate(true);
      }, []);

    return (
        <MDBContainer className="my-5 d-flex justify-content-center align-items-center vh-100" style={{ maxWidth: "85%",  transform: 'translateY(3vh)'}}>
            <MDBRow style={{ marginRight: '-25px', marginLeft: '-25px' }}>
                <MDBCol size='12' md='5' style={{ paddingRight: '0px', marginRight: '-15px', height: '680px'}}>
                    <div className="text-center">
                        <div className="logo logo-animate">
                            <img src="/datasky_logo.png" alt="logo" style={{ maxWidth: "60%", height: "auto" }} />
                        </div>
                        
                    </div>
                    <h4>Please create an account</h4>
                        
                        {error && (
                            <div className="alert alert-danger" role="alert">
                                {error}
                            </div>
                        )}
                        <form onSubmit={handleSubmit}>
                            <MDBInput wrapperClass='mb-4' label='Name' id='form3' type='text' value={name} onChange={(e) => setName(e.target.value)} required />
                            <MDBInput wrapperClass='mb-4' label='Email address' id='form1' type='email' value={email} onChange={(e) => setEmail(e.target.value)} required />
                            <MDBInput wrapperClass='mb-4' label='Password' id='form2' type='password' value={password} onChange={(e) => setPassword(e.target.value)} required />
                            <button type="submit" className="mb-4 w-100 blue-gradient-btn">Register</button>
                        </form>
                        <div className="text-center pt-1 mb-5 pb-1">
                            <p>Already have an account?</p>
                            <Link to="/login" className="text-decoration-none">
                                <MDBBtn color='link'>Login</MDBBtn>
                            </Link>
                        </div>
                    
                </MDBCol>
                <MDBCol col='6' md='5'  className={`mb-5 ${animate ? 'slide-in' : ''}`} style={{ maxWidth: "50%"}}>
                    <div className="d-flex flex-column justify-content-center gradient-custom-2 h-100">
                        <div className="text-white px-3 py-4 p-md-5 mx-md-4">
                            <h3 className="mb-4">Join Us</h3>
                            <p className="small mb-0">By registering, you join a community dedicated to systematic literature review, offering collaboration tools, real-time teamwork, and much more. Welcome aboard!</p>
                        </div>
                    </div>
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    );
};

export default Register;
