import React, { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { Card, CardContent, Typography, Box, Button } from '@mui/material';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import ArrowForwardIcon from '@mui/icons-material/ArrowRightAlt';
import UndoIcon from '@mui/icons-material/Undo';
import { Link } from 'react-router-dom';
import './PrismaFlowchart.css'; 
import { Document, Packer, Paragraph, TextRun } from "docx";
import { saveAs } from "file-saver";

function PrismaFlowchart() {
  const navigate = useNavigate();
  const { projectId } = useParams();
  const [projectTitle, setProjectTitle] = useState('');
  const [stages, setStages] = useState([]);

  useEffect(() => {
    // Fetch project details based on projectId
    // Placeholder for fetch request - replace with actual request to backend?
    const fetchProjectDetails = async () => {
      // Example data structure, replace with actual fetch call from backend?
      const data = {
        title: `PRISMA for ${projectId}`,
        prismaStages: [
          { title: 'Identification', records: { DatabaseSearch: 17, AdditionalImports: 50}, details: [] },
          { title: 'Duplicates', records: { DuplicatedArticlesRemoved: 9 }, details: [], alignment: 'right' },
          { title: 'Screening', records: { ArticlesScreened: 59, ConflictsResolved: 17}, expanded: false, details: []},
          { title: 'Excluded', records: { ArticlesExcluded: 21 }, details: [], alignment: 'right' },
          { title: 'Assessment', records: { FullTextArticlesAssessed: 45, FullTextArticlesResolved: 11 }, expanded: false, details: [], conflicts: [] },
          { title: 'Excluded', records: { FullTextArticlesExcluded: 7 }, details: [], alignment: 'right' },
          { title: 'Included in Extraction', records: { ArticlesIncludedInDataExtraction: 23}, expanded: false, details: [], },
        ],
      };
      setProjectTitle(data.title);
      setStages(data.prismaStages);
    };

    fetchProjectDetails();
  }, [projectId]);

  return (
    <div className="AppContainer">
      <Box className="MainContent">
        <Box className="Prisma">
          <div className="PrismaBackButton" style={{ float: 'right', marginRight: '20px' }}>
            <Button component={Link} to={`/oneproject/${projectId}`} variant="contained" startIcon={<UndoIcon />} style={{ fontFamily: 'inherit' }}>Back</Button>
          </div>  
          <Typography variant="h4" style={{ margin: '20px 0'}}>{projectTitle}</Typography>
          <Box className="flowchartContainer">
          {stages.map((stage, index) => {
            // Check if the current stage is a main stage
            const isMainStage = !stage.alignment || stage.alignment !== 'right';
            // Find the next main stage to determine if a vertical connector is needed
            const hasNextMainStage = stages.slice(index + 1).some((nextStage) => !nextStage.alignment || nextStage.alignment !== 'right');
            return (
              <React.Fragment key={stage.title}>
                {isMainStage && (
                  <Box className="mainStageWithConnectorContainer">
                    <Box className="mainAndSubStageContainer">
                      <Card className="stageCard">
                        <CardContent>
                        <Typography variant="h6" style={{ color: '#1d75af', fontWeight: 'bold' }}>{stage.title}</Typography>
                          {Object.entries(stage.records).map(([key, value]) => (
                            <Typography key={key}>{`${key.replace(/([A-Z])/g, ' $1')}: ${value}`}</Typography>
                          ))}
                        </CardContent>
                      </Card>
                      {stages[index + 1] && stages[index + 1].alignment === 'right' && (
                        <>
                          <Box className="horizontalConnector"><ArrowForwardIcon style={{ fontSize: '40px' }} /></Box>
                          <Card className="stageCard subStage">
                            <CardContent>
                              <Typography variant="h6" style={{ color: '#1d75af', fontWeight: 'bold' }}>{stages[index + 1].title}</Typography>
                              {Object.entries(stages[index + 1].records).map(([key, value]) => (
                                <Typography key={key}>{`${key.replace(/([A-Z])/g, ' $1')}: ${value}`}</Typography>
                              ))}
                            </CardContent>
                          </Card>
                        </>
                      )}
                    </Box>
                    {hasNextMainStage && (
                      <Box className="verticalConnector"><ArrowDownwardIcon style={{ fontSize: '30px' }}/></Box>
                    )}
                  </Box>
                )}
              </React.Fragment>
            );
          })}
          <Button onClick={downloadDocx} variant="contained" style={{ marginTop: '20px' }}>Download as DOCX</Button>
        </Box>
        </Box>
      </Box>
    </div>
  );
  function downloadDocx() {
    // Create a new document
    const doc = new Document({
      sections: [
        {
          properties: {},
          children: [
            new Paragraph({
              children: [
                new TextRun({
                  text: projectTitle,
                  bold: true,
                  size: 24,
                }),
              ],
            }),
            ...stages.map(stage => 
              new Paragraph({
                children: [
                  new TextRun({
                    text: `${stage.title}\n`,
                    bold: true,
                  }),
                  ...Object.entries(stage.records).map(([key, value]) =>
                    new TextRun(`${key.replace(/([A-Z])/g, " $1")}: ${value}\n`)
                  ),
                  ...(stage.details.length > 0 ? [new TextRun(`Details: ${stage.details.join(", ")}\n`)] : []),
                  ...(stage.conflicts ? [new TextRun(`Conflicts: ${stage.conflicts.join(", ")}\n`)] : []),
                ],
              })
            ),
          ],
        },
      ],
    });
  
    // Use Packer to generate a Blob from the document
    Packer.toBlob(doc).then(blob => {
      // Use FileSaver to save the Blob as a .docx file
      saveAs(blob, "PrismaFlowchart.docx");
    });
  }
}

export default PrismaFlowchart;
