import React, { useState } from 'react';
import '../../App.css';
import Projects from '../Projects/Projects';
import CompletedProjects from '../Projects/CompletedProjects';
import SearchIcon from '@mui/icons-material/Search';
import Modal from 'react-modal';
import 'reactjs-popup/dist/index.css';
import AddIcon from '@mui/icons-material/Add';
import ProjectForm from '../../Components/projectForm';
import './Dashboard.css';
import { Paper } from '@mui/material';
import { Box, Tab, Tabs} from '@mui/material';


function Dashboard() {
  const [selectedTab, setSelectedTab] = useState(0);
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [projects, setProjects] = useState([]);
  const [projectTitle, setProjectTitle] = useState('');
  

  const openModal = () => {
    setModalIsOpen(true);
  };

  const handleChange = (event, newValue) => {
    setSelectedTab(newValue);
  };

  const closeModal = () => {
    setModalIsOpen(false);
  };

  return (
    <div className="AppContainer">
      <div className="MainContent">
        <div className="Dashboard">
          <Box sx={{ borderBottom: 1, borderColor: 'divider', width: '100%' }}>
            <Tabs 
              value={selectedTab} 
              onChange={handleChange} 
              aria-label="dashboard tabs" 
              variant="fullWidth"
            >
              <Tab label="Projects" />
              <Tab label="Completed Projects" />
            </Tabs>
          </Box>
          <div className="SearchBar">
          <h1 className="SectionTitle">{selectedTab === 0 ? 'Projects' : 'Completed Projects'}</h1>
            <input
              type="text"
              placeholder="Search studies"
              className="SearchInput"
            />
            <button className="SearchButton">
              <SearchIcon />
            </button>
            </div>
          {selectedTab === 0 && (
            <button className="CreateButton" onClick={openModal}>
              Create Project
              <AddIcon />
            </button>
          )}

          {selectedTab === 0 && (
            <Paper className="TabContent">
              <Projects projects={projects} setProjects={setProjects} />
            </Paper>
          )}
          {selectedTab === 1 && (
            <Paper className="TabContent">
              <CompletedProjects />
            </Paper>
          )}

          <Modal
            isOpen={modalIsOpen}
            onRequestClose={closeModal}
            contentLabel="Create New Project"
            style={{
              content: {
                width: '80%',
                height: '80%',
                margin: 'auto',
              },
            }}
          >
            <button onClick={closeModal}>Close</button>
            <ProjectForm setProjectTitle={setProjectTitle} closeModal={closeModal} />
          </Modal>
        </div>
      </div>
    </div>
  );
}

export default Dashboard;