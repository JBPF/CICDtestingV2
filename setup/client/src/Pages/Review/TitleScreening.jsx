import React, { useState, useEffect } from "react";
import { useParams, useNavigate, Link } from "react-router-dom";
import axios from "axios";
import { Card, CardContent, CardActions, Typography, Button, CircularProgress, IconButton, Paper, Container } from "@mui/material";
import UndoIcon from "@mui/icons-material/Undo";
import DeleteIcon from "@mui/icons-material/Delete";
import DoneIcon from "@mui/icons-material/Done";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ExpandLessIcon from "@mui/icons-material/ExpandLess";
import QuestionMarkIcon from '@mui/icons-material/QuestionMark';
import "../../App.css";
import TablePagination from '@mui/material/TablePagination';
import "./TitleScreening.css";
import Sorter from '../../Components/Sorter'
import CriteriaSection from "../Projects/CriteriaSection";
import "../Projects/CriteriaSection.css";
import { updateReviewDecision } from '../../services/reviewService';


function TitleScreening() {
    const [reviewInstances, setReviewInstances] = useState([]);
    const [loading, setLoading] = useState(true);
    const { projectId } = useParams();
    const [inclusionCriteria, setInclusionCriteria] = useState([]);
    const [exclusionCriteria, setExclusionCriteria] = useState([]);
    const [criteriaSelections, setCriteriaSelections] = useState([]);
    const [canReject, setCanReject] = useState(false);

    const [currentIndex, setCurrentIndex] = useState(0);



    const navigate = useNavigate();

    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);

    useEffect(() => {
        const fetchReviewInstances = async () => {
            setLoading(true);
            try {
                const token = localStorage.getItem("authToken");
                const stage = "titleReview";
                const response = await axios.get(`/api/projects/${projectId}/review-instances/${stage}`, {
                    headers: { Authorization: `Bearer ${token}` },
                });
                const instances = response.data.reviewInstances
                    .filter(instance => instance.reviewStatus === 'pending')
                    .map(instance => ({ ...instance, isExpanded: false }));
                setReviewInstances(instances);
                setLoading(false);
            } catch (error) {
                console.error("Error fetching review instances:", error);
                setLoading(false);
            }
        };

        fetchReviewInstances();
    }, [projectId]);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleCriteriaChange = (newSelections) => {
        setCriteriaSelections(newSelections);
    };

    const toggleAbstract = (index) => {
        setReviewInstances(current =>
            current.map((item, idx) => idx === index ? { ...item, isExpanded: !item.isExpanded } : item)
        );
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleDecision = async (reviewInstanceId, decision) => {
        try {
            if (decision === 'reject' && criteriaSelections.length === 0) {
                alert("Please select at least one exclusion criterion before rejecting.");
                return;
            }

            const payload = {
                decision,
                criteriaSelections,
            };

            console.log("Payload:", payload);
            await axios.patch(`/api/review-instances/${reviewInstanceId}/decision`, payload, {
                headers: { 'Content-Type': 'application/json' },
            });

            if (currentIndex < reviewInstances.length - 1) {
                // Move to next review instance
                setCurrentIndex(currentIndex + 1);
            } else {
                // No more instances, can navigate away or show a completion message
                alert("All titles have been reviewed.");
                navigate(`/oneproject/${projectId}`);
            }

        } catch (error) {
            console.error("Error updating review decision:", error);
        }
    };



    if (loading) return <CircularProgress />;

    return (
        <div className="AppContainer">
            <div className="MainContent">
                <div className="TitleScreen">
                    <div className="TitleScreenHeader">
                        <h2 className="TitleScreenTitle">Title Review Screening</h2>
                        <div className="BackButton">
                            <Button component={Link} to={`/oneproject/${projectId}`} variant="contained" startIcon={<UndoIcon />} style={{ fontFamily: 'inherit' }}>Back</Button>
                        </div>
                    </div>
                    <div className="HeaderText">
                        Welcome back. Accept all titles relevant to your study and reject any that aren’t by selecting the necessary rejection criteria option.
                    </div>
                    <div className="Body">
                        {reviewInstances.length > 0 ? (
                            <Paper className="Articles" variant="outlined" sx={{ backgroundColor: "rgb(231, 231, 231)", border: "0px solid" }}>
                                <div className="Article" variant="outlined" >
                                    <div className="ArticleTitle" variant="outline">
                                        {reviewInstances[currentIndex].article.title}
                                    </div>
                                    <div className="RejectAcceptButtons">
                                        <Button variant="contained" color="success" style={{ fontFamily: 'inherit', fontWeight: 'bold', borderRadius: 5, width: '120px' }} onClick={() => handleDecision(reviewInstances[currentIndex]._id, 'accept')} startIcon={<DoneIcon />}>
                                            Accept
                                        </Button>
                                        <Button variant="contained" color="error" startIcon={<DeleteIcon />} style={{ fontFamily: 'inherit', borderRadius: 5, width: '120px' }} onClick={() => handleDecision(reviewInstances[currentIndex]._id, 'reject')}>
                                            Reject
                                        </Button>
                                    </div>
                                </div>
                            </Paper>

                        ) : (
                            <Typography variant="h6" align="center" style={{ marginTop: '20px', marginBottom: '20px', fontFamily: 'inherit' }}>
                                End of Title Screening.
                            </Typography>
                        )}

                    </div>
                    <div className="CriteriaSectionTitle">
                        <CriteriaSection projectId={projectId} onCriteriaChange={handleCriteriaChange} />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default TitleScreening;
