import React, { useState, useEffect } from "react";
import { useParams, useNavigate, Link } from "react-router-dom";
import axios from "axios";
import { Card, CardContent,CardActions, Typography, Button, CircularProgress, IconButton, Paper, Container } from "@mui/material";
import UndoIcon from "@mui/icons-material/Undo";
import DeleteIcon from "@mui/icons-material/Delete";
import DoneIcon from "@mui/icons-material/Done";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ExpandLessIcon from "@mui/icons-material/ExpandLess";
import QuestionMarkIcon from '@mui/icons-material/QuestionMark';
import "../../App.css";
import TablePagination from '@mui/material/TablePagination';
import "./TitleScreen.css";
import Sorter from '../../Components/Sorter'
import CriteriaSection from "../Projects/CriteriaSection";
import "../Projects/CriteriaSection.css";
import { updateReviewDecision } from '../../services/reviewService';


function TitleAbstractReview() {
    const [reviewInstances, setReviewInstances] = useState([]);
    const [loading, setLoading] = useState(true);
    const { projectId } = useParams();
    const [inclusionCriteria, setInclusionCriteria] = useState([]);
    const [exclusionCriteria, setExclusionCriteria] = useState([]);
    const [criteriaSelections, setCriteriaSelections] = useState([]);
    const [canReject, setCanReject] = useState(false);



  const navigate = useNavigate();

  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  useEffect(() => {
    const fetchReviewInstances = async () => {
      setLoading(true);
      try {
        const token = localStorage.getItem("authToken");
        const stage = "titleAbstractReview";
        const response = await axios.get(`/api/projects/${projectId}/review-instances/${stage}`, {
          headers: { Authorization: `Bearer ${token}` },
        });
        const instances = response.data.reviewInstances
            .filter(instance => instance.reviewStatus === 'pending')
            .map(instance => ({ ...instance, isExpanded: false }));
        setReviewInstances(instances);
        setLoading(false);
      } catch (error) {
        console.error("Error fetching review instances:", error);
        setLoading(false);
      }
    };

    fetchReviewInstances();
  }, [projectId]);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleCriteriaChange = (newSelections) => {
    setCriteriaSelections(newSelections);
  };

  const toggleAbstract = (index) => {
    setReviewInstances(current =>
        current.map((item, idx) => idx === index ? { ...item, isExpanded: !item.isExpanded } : item)
    );
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleDecision = async (reviewInstanceId, decision) => {
    try {
      if (decision === 'reject' && criteriaSelections.length === 0) {
        alert("Please select at least one exclusion criterion before rejecting.");
        return;
      }

      const payload = {
        decision,
        criteriaSelections,
      };

      console.log("Payload:", payload);
      await axios.patch(`/api/review-instances/${reviewInstanceId}/decision`, payload, {
        headers: { 'Content-Type': 'application/json' },
      });

    } catch (error) {
      console.error("Error updating review decision:", error);
    }
  };



  if (loading) return <CircularProgress />;

  return (
      <div className="AppContainer">
        <div className="MainContent">
          <div className="TitleScreen">
            <div className="TitleScreenHeader">
              <h2 className="TitleScreenTitle">Title/Abstract Review Screening</h2>
              <TablePagination
                  component="div"
                  count={reviewInstances.length}
                  page={page}
                  onPageChange={handleChangePage}
                  rowsPerPage={rowsPerPage}
                  onRowsPerPageChange={handleChangeRowsPerPage}
              />
              <div className="BackButton">
              <Button component={Link} to={`/oneproject/${projectId}`} variant="contained" startIcon={<UndoIcon />}style={{ fontFamily: 'inherit' }}>Back</Button>
              </div>
            </div>
            <div className="Body">

              <Paper className="Articles" variant="outlined" sx={{ backgroundColor: "rgb(231, 231, 231)", border: "0px solid" }}>
                {reviewInstances.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((instance, index) => (
                    <Card className="Article" key={index} variant="outlined">
                      <CardContent className="ArticleHeader">
                        <IconButton onClick={() => toggleAbstract(index)} className="AbstractCollapseButton">
                          {instance.isExpanded ? <ExpandLessIcon /> : <ExpandMoreIcon />}
                        </IconButton>

                        <Typography className="ArticleTitle" variant="h6"  component="h2" style={{ fontFamily: 'inherit' }}>{instance.article.title}</Typography>
                      <div style={{ marginTop: 16 }}>

                        <Button variant="contained" color="success" style={{ fontFamily: 'inherit', fontWeight:'bold',borderRadius: 5 }} onClick={() => handleDecision(instance._id, 'accept')} startIcon={<DoneIcon />}>Accept</Button>
                        <Button variant="contained" color="error" startIcon={<DeleteIcon />} style={{ fontFamily: 'inherit', borderRadius: 5 }} onClick={() => handleDecision(instance._id, 'reject')}>Reject</Button>
                      </div>
                    </CardContent>
                      {instance.isExpanded && (
                          <Typography className="Abstract" variant="body2">{instance.article.abstract}</Typography>
                      )}
                  </Card>
              ))};
                <Typography variant="h6" align="center" style={{ marginTop: '20px', marginBottom: '20px', fontFamily: 'inherit' }}>
                  End of Title/Abstract Screening.
                </Typography>
              </Paper>
              <CriteriaSection projectId={projectId} onCriteriaChange={handleCriteriaChange} />


            </div>
          </div>
        </div>
      </div>
  );
}

export default TitleAbstractReview;
