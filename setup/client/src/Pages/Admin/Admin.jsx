import React, { useEffect, useState } from 'react';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import AddIcon from '@mui/icons-material/Add';
import Modal from 'react-modal';
import RoleForm from '../../Components/roleForm';
import UserRoleModal from '../../Components/UserRole';
import UserInvitationForm from '../../Components/invitationForm';
import UserRoleAssignmentForm from '../../Components/roleAssingnmentForm';
import { Paper } from '@mui/material';
import { fetchRoles, deleteRole, addPermission, deletePermission} from '../../services/roleService';
import { Select, message } from 'antd';
import { fetchUsers } from '../../services/userService';
import { fetchPermissions } from '../../services/permissionService';


import './Admin.css';
const { Option } = Select;

function Admin() {
    const [roleModalIsOpen, setRoleModalIsOpen] = useState(false);
    const [inviteModalIsOpen, setInviteModalIsOpen] = useState(false);
    const [assignModalIsOpen, setAssignModalIsOpen] = useState(false);
    const [userModalIsOpen, setUserModalIsOpen] = useState(false);
    const [roles, setRoles] = useState([]);
    const [selectedRole, setSelectedRole] = useState(null);
    const [users, setUsers] = useState([]);
    const [selectedUserId, setSelectedUserId] = useState('');
    const [selectedRolePermissions, setSelectedRolePermissions] = useState([]);
    const [isEditing, setIsEditing] = useState(false);
    const [newPermission, setNewPermission] = useState('');
    const [selectedPermissionToDelete, setSelectedPermissionToDelete] = useState(null);
    const [permissions, setPermissions] = useState('');

    const selectedUser = users.find((user) => user._id === selectedUserId);
    //fetch information needed from back
    useEffect(() => {
        const getRoles = async () => {
            try {
                const rolesData = await fetchRoles();
                setRoles(rolesData);
            } catch (error) {
                message.error('Failed to fetch roles: ' + error.message);
            }
        };
        getRoles();
    }, []);

    useEffect(() => {
        const getUsers = async () => {
            try {
                const usersData = await fetchUsers();
                setUsers(usersData);
            } catch (error) {
                message.error('Failed to fetch users: ' + error.message);
            }
        };

        getUsers();
    }, []);

    useEffect(() => {
        const getPermissions = async () => {
            try {
                const permissionsData = await fetchPermissions();
                setPermissions(permissionsData);
            } catch (error) {
                message.error('Failed to fetch permissions: ' + error.message);
            }
        };

        getPermissions();
    }, []);

    const handleDelete = async (roleId) => {
        try {
            console.log(`role passed in: ${roleId}`);

            await deleteRole(roleId);
            setRoles(roles.filter((role) => role._id !== roleId));
            message.success('Role deleted successfully');
        } catch (error) {
            message.error('Failed to delete role from admin: ' + error.message);
            console.log(`role was: ${roleId}`);

        }
    };

    const handleEditClick = () => {
        setIsEditing(!isEditing);
    };
    const handleDeletePermission = (permissionId) => {
        if (permissionId) {
            try{
                deletePermission(selectedRole._id, permissionId);
                message.success('Permission deleted successfully')

            }
            catch (error){
                message.error('Failed to delete permission:'+ error.message);
            }
            // Additional logic as needed
        }
    };
    const handleNewPermissionAdd = async (selectedPermission) => {
        if (selectedPermission) {
            try {
                const roleId = selectedRole._id;
                console.log('Adding permission to role:', roleId);
                console.log('Selected Permission ID:', selectedPermission);
    
                // Make the API request
                await addPermission(roleId, selectedPermission);
                
                message.success('Permission added successfully');
            } catch (error) {
                console.error('Error adding permission:', error);
    
                // Display an error message
                message.error('Failed to add permission: ' + error.message);
            }
        }
    };
    
    

    
    const openRoleModal = () => setRoleModalIsOpen(true);
    const closeRoleModal = () => setRoleModalIsOpen(false);
    const openInviteModal = () => setInviteModalIsOpen(true);
    const closeInviteModal = () => setInviteModalIsOpen(false);
    //why not used?
    const openAssignModal = (role) => {
        setSelectedRole(role);
        setAssignModalIsOpen(true);
    };
    const closeAssignModal = () => {
        setSelectedRole(null);
        setAssignModalIsOpen(false);
    };
    const openUserRoleModal = () => setUserModalIsOpen(true);
    const closeUserRoleModal = () => setUserModalIsOpen(false);
    
    const handleUserClick = (value) => {
        setSelectedUserId(value);
        setSelectedRolePermissions([]); // Clear role permissions when a user is selected
    };

    const handleRoleClick = (value) => {
        const selectedRole = roles.find((role) => role._id === value);
        setSelectedRole(selectedRole);
        setSelectedRolePermissions(selectedRole.permissions);
    };

    const onSearch = (value) => {
        console.log('search:', value);
    };
    const filterOption = (input, option) =>
        option.label.toLowerCase().indexOf(input.toLowerCase()) >= 0;

    return (
        <div className="AppContainer">
            <div className="MainContent">
                <div className="Dashboard">
                    <div className="DashboardHeader">
                        <h2 className="SummaryTitle">Admin Dashboard</h2>
                    </div>
                    <div class="button-container">
                        <button className="CreateButtonAdmin" onClick={openRoleModal}>
                            Create Role
                            <AddIcon />
                        </button>
                        <button className="CreateButtonAdmin" onClick={openInviteModal}>
                            Invite User
                            <AddIcon />
                        </button>
                    </div>
                    <div style={{ display: "flex" }}>

                        <Paper className="CurrentProjects" variant="outlined" sx={{ backgroundColor: "rgb(243,243,243)", border: "0px solid", width: '50%', display: "inline-block" }}>
                            <h3>Users</h3>
                            <Select
                                showSearch
                                onSearch={onSearch}
                                optionFilterProp="children"
                                filterOption={filterOption}
                                style={{ width: '100%', marginBottom: '20px' }}
                                placeholder="Select a user"
                                onChange={(value) => handleUserClick(value)}
                            >
                                {users.map((user) => (
                                    <Option key={user._id} value={user._id} label={user.email}>{user.email}</Option>
                                ))}
                            </Select>
                            {selectedUserId && (
                                <button className="CreateButtonAdmin" onClick={() => openUserRoleModal()}>
                                    Manage permissions
                                </button>
                            )}
                            {selectedUserId && (
                                <UserRoleModal isOpen={userModalIsOpen} handleClose={closeUserRoleModal} selectedUser={selectedUser} />
                            )}
                        </Paper>

                        <Paper className="CurrentProjects" variant="outlined" sx={{ backgroundColor: "rgb(243,243,243)", border: "0px solid", width: '50%', display: "inline-block" }}>
                            <h3>Roles</h3>
                            <Select
                                showSearch
                                onSearch={onSearch}
                                optionFilterProp="children"
                                filterOption={filterOption}
                                style={{ width: '100%', marginBottom: '20px' }}
                                placeholder="Search for a role"
                                onChange={(value) => handleRoleClick(value)}
                            >
                                {roles.map((role) => (
                                    <Option key={role._id} value={role._id} label={role.name}>{role.name}</Option>
                                ))}
                            </Select>
                            {selectedRole && (
                                <div style={{justifyContent: 'space-between'}}>
                                    
<Paper className="PermissionsCurrent" variant="outlined" sx={{ backgroundColor: "rgb(243,243,243)", border: "0px solid" }}>
    <div className="TagsElementTitle">
        <h3> {selectedRole.name} Permissions:</h3>
    </div>
    <div className="row" style={{ paddingBottom: '20px' }}>
        <div className="col">
            <ul>
                {selectedRolePermissions.map((permission) => (
                    <Paper className="TagElements" variant="outlined" sx={{ backgroundColor: "rgb(243,243,243)", border: "0px solid" }} key={permission._id}>
                        <div className="TagsElementTitle">
                            {permission.description}
                        </div>
                        {isEditing && (
                            <div className="TagElementDeleteButton">
                                <button className="btn btn-danger btn-sm" onClick={() => handleDeletePermission(permission._id)}>
                                    <DeleteIcon />
                                </button>
                            </div>
                        )}
                    </Paper>
                ))}
            </ul>
        </div>
    </div>
</Paper>

        <button className="CreateButtonAdmin" style={{marginBottom: '10px'}} onClick={handleEditClick} >
            Edit role
            <EditIcon />
            </button>
                        {isEditing && (
                            console.log(`role to delete: ${selectedRole._id}`),

                            <button className="DeleteButtonAdmin" onClick={() => handleDelete(selectedRole._id)}>
                            Delete Role
                                <DeleteIcon />
                            </button>
                        )}

                        {isEditing && (
                            <div>
                                <Select
                                    showSearch
                                    onSearch={onSearch}
                                    optionFilterProp="children"
                                    filterOption={filterOption}
                                    style={{ width: '100%', marginBottom: '20px' }}
                                    placeholder="Search for a permission"
                                    onChange={(value) => {
                                        console.log('Selected Permission ID:', value);
                                        setNewPermission(value);
                                    }}                                >
                                    {permissions.map((permission) => (
                                        <Option key={permission._id} value={permission._id} label={permission.description}>{permission.description}</Option>
                                    ))}
                                </Select>
                                <button className="CreateButtonAdmin" onClick={() => handleNewPermissionAdd(newPermission)}>
                                        Add New Permission
                                    <AddIcon />
                                </button>
                                
                            </div>
                        )}
                    </div>
                )}

            </Paper>
                    </div>
                    {/* Role Creation Modal */}
                    <Modal isOpen={roleModalIsOpen} onRequestClose={closeRoleModal} contentLabel="Create Role">
                        <RoleForm closeModal={closeRoleModal} />
                    </Modal>
                    {/* User Invitation Modal */}
                    <Modal isOpen={inviteModalIsOpen} onRequestClose={closeInviteModal} contentLabel="Invite User">
                        <UserInvitationForm closeModal={closeInviteModal} />
                    </Modal>
                    {/* User Role Assignment Modal */}
                    {selectedRole && (
                        <Modal isOpen={assignModalIsOpen} onRequestClose={closeAssignModal} contentLabel="Assign Role">
                            <UserRoleAssignmentForm roleData={selectedRole} closeModal={closeAssignModal} />
                        </Modal>
                    )}
                </div>
            </div>
        </div>
    );
}

export default Admin;
