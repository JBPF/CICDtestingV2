import React, { useState, useEffect, useCallback } from 'react';
import { useParams, useNavigate, Link } from 'react-router-dom';
import axios from 'axios';
import Button from '@mui/material/Button';
import BuildIcon from '@mui/icons-material/Build';
import CircularProgress from "@mui/material/CircularProgress";
import 'reactjs-popup/dist/index.css';
import "../../App.css";
import './OneProject.css';
import { Paper } from '@mui/material';
import UndoIcon from "@mui/icons-material/Undo";



const OneProject = () => {
  const [projectData, setProjectData] = useState(null);
  const [loading, setLoading] = useState(true);
  const { projectId } = useParams();
  const navigate = useNavigate();


  useEffect(() => {
    const fetchProjectData = async () => {
      setLoading(true);
      try {
        const token = localStorage.getItem("authToken");

        const config = {
          headers: { Authorization: `Bearer ${token}` }
        };

        const response = await axios.get(`http://localhost:3000/api/projects/${projectId}`, config);

        setProjectData(response.data.project);
      } catch (error) {
        console.error('Error fetching project data:', error);
      } finally {
        setLoading(false);
      }
    };

    fetchProjectData();
  }, [projectId]);

  if (loading || !projectData) {
    return <CircularProgress />;
  }




  const stagesConfig = projectData.reviewStageConfig ? {
    'three-stage': ['Title Review', 'Abstract Review', 'Full Article Review', 'Extraction'],
    'two-stage': ['Title & Abstract Review', 'Full Article Review', 'Extraction'],
  } : {};

  const stages = stagesConfig[projectData.reviewStageConfig] || [];

  const handleContinueTitle = () => {
    navigate('/titleabstract');
  };

  const handleContinueFullText = () => {
    navigate('/fulltext')
  };

  const handleContinueExtraction = () => {
    navigate('/extraction')
  };

  const handlePrisma = () => {
    navigate('/prisma')
  }

  const formatStageForUrl = (stage) => {
    return stage.toLowerCase().replace(/ & /g, '-').replace(/\s+/g, '-');
  };

  const tiers = stages.map((stage) => {
    //Default description
    let description = [
      `You have screened X studies today for ${stage}`,
      'Other reviewer has screened Y studies',
      'Both reviewed Z studies',
      'W conflicts waiting to be resolved',
    ];

    if (stage === 'Extraction') {
      description = [
        'Data extraction is currently in process',
        'Review extraction guidelines for accuracy',
        'V extraction conflicts need resolution',
      ];
    }

    return {
      title: stage,
      progress: '0', //fetch from backend
      description: description,
      buttonText: 'Continue Screening',
      buttonVariant: 'contained',
      onClick: () => navigate(`/projects/${projectId}/${formatStageForUrl(stage)}`)
    };
  });

  return (
    <div className="AppContainer">
      <div className="MainContent">
        <div className="OneProject">
          <div className="ProjectHeader">

            <div className="TopHeader">
              <div className="Title">
                {projectData.title} Dashboard
              </div>
              <div className="Back">
                <Button component={Link} to="/dashboard" variant="contained" startIcon={<UndoIcon />} style={{ fontFamily: 'inherit', fontWeight: 'bold' }}>Back</Button>
              </div>
            </div>
            <div className="SubHeader">
              <div className="IntroductionText">
                Welcome back, take a moment to review your progress and continue with your screening process.
              </div>
              
            </div>
          </div>

          <div className="ReviewBody">
            {tiers.map((tier) => (
              <Paper className="ReviewStage" variant='outlined' sx={{ backgroundColor: 'rgb(217,217,217)', border: "0px solid" }}>
                <Paper className="ReviewTitle" variant='outlined'>
                  {tier.title}
                </Paper>

                <Paper className="ReviewDescription" variant='outlined'>

                  {tier.description.map((line) => (
                    <div className="DescriptionLine" key={line}>
                      {line}
                    </div>
                  ))}
                </Paper>

                <Button className="ReviewButton" variant="contained" onClick={tier.onClick} sx={{
                  /* styling done here as I there was problems using styling on .css page */
                  margin: '5px',
                  width: 'calc(100% - 10px)',
                  background: 'rgb(27, 84, 116)',
                  color: 'white',
                  padding: '10px 20px',
                  border: 'none',
                  borderRadius: '5px',
                  cursor: 'pointer',
                  fontSize: '16px',
                  boxShadow: '0 2px 2px rgba(0, 0, 0, 0.2)',
                  outline: 'none',
                  transition: '0.3s ease',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  fontFamily: 'inherit',
                  fontWeight: '600',
                  '&:hover': {
                    background: 'rgb(21, 71, 100)',
                    boxShadow: '0px 4px 5px rgba(0, 0, 0, 0.2)',
                  },
                }}>
                  Continue Screening
                </Button>

              </Paper>
            ))}
          </div>
          <Button className="GeneratePrimsaButton" startIcon={<BuildIcon />} component={Link} to="/prisma" variant="contained" sx={{
            fontFamily: 'inherit',
            background: 'rgb(27, 116, 63)',
            fontWeight: '600',
            fontSize: '1.20rem',
            marginLeft: '10px',
            marginTop: '6px',
            width: 'calc(100% - 10px)',
            boxShadow: '0 2px 2px rgba(0, 0, 0, 0.2)',
            transition: '0.4s ease',
            '&:hover': {
              background: 'rgb(21, 103, 47)',
              boxShadow: '0px 4px 5px rgba(0, 0, 0, 0.2)',
            },

          }}>
            generate Prisma
          </Button>




        </div>
      </div>
    </div >
  );
};

export default OneProject;