import React, { useState } from 'react'
import PeopleIcon from '@mui/icons-material/People';
import DownloadIcon from '@mui/icons-material/GetApp';
import DeleteIcon from '@mui/icons-material/Delete';
import './CompletedProjects.css';
import { Link } from "react-router-dom";
import SettingsIcon from '@mui/icons-material/Settings';
import { Button } from '@mui/material';


// fake data for completed projects
export const completedProjectsData = [
  {
    id: 1,
    title: 'Completed Project 1',
  },
  {
    id: 2,
    title: 'Completed Project 2',
  },
  {
    id: 3,
    title: 'Completed Project 3',
  },
  {
    id: 4,
    title: 'Completed Project 4',
  },
  {
    id: 5,
    title: 'Completed Project 5',
  },

  // ... more completed projects
];

function CompletedProjects() {
  const [currentPage, setCurrentPage] = useState(0);
  const projectsPerPage = 7;

  const indexOfLastProject = (currentPage + 1) * projectsPerPage;
  const indexOfFirstProject = indexOfLastProject - projectsPerPage;
  const currentProjects = completedProjectsData.slice(indexOfFirstProject, indexOfLastProject);

  const handlePrevious = () => {
    setCurrentPage(prevPage => Math.max(0, prevPage - 1));
  };

  const handleNext = () => {
    setCurrentPage(prevPage => Math.min(prevPage + 1, Math.ceil(completedProjectsData.length / projectsPerPage) - 1));
  };

  return (
    <>
      <div className="CompletedProjects">
        {currentProjects.map((project) => (
          <Link to={`/oneproject/${project.id}`} key={project.id} style={{ textDecoration: 'none' }}>
            <div className="CompletedProjectItem">
              <PeopleIcon className="PeopleIcon" />
              <span className="CompletedProjectTitle">{project.title}</span>
              <div className="CompletedProjectActions">
                <Link to="/export" className="Export">
                  <div className="ExportTitle">Export</div>
                  <div className="ExportIcon"><DownloadIcon /></div>
                </Link>
                <Link to="/dashboard" className="ActionIcon">
                  <DeleteIcon />
                </Link>
                <Link to="/projectsettings" className="SettingsIcon">
                  <SettingsIcon />
                </Link>
              </div>
            </div>
          </Link>
        ))}
      </div>
      <div className="PaginationButtons">
        <Button variant="contained" onClick={handlePrevious} disabled={currentPage === 0}>
          Previous
        </Button>
        <Button variant="contained" onClick={handleNext} disabled={currentPage >= Math.ceil(completedProjectsData.length / projectsPerPage) - 1}>
          Next
        </Button>
      </div>
    </>
  );
}

export default CompletedProjects;