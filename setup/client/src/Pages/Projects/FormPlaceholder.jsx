import React, { useEffect, useRef, useState } from 'react';
import { Paper, TextField, Typography, Divider, Button, ThemeProvider, createTheme } from '@mui/material';
import '../../App.css';
import SaveIcon from '@mui/icons-material/Save';
import { Link } from 'react-router-dom';

const theme = createTheme({
  typography: {
    fontFamily: [
      'Montserrat',
      'Roboto',
    ].join(','),
  },
});

const FormPlaceholder = ({ handleChange, options, formValues }) => {
  const formRefs = useRef({});

  useEffect(() => {
    console.log("Options being set");
    console.log(options);
    options.forEach((option) => {
      if (!formRefs.current[option.tag]) {
        formRefs.current[option.tag] = React.createRef();
      }
    });
  }, [options]); 

  useEffect(() => {
    console.log("Form Values being set");
    console.log(formValues);
    Object.keys(formRefs.current).forEach((key) => {
      const ref = formRefs.current[key];
      if (ref.current) {
        ref.current.value = formValues[key];
      }
    });
  }, [formValues]);

  const handleSave = () => {
    // Save the form values
  }



  return (
    <Paper style={{ padding: '10px', margin: '2px', height: '100%', overflowY: 'auto', width:'400px' }}>
      <ThemeProvider theme={theme}>
        <Typography variant="h6" gutterBottom>
          Study Details
        </Typography>

        {options.map((option) => (
          <TextField
            key={option.id}
            multiline={true}
            variant="filled"
            id={option.value}
            label={option.value}
            fullWidth
            margin="normal"
            inputRef={formRefs.current[option.tag]}
            onChange={(e) => handleChange(e.target.value, option.tag)}
          />
        ))}

        <Divider style={{ margin: '20px 0' }} />
        <Divider style={{ margin: '10px 0' }} />

        <div style={{marginRight:'10px'}}>
          <Button component={Link} to="/oneproject/:projectId" variant="contained" onClick={handleSave()} startIcon={<SaveIcon />}>Save</Button>
        </div>
        
      </ThemeProvider>
    </Paper>
  );
};

export default FormPlaceholder;