import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import axios from 'axios';
import PersonIcon from '@mui/icons-material/Person';
import './Projects.css';
import SettingsIcon from '@mui/icons-material/Settings';
import { Button } from '@mui/material';

function Projects() {
  const [projects, setProjects] = useState([]);
  const [currentPage, setCurrentPage] = useState(0);
  const projectsPerPage = 7;
  const navigate = useNavigate();

  const fetchProjects = async () => {
    try {
      const token = localStorage.getItem('token');
      console.log("token:", token);
      const response = await axios.get('http://localhost:3000/api/projects/display', {
        headers: { Authorization: `Bearer ${token}` }
      });
      setProjects(response.data.projects);
    } catch (error) {
      console.error('Error fetching projects:', error);
    }
  };

  useEffect(() => {
    fetchProjects();
  }, []);

   const indexOfLastProject = (currentPage + 1) * projectsPerPage;
  const indexOfFirstProject = indexOfLastProject - projectsPerPage;
  const currentProjects = projects.slice(indexOfFirstProject, indexOfLastProject);

  const handlePrevious = () => {
    setCurrentPage(prevPage => Math.max(0, prevPage - 1)); // Prevents going below 0
  };

  const handleNext = () => {
    setCurrentPage(prevPage => Math.min(prevPage + 1, Math.ceil(projects.length / projectsPerPage) - 1)); // Prevents going past the last page
  };

  return (
    <>
    <div className="ProjectsSection">
      {currentProjects.map((project) => (
        <Link to={`/oneproject/${project._id}`} key={project._id} style={{ textDecoration: 'none' }}>
          <div className="ProjectItem">
            <PersonIcon className="PeopleIcon" />
            <span className="ProjectTitle">{project.title}</span>
            <div className="ProjectActions">
              <div className="ProgressContainer">
                <div className="ProgressBar" style={{ width: project.progression ? `${project.progression}%` : '75%' }}></div>
              </div>
              <div className="ProgressPercentage">{project.progression ? `${project.progression}%` : '75%'}</div>
              <Link to="/projectsettings" className="SettingsIcon">
                <SettingsIcon />
              </Link>
            </div>
          </div>
        </Link>
      ))}
      </div>
      <div className="PaginationButtons">
        <Button variant="contained" onClick={handlePrevious} disabled={currentPage === 0}>
          Previous
        </Button>
        <Button variant="contained" onClick={handleNext} disabled={currentPage >= Math.ceil(projects.length / projectsPerPage) - 1}>
          Next
        </Button>
      </div>
      </>
  );
}

export default Projects;