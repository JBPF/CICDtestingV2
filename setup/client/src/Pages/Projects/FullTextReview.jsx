import React, {useEffect, useState} from 'react';
import { Worker, Viewer } from '@react-pdf-viewer/core';
import { defaultLayoutPlugin } from '@react-pdf-viewer/default-layout';
import '@react-pdf-viewer/core/lib/styles/index.css';
import '@react-pdf-viewer/default-layout/lib/styles/index.css';
import { Paper, Button } from '@mui/material';
import '../../App.css';
import UndoIcon from '@mui/icons-material/Undo';
import {Link, useNavigate, useParams} from 'react-router-dom';
import { fullScreenPlugin } from '@react-pdf-viewer/full-screen';
import CriteriaSection from './CriteriaSection';
import "./FullTextReview.css";
import axios from "axios";
import {updateReviewDecision} from "../../services/reviewService";

const FullTextReview = () => {
  const [reviewInstances, setReviewInstances] = useState([]);
  const [currentInstanceId, setCurrentInstanceId] = useState(null);
  const [loading, setLoading] = useState(true);
  const { projectId } = useParams();
  const [articleUrl, setArticleUrl] = useState('');
  const fullScreenPluginInstance = fullScreenPlugin();
  const [criteriaSelections, setCriteriaSelections] = useState([]);



  useEffect(() => {
    const fetchReviewInstances = async () => {
      setLoading(true);
      try {
        const token = localStorage.getItem("authToken");
        const stage = "fullArticleReview";
        const response = await axios.get(`/api/projects/${projectId}/review-instances/${stage}`, {
          headers: { Authorization: `Bearer ${token}` },
        });
        const instances = response.data.reviewInstances.filter(instance => instance.reviewStatus === 'pending');
        setReviewInstances(instances);


        if (instances.length > 0) {
          const currentInstance = instances[0];
          setArticleUrl(currentInstance.article.documentLink);
          setCurrentInstanceId(currentInstance._id);
        }
        setLoading(false);
      } catch (error) {
        console.error("Error fetching review instances:", error);
        setLoading(false);
      }
    };

    fetchReviewInstances();
  }, [projectId]);


  const handleCriteriaChange = (newSelections) => {
    setCriteriaSelections(newSelections);
  };

  const handleDecision = async (currentInstanceId, decision) => {
    if (!currentInstanceId) return;

    if (decision === 'reject' && criteriaSelections.length === 0) {
      alert("Please select at least one exclusion criterion before rejecting.");
      return;
    }

    try {

      const payload = {
        decision,
        criteriaSelections,
      };

      console.log("Payload:", payload);
      await axios.patch(`/api/review-instances/${currentInstanceId}/decision`, payload, {
        headers: { 'Content-Type': 'application/json' },
      });
      const updatedInstances = reviewInstances.filter(instance => instance._id !== currentInstanceId);
      setReviewInstances(updatedInstances);


      if (updatedInstances.length > 0) {
        const nextInstance = updatedInstances[0];
        setArticleUrl(nextInstance.article.documentLink);
        setCurrentInstanceId(nextInstance._id);
      } else {
        setArticleUrl('');
        setCurrentInstanceId(null);
        alert("All documents have been reviewed.");
      }
    } catch (error) {
      console.error("Error updating review decision:", error);
    }
  };


  if (loading) {
    return <div>Loading...</div>;
  }

  if (!loading && reviewInstances.length === 0) {
    return (
        <div className="no-more-reviews">
          <h2>No more documents to review</h2>
        </div>
    );
  }

  return (
    <div className="AppContainer">
      <div className="MainContent">
        <div className="FullText">
          <div className="FullTextHeader">
            <h2 className="FullTextTitle">Full Text Review</h2>
            <div style={{ float: 'right', marginRight: '20px' }}>
              <Button component={Link} to={`/oneproject/${projectId}`} variant="contained" startIcon={<UndoIcon />}>Back</Button>
            </div>
          </div>


          <div style={{ display: 'flex', height: '100%', marginTop: '30px' }}>
            {/* PDF Viewer */}
            <div style={{ flex: 1, minWidth: '700px' , }}>
              <Paper elevation={3} style={{ height: '84vh', marginRight: '10px', flexGrow: 1  }}>
                <Worker workerUrl="https://unpkg.com/pdfjs-dist@3.11.174/build/pdf.worker.min.js">
                  <Viewer
                    fileUrl={articleUrl}
                    plugins={[fullScreenPluginInstance]} />
                </Worker>
              </Paper>
            </div>

            {/* Buttons */}
            <div style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              gap: '20px',
              marginRight: '20px',
              marginTop: '20px'
            }}>
              <div className="AcceptDeleteButtons">
                <Button variant="outlined" size="large"
                        onClick={() => handleDecision(currentInstanceId, 'accept')}>Accept</Button>
                <Button variant="outlined" size="large"
                        onClick={() => handleDecision(currentInstanceId, 'reject')}>Reject</Button>
              </div>
              <div className="CriteriaContainer" >
              <CriteriaSection projectId={projectId} onCriteriaChange={handleCriteriaChange} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  );
};

export default FullTextReview;