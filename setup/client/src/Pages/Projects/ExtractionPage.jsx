import React, {useState, useEffect, useRef} from 'react';
import axios from 'axios';
import { Worker, Viewer } from '@react-pdf-viewer/core';
import '@react-pdf-viewer/core/lib/styles/index.css';
import '@react-pdf-viewer/default-layout/lib/styles/index.css';
import { Paper, Button, CircularProgress } from '@mui/material';
import '../../App.css';
import UndoIcon from '@mui/icons-material/Undo';
import { Link, useParams } from 'react-router-dom';
import FormPlaceholder from './FormPlaceholder';
import './ExtractionPage.css';
import optionsData from './form.json';
import { Trigger, highlightPlugin } from '@react-pdf-viewer/highlight';
import _uniqueId from 'lodash/uniqueId';

const ExtractionPage = () => {
  const [selection, setSelection] = useState('');
  const [buttonPosition, setButtonPosition] = useState({ x: 0, y: 0 });
  const [showButton, setShowButton] = useState(false);
  const buttonRef = useRef(null);
  const [showOptions, setShowOptions] = useState(false);
  const pdfViewerRef = useRef(null);
  const [options, setOptions] = useState(optionsData);
  const { projectId } = useParams();
  const [test, setTest] = useState('');
  const [loading, setLoading] = useState(true);
  const [transformedOptions, setTransformedOptions] = useState(options);

  const [formValues, setFormValues] = useState({
    sponsorshipValue: '',
    countryValue: '',
    settingValue: '',
    commentsValue: '',
    authorNameValue: '',
    institutionValue: '',
    emailValue: '',
    addressValue: '',
  });


useEffect(() => {
  const fetchExtractionVariables = async () => {
    try {
      setLoading(true);
      const studiesResponse = await axios.get(`http://localhost:3000/api/projects/${projectId}/extractionVariables`).then((response) => {
        setOptions(response.data.extractionVariables);
        });
    } catch (error) {
      console.error('Error fetching extraction variables:', error);
    } 
  };

  fetchExtractionVariables();
}, [projectId]);

useEffect(() => {
  if (options.length > 0) {
    console.log("looping")
    var change = false;
    const updatedOptions = options.map(option => {
      if (!option.tag) {
        change = true;
        return { ...option, tag: _uniqueId('tag-') };
      } else {
        return {...option};
      }
    });

    const initialFormValues = updatedOptions.reduce((acc, option) => {
      return { ...acc, [option.tag]: '' };
    }, {});

    setFormValues(initialFormValues);

    if (change) { 
      setOptions(updatedOptions);
      console.log(updatedOptions)
      console.log(initialFormValues)
    }
  }

  setLoading(false);
}, [options]); 
  
  const highlightPluginInstance = highlightPlugin({
    trigger: Trigger.None,
  });


  const handleChange = (value, prop) => {
    setFormValues((prevValues) => ({
      ...prevValues,
      [prop]: value,
    }));
  };

  const handleInsert = (e) => {
    setFormValues((prevValues) => ({
      ...prevValues,
      [e]: formValues[e]+selection,
    }));
  }
  

  const handleSelectionChange = () => {
    const selection = window.getSelection();
    const text = selection.toString().trim();
    if (selection.rangeCount > 0 && selection.toString().trim() !== '') {
      const range = selection.getRangeAt(0);
      const pdfViewer = pdfViewerRef.current;
      
      if (pdfViewer && pdfViewer.contains(range.commonAncestorContainer)) {
        setSelection(text);
        const range = selection.getRangeAt(0);
        const rect = range.getBoundingClientRect();
        setButtonPosition({ x: rect.right, y: rect.top });
        setShowButton(true);
      } else {
        setShowButton(false);
        setShowOptions(false);
      }
    }
  };

    useEffect(() => {
      const handleClickOutside = (event) => {
          if (buttonRef.current && !buttonRef.current.contains(event.target)) {
              setShowButton(false);
              setShowOptions(false);
          }
      };

      document.addEventListener('mouseup', handleSelectionChange);
      document.addEventListener('mousedown', handleClickOutside);

      return () => {
          document.removeEventListener('mouseup', handleSelectionChange);
          document.removeEventListener('mousedown', handleClickOutside);
      };
  }, []);

  const toggleOptions = () => {
    setShowOptions(!showOptions);
};

  return (
<div className="AppContainer">
      <div className="MainContent">
        <div className="ExtractionPage">
          <div className="ExtractionHeader">
            <h2 className="ExtractionTitle">{test}</h2>
            <div style={{ float: 'right', marginRight: '20px' }}>
              <Button component={Link} to="/oneproject/:projectId" variant="contained" startIcon={<UndoIcon />}>Back</Button>
            </div>
          </div>

          <div className="ExtractionBody">
            <div style={{ flex: 1 }}>
              <Paper elevation={3} style={{ height: '100vh', marginRight: '10px' }}>
                <Worker workerUrl="https://unpkg.com/pdfjs-dist@3.11.174/build/pdf.worker.min.js">
                  <div ref={pdfViewerRef}>
                    {<Viewer
                    
                      fileUrl={`${process.env.PUBLIC_URL}/pdfarticle.pdf`}
                      plugins={[highlightPluginInstance]}
                    
                    />}
                  </div>
                  {showButton && selection && (
                      <Button
                          style={{
                              position: 'absolute',
                              fontStyle: 'bold',
                              backgroundColor: 'white',
                              left: `${buttonPosition.x}px`,
                              top: `${buttonPosition.y}px`,
                          }}
                          onClick= {toggleOptions}
                          ref={buttonRef}
                      >
                          Select
                      </Button>
                  )}
                  {showOptions && !loading && (
                    <div style={{ position: 'absolute', left: `${buttonPosition.x}px`, top: `${buttonPosition.y + 20}px` }}>
                    <div ref={buttonRef} style={{ padding: 0, margin: '5px 0 0', backgroundColor: '#fff', border: '1px solid #ddd', borderRadius: '4px'}}>
                      
                        {options.map(option => (
                            <button
                                key={option.id}
                                style={{
                                    display: 'block', 
                                    width: '100%', 
                                    padding: '5px 10px',
                                    border: 'none', 
                                    borderRadius: '4px', 
                                    backgroundColor: '#f0f0f0', 
                                    textAlign: 'left', 
                                    margin: '2px 0', 
                                    cursor: 'pointer'
                                }}
                                onClick={() => handleInsert(option.tag)}
                            >
                              

                                {option.value}
                            </button>
                        ))}
                    </div>
                </div>
                
                    )}
                </Worker>
              </Paper>
            </div>

            {/* Form Placeholder */}
            <div style={{ width: '400px', height: '100vh', overflow: 'auto' }}>
            {!loading && <FormPlaceholder handleChange={handleChange} options={options} formValues={formValues} />}
            </div>
          </div>
      
        </div>
      </div>
    </div>
  
  );
};

export default ExtractionPage;