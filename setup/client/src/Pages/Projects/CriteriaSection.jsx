import React, { useEffect, useState } from 'react';
import { Paper, FormGroup, FormControlLabel, Checkbox, TextField, IconButton } from '@mui/material';
import CheckIcon from '@mui/icons-material/Check';
import CloseIcon from '@mui/icons-material/Close';
import SearchIcon from '@mui/icons-material/Search';
import axios from 'axios';
import './CriteriaSection.css';
import { Typography } from '@mui/material';

const CriteriaSection = ({ projectId, onCriteriaChange }) => {
  const [inclusionCriteria, setInclusionCriteria] = useState([]);
  const [exclusionCriteria, setExclusionCriteria] = useState([]);
  const [inclusionSearchTerm, setInclusionSearchTerm] = useState('');
  const [exclusionSearchTerm, setExclusionSearchTerm] = useState('');
  const [exclusionSelections, setExclusionSelections] = useState([]);

  useEffect(() => {
    const fetchCriteria = async () => {
      try {
        const response = await axios.get(`/api/projects/${projectId}/criteria`);
        setInclusionCriteria(response.data.inclusionCriteria);
        setExclusionCriteria(response.data.exclusionCriteria);
        const initialExclusionSelections = response.data.exclusionCriteria.map(criterion => ({
          criterionId: criterion.id,
          selected: false
        }));
        setExclusionSelections(initialExclusionSelections);


      } catch (error) {
        console.error('Error fetching criteria:', error);
      }
    };
    fetchCriteria();
  }, [projectId]);

  console.log("exclusionCriteria", exclusionCriteria);

  const handleExclusionCheckboxChange = (criterionId, isSelected) => {
    const updatedSelections = exclusionSelections.map(selection =>
      selection.criterionId === criterionId ? { ...selection, selected: isSelected } : selection
    );
    setExclusionSelections(updatedSelections);

    // Filter and send back only selected criteria
    const selectedCriteria = updatedSelections.filter(selection => selection.selected);
    onCriteriaChange(selectedCriteria); // Assuming the parent component expects only selected criteria
  };

  const filteredInclusionCriteria = inclusionCriteria.filter(criterion =>
    criterion.value.toLowerCase().includes(inclusionSearchTerm.toLowerCase())
  );

  const filteredExclusionCriteria = exclusionCriteria.filter(criterion =>
    criterion.value.toLowerCase().includes(exclusionSearchTerm.toLowerCase())
  );


  return (
    <div variant="outlined" className="Criteria">
      <div className="CriteriaHeader">
        <h3 className="CriteriaTitle">Review Criteria</h3>
      </div>
      <div className="CriteriaContainerStyle">
        <div className="IndividualCriteriaStyle">
          <div className="IndividualCriteriaHeader">
            <CheckIcon className="AcceptCriteriaIcon" />
            <span className="IndividualCriteriaTitle">Accept Criteria</span>
          </div>
          <div className="SearchCriteria">
            <div className="Search">
              <TextField
                size="small"
                placeholder="Search"
                fullWidth="True"
                value={inclusionSearchTerm}
                onChange={(e) => setInclusionSearchTerm(e.target.value)}
                InputProps={{
                  startAdornment: (
                    <SearchIcon sx={{ color: 'grey' }} />
                  ),
                }}
              />
            </div>
          </div>
          <div className="CriteriaElementsAccept">
            {filteredInclusionCriteria.map((criterion, index) => (
              <div className="IndividualCriteria" key={index}>{criterion.value}</div>
            ))}
          </div>
        </div>


        <div className="IndividualCriteriaStyle">
          <div className="IndividualCriteriaHeader">
            <CloseIcon className="RejectCriteriaIcon" />
            <span className="IndividualCriteriaTitle">Decline Criteria</span>
          </div>
          <div className="SearchCriteria">
            <div className="Search">
              <TextField
                size="small"
                placeholder="Search"
                fullWidth="True"
                value={exclusionSearchTerm}
                onChange={(e) => setExclusionSearchTerm(e.target.value)}
                InputProps={{
                  startAdornment: (
                    <SearchIcon sx={{ color: 'grey' }} />
                  ),
                }}
              />
            </div>
          </div>


          <div className="CriteriaElementsReject">
            <FormGroup>
              {filteredExclusionCriteria.map((criterion, index) => (
                <FormControlLabel
                  key={criterion.id}
                  control={
                    <Checkbox
                      checked={exclusionSelections.find(sel => sel.criterionId === criterion.id)?.selected || false}
                      onChange={(e) => handleExclusionCheckboxChange(criterion.id, e.target.checked)}
                      size="medium"
                      sx={{
                        marginTop: '-10px',
                        '&.Mui-checked': {
                          color: "rgb(27, 84, 116)",
                        },
                      }}
                    />
                  }
                  label={
                    <Typography variant="body2" sx={{ fontFamily: 'inherit', fontSize: 'inherit' }}>
                      {criterion.value}
                    </Typography>
                  }
                  sx={{
                    alignItems: 'flex-start',
                    '& .MuiTypography-body2': { fontFamily: 'inherit', fontSize: 'inherit' },
                    margin: 0,
                    '& .MuiCheckbox-root': { padding: '8px' },
                    '& .MuiFormControlLabel-label': { fontFamily: 'inherit', color: 'inherit', fontSize: '1rem' }
                  }}
                />
              ))}
            </FormGroup>
          </div>



        </div>
      </div>
    </div>
  );
};

export default CriteriaSection;