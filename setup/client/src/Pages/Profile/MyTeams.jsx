import React, { useState, useEffect } from 'react';
import { message } from 'antd';
import { getTeamDetails } from '../../services/teamsService';
import CreateTeamModal from '../../Components/MyTeamsForm/CreateTeamModal';
import { Box, Card, Container, Grid, Paper, Typography } from '@mui/material';
import PageHeader from '../../Components/MyTeamsForm/PageHeader';
import { DashboardLayout } from '../../Components/MyTeamsForm/DashboardLayout';
import { grey } from '@mui/material/colors';
import { fetchUsers } from '../../services/userService';
import { fetchRoles } from '../../services/roleService';
import EditTeamModal from '../../Components/MyTeamsForm/EditTeamModal';





const MyTeams = () => {

  // State for roles
  const [teams, setTeams] = useState([]); 


  const [isTeamModalOpen, setIsTeamModalOpen] = useState(false);


  useEffect(() => {
    const loadTeams = async () => {
      try {
        setIsLoading(true);
        const fetchedTeams = await getTeamDetails();
        setTeams(fetchedTeams);
      } catch (error) {
        message.error('Failed to load teams: ' + error.message);
      } finally {
        setIsLoading(false);
      }
    };

    loadTeams();
  }, []);


  const [users, setUsers] = useState([]); // To store users for selection
  const [roles, setRoles] = useState([]);

  const [selectedTeam, setSelectedTeam] = useState(undefined); 

  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const loadInitialData = async () => {
      try {
        const [fetchedUsers, fetchedRoles] = await Promise.all([
          fetchUsers(), fetchRoles()
        ])
        setUsers(fetchedUsers);
        setRoles(fetchedRoles);
      } catch (error) {
        message.error('Failed to load initial data: ' + error.message);
      }
    };

    loadInitialData();
  }, []);





  return (
    <DashboardLayout>

      <CreateTeamModal isOpen={isTeamModalOpen} onClose={() => setIsTeamModalOpen(false)} data={{
        users,
        roles
      }} />

<EditTeamModal
  isOpen={Boolean(selectedTeam)}
  onClose={() => setSelectedTeam(undefined)}
  data={{ users, roles, selectedTeam }}
  onUpdate={(updatedTeam) => {
    setTeams(teams.map(team => team._id === updatedTeam._id ? updatedTeam : team));
    setSelectedTeam(undefined); // Optionally reset the selected team
  }}
/>




      <Container width="100%" maxH={"100vh"}>

        <PageHeader title="My Teams" onButtonClick={() => setIsTeamModalOpen(true)} />


        <Box flex={1} position={"flex"} width={"100%"} pt={"50px"}>

          <Grid container spacing={3}>
            {teams.map(team => (
              <Grid item height={"200px"} xs={12} sm={6} md={4} lg={3} key={team._id}>
                <Paper
                  onClick={() => setSelectedTeam(team)}
                  sx={{
                    backgroundColor: grey[100],
                    minHeight: '100%',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    position: "relative",
                    '&:hover': {
                      backgroundColor: grey[300],
                      cursor: 'pointer'
                    }
                  }}
                >
                  <div style={{ width: "100%", display: "flex", flexDirection: "column", gap: "10px", }} >
                    <Typography color={grey[700]} textAlign={"center"} variant="h5">{team.name}</Typography>
                    <Typography width={"100%"} color={grey[500]} textAlign={"center"} variant='p'>{team.description}</Typography>
                  </div>
                </Paper>
              </Grid>
            ))}
          </Grid>
        </Box>

      </Container>

    </DashboardLayout>
  );
};

export default MyTeams;