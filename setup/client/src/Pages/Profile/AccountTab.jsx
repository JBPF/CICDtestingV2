import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Box, Grid, List, ListItem, ListItemText, Typography, Avatar, TextField, Button, Chip, Tooltip, Paper} from '@mui/material';
import CancelIcon from '@mui/icons-material/Cancel';
import EditIcon from '@mui/icons-material/Edit';


function AccountTab() {
  const [userInfo, setUserInfo] = useState({
    email: '',
    username: '',
    avatar: '',
  });

  const [interests, setInterests] = useState([]);
  const [newInterest, setNewInterest] = useState('');
  const DEFAULT_AVATAR_URL = '';

  const [passwordFields, setPasswordFields] = useState({
    currentPassword: '',
    newPassword: '',
    confirmNewPassword: '',
  });

  useEffect(() => {
    const fetchUserInfo = async () => {
      const token = localStorage.getItem('token');
      if (token) {
        try {
          const response = await axios.get('http://localhost:3000/api/user/info', {
            headers: {
              Authorization: `Bearer ${token}`
            }
          });
          setUserInfo({
            email: response.data.email,
            username: response.data.name,
            avatar: response.data.avatar,
          });
        } catch (error) {
          console.error("Error fetching user info:", error);
        }
      }
    };

    fetchUserInfo();
  }, []);

  const handleAvatarChange = (event) => {
    const file = event.target.files[0];
    if (file) {
      // upload the file to your server and fetch the URL?
      const newAvatarUrl = URL.createObjectURL(file);
      setUserInfo({ ...userInfo, avatar: newAvatarUrl });
    }
  };

  const handleRemoveAvatar = () => {
    setUserInfo({ ...userInfo, avatar: DEFAULT_AVATAR_URL });
  };

  const handlePasswordChange = (e) => {
    const { name, value } = e.target;
    setPasswordFields({ ...passwordFields, [name]: value });
  };

  const [teamInfo, setTeamInfo] = useState([
    {
      id: 1,
      name: 'Team A',
      members: [
        { id: 1, name: 'Stella', role: 'Reviewer' },
        { id: 2, name: 'Anthony', role: 'Reviewer' },
      ],
    },
    {
      id: 2,
      name: 'Team B',
      members: [
        { id: 3, name: 'Tim', role: 'Resolver' },
        { id: 4, name: 'Gavin', role: 'Resolver' },
        // Add more members if needed
      ],
    },
    // Add more if user is part of more teams
  ]);

  const handleAddInterest = () => {
    if (newInterest && !interests.some(interest => interest.topic.toLowerCase() === newInterest.toLowerCase())) {
      setInterests(prevInterests => [...prevInterests, { id: Date.now(), topic: newInterest }]);
      setNewInterest('');
    }
  };

  const handleRemoveInterest = (id) => {
    setInterests(interests.filter(interest => interest.id !== id));
  };

  const handleSubmit = () => {
    console.log('Password change requested:', passwordFields);
    alert('Password change is under construction.');
    setPasswordFields({
      currentPassword: '',
      newPassword: '', 
      confirmNewPassword: '',
    });
  };

  return (
    <Box sx={{ p: 2 }}>
      <Paper sx={{ p: 2, mb: 2, backgroundColor: "rgb(243,243,243)", position: 'relative', border: "2px solid #ccc", borderRadius: "15px"}}>
        <Grid container spacing={2} alignItems="center">
          <Grid item>
            <Avatar src={userInfo.avatar} alt={userInfo.username} sx={{ width: 100, height: 100 }} />
            {userInfo.avatar && (
              <Button
                variant="outlined"
                color="secondary"
                onClick={handleRemoveAvatar}
                sx={{ mt: 1 }}
              >
                Remove Icon
              </Button>
            )}
          </Grid>
          <Grid item xs>
            <Typography variant="h6">User name: {userInfo.username}</Typography>
            <Typography variant="h6">Email: {userInfo.email}</Typography>
          </Grid>
          <Box sx={{ position: 'absolute', top: '50%', right: '16px', transform: 'translateY(-50%)' }}>
            <Tooltip title="Edit Profile">
              <Button
                variant="contained"
                color="primary"
                startIcon={<EditIcon />}
                component="label"
              >
                Edit Icon
                <input
                  accept="image/*"
                  style={{ display: 'none' }}
                  id="avatar-upload"
                  type="file"
                  onChange={handleAvatarChange}
                />
              </Button>
            </Tooltip>
          </Box>
        </Grid>
      </Paper>
      <Paper sx={{ p: 2, mb: 2, backgroundColor: "rgb(243,243,243)" }}>
        <Typography variant="h4" sx={{ mt: 2, mb: 2 }}>Teams</Typography>
        {teamInfo.map((team) => (
          <Box key={team.id} sx={{ mb: 1 }}>
            <Paper sx={{ borderRadius: "8px", backgroundColor: "lightgray" }}>
            <Typography variant="h6" sx={{ mt: 1, mb: 1, textAlign: 'center' }}>{team.name}</Typography>
            </Paper>
            <List>
              {team.members.map((member) => (
                <ListItem key={member.id}>
                  <ListItemText primary={`${member.name} - ${member.role}`} />
                </ListItem>
              ))}
            </List>
          </Box>
        ))}
      </Paper>
      <Paper sx={{ p: 2, mb: 2, backgroundColor: "rgb(243,243,243)" }}>
      <Typography variant="h4" sx={{ mt: 2, mb: 2 }}>Fields of Interest</Typography>
      <Box sx={{ display: 'flex', flexDirection: 'column', gap: 1, mb: 2 }}>
        {interests.map((interest) => (
          <Chip
            key={interest.id}
            label={interest.topic}
            onDelete={() => handleRemoveInterest(interest.id)}
            deleteIcon={<CancelIcon style={{ color: 'red' }} />}
          />
        ))}
      </Box>
      <Box sx={{ mt: 2, display: 'flex', alignItems: 'center', width: '100%' }}>
        <TextField
          value={newInterest}
          onChange={(e) => setNewInterest(e.target.value)}
          onKeyPress={(e) => {
            if (e.key === 'Enter') {
              e.preventDefault(); // make the input be displayed when pressing enter as well
              handleAddInterest();
            }
          }}
          variant="outlined"
          size="small"
          fullWidth
          sx={{ 
            mr: 1, 
            backgroundColor: '#fff', 
            '& .MuiOutlinedInput-input': {
              color: 'black'
            },
            }
          }
        />
        <Button variant="contained" onClick={handleAddInterest}>Add Interest</Button>
      </Box>
      </Paper>
      <Paper sx={{ p: 2, mb: 2, backgroundColor: "rgb(243,243,243)" }}>
      <Typography variant="h4" sx={{ mt: 2, mb: 2 }}>Change Password</Typography>
      <Box sx={{ display: 'flex', flexDirection: 'column', gap: 2, maxWidth: 300 }}>
        <TextField
          label="Current Password"
          type="password"
          variant="filled"
          name="currentPassword"
          value={passwordFields.currentPassword}
          onChange={handlePasswordChange}
          fullWidth
        />
        <TextField
          label="New Password"
          type="password"
          variant="filled"
          name="newPassword"
          value={passwordFields.newPassword}
          onChange={handlePasswordChange}
          fullWidth
        />
        <TextField
          label="Confirm New Password"
          type="password"
          variant="filled"
          name="confirmNewPassword"
          value={passwordFields.confirmNewPassword}
          onChange={handlePasswordChange}
          fullWidth
        />
        <Button variant="contained" onClick={handleSubmit}>Submit</Button>
      </Box>
      </Paper>
    </Box>
  );
}

export default AccountTab;
