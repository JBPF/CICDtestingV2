import React, { useState } from 'react';
import { Box, TextField, Button, Checkbox, FormControlLabel, Typography, IconButton, Paper } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';

function TodoListTab() {
  const [todos, setTodos] = useState([]);
  const [newTodo, setNewTodo] = useState('');

  const handleAddTodo = () => {
    if (newTodo.trim() !== '') {
      setTodos([...todos, { id: Date.now(), text: newTodo, completed: false }]);
      setNewTodo('');
    }
  };

  const handleToggleComplete = (id) => {
    setTodos(
      todos.map((todo) => 
        todo.id === id ? { ...todo, completed: !todo.completed } : todo
      )
    );
  };

  const handleDeleteTodo = (id) => {
    setTodos(todos.filter(todo => todo.id !== id));
  };

  return (
    <Box sx={{ p: 2 }}>
      <TextField
        label="Add To-Do Item"
        variant="filled"
        value={newTodo}
        onChange={(e) => setNewTodo(e.target.value)}
        onKeyPress={(e) => {
          if (e.key === 'Enter') {
            e.preventDefault();
            handleAddTodo();
          }
        }}
        fullWidth
        margin="normal"
      />
      <Button onClick={handleAddTodo} variant="contained" sx={{ mt: 2, mb: 4 }}>Add Task</Button>
      <Paper sx={{p: 1, mt: 2, textAlign: 'center', backgroundColor: "rgb(243,243,243)"}}>
      <Typography variant="h5" sx={{ mt: 1, textAlign: 'center'}}>Unfinished tasks</Typography>
      {todos.filter(todo => !todo.completed).length > 0 ? (
      <div>
        {todos.filter(todo => !todo.completed).map((todo) => (
          <Box key={todo.id} display="flex" alignItems="center">
          <FormControlLabel
            control={
              <Checkbox 
                checked={todo.completed} 
                onChange={() => handleToggleComplete(todo.id)} 
              />
            }
            label={todo.text}
            key={todo.id}
            sx={{ display: 'block' }}
          />
          </Box>
        ))}
      </div>
      ) : (
        <Typography sx={{ mt: 2, fontStyle: 'italic' }}>You have no unfinished tasks</Typography>
      )}
      </Paper>
      <Paper sx={{p: 1, mt: 2, textAlign: 'center', backgroundColor: "rgb(243,243,243)"}}>
      <Typography variant="h5" sx={{ mt: 1, textAlign: 'center' }}>Finished tasks</Typography>
      {todos.filter(todo => todo.completed).length > 0 ? (
      <div>
        {todos.filter(todo => todo.completed).map((todo) => (
          <Box key={todo.id} display="flex" alignItems="center">
          <FormControlLabel
            control={
              <Checkbox 
                checked={todo.completed} 
                onChange={() => handleToggleComplete(todo.id)} 
              />
            }
            label={todo.text}
            key={todo.id}
            sx={{ display: 'block' }}
          />
          <IconButton onClick={() => handleDeleteTodo(todo.id)} edge="end" aria-label="delete">
                <DeleteIcon />
          </IconButton>
          </Box>
        ))}
      </div>
      ) : (
        <Typography sx={{ mt: 2, fontStyle: 'italic' }}>You have no finished tasks</Typography>
      )}
      </Paper>
    </Box>
  );
}

export default TodoListTab;
