import React from 'react';
import { Box, List, ListItem, ListItemText, Divider, Paper } from '@mui/material';

function ActivityTab() {
  
  const activities = [
    { date: '2023-02-10 14:00', activity: 'Accepted review for project A' },
    { date: '2023-02-12 09:30', activity: 'Completed data extraction for project B' },
    { date: '2024-02-12 11:30', activity: 'Resolved conflicts for project C' },
  ];

  return (
    <Box sx={{ p: 2 }}>
      <h3>Activity</h3>
      <List>
        {activities.map((activity, index) => (
          <React.Fragment key={index}>
            <Paper sx={{ mb: 1, p: 1, backgroundColor: "rgb(243,243,243)"}}>
            <ListItem>
              <ListItemText primary={activity.activity} secondary={activity.date} />
            </ListItem>
            </Paper>
            {index < activities.length - 1 && <Divider />}
          </React.Fragment>
        ))}
      </List>
    </Box>
  );
}

export default ActivityTab;
