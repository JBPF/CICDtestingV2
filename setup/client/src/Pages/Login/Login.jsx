import React, { useEffect, useState } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import axios from 'axios';
import './Login.css'
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBInput,
  MDBBtn,
} from 'mdb-react-ui-kit';

const Login = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState(null);
    const navigate = useNavigate()
    const [animate, setAnimate] = useState(false);

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const { data } = await axios.post('http://localhost:3000/users/login', { email, password });
            if (data.token) {
                localStorage.setItem('token', data.token);
                navigate('/dashboard');
            } else {
                setError('Login failed. Please check your credentials.');
            }
        } catch (err) {
            // Here we are assuming the server sends back an error message as part of the response body
            const errorMessage = err.response?.data?.message || 'An error occurred during login.';
            setError(errorMessage);
        }
    };


    useEffect(() => {
        setAnimate(true);
      }, []);
    

    return (
        <MDBContainer className="my-5 d-flex justify-content-center align-items-center vh-100" style={{ maxWidth: "85%",  transform: 'translateY(3vh)'}}>
    <MDBRow style={{ marginRight: '-25px', marginLeft: '-25px' }}>
    <MDBCol size='12' md='5' style={{ paddingRight: '0px', marginRight: '-15px', height: '680px'}}>
            <div className="text-center">
            <div className="logo logo-animate">
                    <img src="/datasky_logo.png" alt="logo" style={{ maxWidth: "60%", height: "auto" }} />
                </div>
                <h4 className="mt-1 mb-5 pb-1">Welcome to your Systematic Literature Review Application</h4>
            </div>
            <h4>Please login to your account</h4>

            {error && (
                <div className="alert alert-danger" role="alert">
                    {error}
                </div>
            )}

            <form onSubmit={handleSubmit}>
                <MDBInput wrapperClass='mb-4' label='Email address' id='form1' type='email' onChange={(e) => setEmail(e.target.value)}/>
                <MDBInput wrapperClass='mb-4' label='Password' id='form2' type='password' onChange={(e) => setPassword(e.target.value)}/>

                <button type="submit" className="mb-4 w-100 blue-gradient-btn">Log in</button>
            </form>
            <div className="text-center pt-1 mb-5 pb-1">
                <p>Don't have an account?</p>
                <Link to="/register" className="text-decoration-none">
                    <MDBBtn color='link'>Register</MDBBtn>
                </Link>
            </div>
        </MDBCol>
        <MDBCol col='6' md='5'  className={`mb-5 ${animate ? 'slide-in' : ''}`} style={{ maxWidth: "50%"}}>
            <div className="d-flex flex-column  justify-content-center gradient-custom-2 h-100 mb-4">
                <div className="text-white px-3 py-4 p-md-5 mx-md-4">
                    <h3 class="mb-4">More than just reviewing</h3>
                    <p class="small mb-0">Data Sky's Systematic Literature Review is not just a tool of reviewing articles but also gives you the 
                    opportunity to work as a team and collaborate with your colleagues in real time. Log in and try the experience!
                    </p>
                </div>
            </div>
        </MDBCol>
    </MDBRow>
</MDBContainer>
    );
};

export default Login;