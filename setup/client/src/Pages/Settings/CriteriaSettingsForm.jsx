
import React, { useState, useEffect } from 'react';
import { Form, Input, message, ConfigProvider } from 'antd';
import axios from 'axios';
import DeleteIcon from '@mui/icons-material/Delete';
import AddIcon from '@mui/icons-material/Add';
import TextArea from 'antd/es/input/TextArea';
import { Paper } from '@mui/material';
import './ProjectSettings.css'
import './CriteriaSettings.css';


const ProjectForm = () => {
  const [projectData, setProjectData] = useState({
    tag: '',
    include: '',
    exclude: '',
  });

  const [tagList, setTagList] = useState([
    'Wrong study design',
    'Wrong setting',
    'Wrong route of administration',
    'Wrong dose',
    'Adult population',
  ]);

  const [includeList, setIncludeList] = useState([
    'healthcare',
    'data',
    'librarian',
  ]);

  const [excludeList, setExcludeList] = useState([
    'covid19',
    'adult', 
  ]);

  const [loading, setLoading] = useState(false);
  const [currentReviewName, setCurrentReviewName] = useState('');
  useEffect(() => {
    const fetchReviewName = async () => {
      try {
        const response = await axios.get('/api/getCurrentReviewName');
        setCurrentReviewName(response.data.reviewName);
      } catch (error) {
        console.error('Error fetching current review name:', error);
      }
    };
    fetchReviewName();
  }, []);
  const handleChange = (key, value) => {
    setProjectData({ ...projectData, [key]: value });
  };
  const handleAddTag = () => {
    if (projectData.tag) {
      setTagList([...tagList, projectData.tag]);
      setProjectData({ tag: '' });
    }
  };
  const handleDeleteTag = (index) => {
    const updatedTagList = [...tagList];
    updatedTagList.splice(index, 1);
    setTagList(updatedTagList);
  };
  const handleAddInclude = () => {
    if (projectData.include) {
      setIncludeList([...includeList, projectData.include]);
      setProjectData({ include: '' });
    }
  };
  const handleDeleteInclude = (index) => {
    const updatedIncludeList = [...includeList];
    updatedIncludeList.splice(index, 1);
    setIncludeList(updatedIncludeList);
  };
  const handleAddExclude = () => {
    if (projectData.exclude) {
      setExcludeList([...excludeList, projectData.exclude]);
      setProjectData({ exclude: '' });
    }
  };
  const handleDeleteExclude = (index) => {
    const updatedExcludeList = [...excludeList];
    updatedExcludeList.splice(index, 1);
    setExcludeList(updatedExcludeList);
  };
  const handleSubmit = async () => {
    setLoading(true);
    try {
      message.success('Project details updated successfully!');
      setLoading(false);
    } catch (error) {
      console.error('Error updating project details:', error);
      setLoading(false);
      message.error('Failed to update project details. Please try again.');
    }
  };
  return (
    <div className="IndividualSettings">

      Manage criteria for screening and full text review
      
      <Form onFinish={handleSubmit} layout="vertical">
      <Paper className="CriteriaPaper" variant="outlined" sx={{ backgroundColor: "rgb(243,243,243)", border: "0px solid" }}>
      <div className="InclusionExclusionCriteria">
        <div className="InclusionCriteria">

            <Form.Item label="Inclusion criteria:">
              <TextArea value={projectData.include} placeholder="healthcare analytics, data, librarian" style={{ width: 300}} onChange={(e) => handleChange('include', e.target.value)} />
              </Form.Item>
        </div>
      <div className="ExclusionCriteria">
        <Form.Item label="Exclusion criteria:">
          <TextArea value={projectData.exclude} placeholder="COVID-19" style={{ width: 300 }} onChange={(e) => handleChange('exclude', e.target.value)} />
          </Form.Item>
      </div>
      </div>
          <div style={{  marginTop: '10px', marginBottom: '10px'}}>

          <button className="btn btn-success">
                Save
         </button>
        
         </div>
         </Paper>

         <div className="SettingsSubTitle"> Edit exclusion reasons </div>
         <Paper className="ExclusionReasonsPaper" variant="outlined" sx={{ backgroundColor: "rgb(243,243,243)", border: "0px solid" }}>
         <Form.Item>
            <div className="ExclusionReasonsHeader">
              
              <Input className="AddExclusionInput" value={projectData.tag} placeholder="Add exclusion reason" style={{ width: 400 }} onChange={(e) => handleChange('tag', e.target.value)} />
              <div className="AddReasonButton">
                <button className="btn btn-success" onClick={() => handleAddTag()}>
                      <AddIcon /> Add Reason
              </button>
              </div>
              </div>
              </Form.Item>
         
        <Paper className="ExclusionReasonsBody" variant="outlined" sx={{ backgroundColor: "rgb(233,233,233)", border: "0px solid" }}>
          {tagList.map((tag, index) => (
            <Paper className="ExclusionReasonElements" variant="outlined" sx={{ backgroundColor: "white", border: "0px solid" }} key={index}>
              <div className="ExclusionReasonTitle">
              {tag}
              </div>
              <div className="ExclusionReasonDeleteButton">
              <button className="btn btn-danger btn-sm" onClick={() => handleDeleteTag(index)}>
                <DeleteIcon />
              </button>
              </div>
            </Paper>
          ))}
        </Paper>
        <button className="btn btn-success" style={{marginTop: '10px', marginBottom:'10px'}}>
                Done
         </button>
         </Paper>


         <div className="SettingsSubTitle">
                Manage Highlights
         </div>
         <Paper className="KeywordBody" variant="outlined" sx={{ backgroundColor: "rgb(243, 243, 243)", border: "0px solid" }}>
          <div className="KeywordBodyFlexDisplay">
    <div className="InclusionKeywords">
        <div className="KeywordSection">
            <div style={{ fontSize: '20pt' }}>Keywords for Inclusion</div>
            <Form.Item>
                <Input value={projectData.include} placeholder="Add included keyword" style={{ width: '100%' }} onChange={(e) => handleChange('include', e.target.value)} />
            </Form.Item>
            <div className="AddKeywordButton">
                <button className="btn btn-success" onClick={() => handleAddInclude()}>
                    <AddIcon /> Add Keyword
                </button>
            </div>
        </div>
        <Paper className="KeywordElementPaper" variant="outlined" sx={{ backgroundColor: "rgb(233, 233, 233)", border: "0px solid" }}>
          {includeList.map((include, index) => (
            <Paper className="KeywordElements" variant="outlined" sx={{ backgroundColor: "white", border: "0px solid" }} key={index}>
              <div className="KeywordTitle">
                {include}
              </div>
              <div className="DeleteButton">
                <button className="btn btn-danger btn-sm" onClick={() => handleDeleteInclude(index)}>
                  <DeleteIcon />
                </button>
              </div>
            </Paper>
          ))}
          </Paper> 
        </div>

        <div className="ExclusionKeywords">
        <div className="KeywordSection">
            <div style={{ fontSize: '20pt' }}>Keywords for Exclusion</div>
            <Form.Item>
                <Input value={projectData.exclude} placeholder="Add excluded keyword" style={{ width: '100%' }} onChange={(e) => handleChange('exclude', e.target.value)} />
            </Form.Item>
            <div className="AddKeywordButton">
                <button className="btn btn-success" onClick={() => handleAddExclude()}>
                    <AddIcon /> Add Keyword
                </button>
            </div>
        </div>
        <Paper className="KeywordElementPaper" variant="outlined" sx={{ backgroundColor: "rgb(233, 233, 233)", border: "0px solid" }}>
          {excludeList.map((exclude, index) => (
            <Paper className="KeywordElements" variant="outlined" sx={{ backgroundColor: "white", border: "0px solid" }} key={index}>
               <div className="KeywordTitle">
                {exclude}
              </div>
              <div className="DeleteButton">
                <button className="btn btn-danger btn-sm" onClick={() => handleDeleteExclude(index)}>
                  <DeleteIcon />
                </button>
              </div>
            </Paper>

              
            
            
          ))}
        </Paper>
        </div>
        </div>
        <button className="btn btn-success" style={{marginTop: '10px', marginBottom:'10px', marginLeft: '10px'}}>
                Done
         </button>
      </Paper>
        
        </Form>

    </div>

     
        
  );
};
export default ProjectForm;
