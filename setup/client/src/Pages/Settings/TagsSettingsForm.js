
import React, { useState, useEffect } from 'react';
import { Form, Input, message, ConfigProvider } from 'antd';
import axios from 'axios';
import DeleteIcon from '@mui/icons-material/Delete';
import AddIcon from '@mui/icons-material/Add';
import { Paper } from '@mui/material';
import './ProjectSettings.css';
import './TagsSettings.css';

const ProjectForm = () => {
  const [projectData, setProjectData] = useState({
    tag: '',
  });
  const [tagList, setTagList] = useState([
    'Awaiting classification',
    'Cochrane review',
    'Ongoing study',
    'Randomised controlled Trial',
  ]);
  const [loading, setLoading] = useState(false);
  const [currentReviewName, setCurrentReviewName] = useState('');
  useEffect(() => {
    const fetchReviewName = async () => {
      try {
        const response = await axios.get('/api/getCurrentReviewName');
        setCurrentReviewName(response.data.reviewName);
      } catch (error) {
        console.error('Error fetching current review name:', error);
      }
    };
    fetchReviewName();
  }, []);
  const handleChange = (key, value) => {
    setProjectData({ ...projectData, [key]: value });
  };
  const handleAddTag = () => {
    if (projectData.tag) {
      setTagList([...tagList, projectData.tag]);
      setProjectData({ tag: '' });
    }
  };
  const handleDeleteTag = (index) => {
    const updatedTagList = [...tagList];
    updatedTagList.splice(index, 1);
    setTagList(updatedTagList);
  };
  const handleSubmit = async () => {
    setLoading(true);
    try {
      message.success('Project details updated successfully!');
      setLoading(false);
    } catch (error) {
      console.error('Error updating project details:', error);
      setLoading(false);
      message.error('Failed to update project details. Please try again.');
    }
  };
  return (
    <ConfigProvider componentSize={{ token: { fontFamily: 'Roboto' } }}>
    <div className="IndividualSettings">
      Manage study tags
      <Paper className="TagsPaper" variant="outlined" sx={{ backgroundColor: "rgb(243,243,243)", border: "0px solid" }}>
      
      <Form onFinish={handleSubmit} layout="vertical">
        <Form.Item>

        <div className="TagsHeader">
          <Input className="TagsHeaderInput" value={projectData.tag} placeholder="Add Tag" onChange={(e) => handleChange('tag', e.target.value)} />
          <div className="TagsHeaderButtonContainer">
            <button className="btn btn-success" onClick={() => handleAddTag()}>
              <AddIcon /> Add Tag
            </button>
           </div>
         </div>
        </Form.Item>
        
        <Paper className="TagsCurrent" variant="outlined" sx={{ backgroundColor: "rgb(233,233,233)", border: "0px solid" }}>
          {tagList.map((tag, index) => (
            <Paper className="TagElements" variant="outlined" sx={{ backgroundColor: "white", border: "0px solid" }} key={index}>
              <div className="TagsElementTitle">
                {tag}
              </div>

              <div className="TagElementDeleteButton">
              <button className="btn btn-danger btn-sm" onClick={() => handleDeleteTag(index)}>
                <DeleteIcon />
              </button>
              </div>
            </Paper>
          ))}
        </Paper>
      </Form>
      </Paper>
    </div>
    </ConfigProvider>
  );
};
export default ProjectForm;
