import React, { useState, useEffect } from 'react';
import { Form, Input, message, Radio, Select } from 'antd';
import { Button } from '@mui/material';
import '../../App.css';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import './ProjectSettings.css';
import './TeamSettings.css';


const {Option} = Select;

const filterOptions = (input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
const employeeOptions = [
    { value: '1', label: 'John Doe' },
    { value: '2', label: 'Jane Doe' },
];
const ProjectForm = () => {
  const [projectData, setProjectData] = useState({
    title: '',
    searchStrat: '',
    searchDate: null,
    screenNo: '',
    fullNo: '',
  });
  const [value, setValue] = useState(1);
  const onChange = (e) => {
    console.log('radio checked', e.target.value);
    setValue(e.target.value);
  };
  const [loading, setLoading] = useState(false);

  

  const handleChange = (key, value) => {
    setProjectData({ ...projectData, [key]: value });
  };

  const handleSubmit = async () => {
    setLoading(true);

    try {

      message.success('Project details updated successfully!');
      setLoading(false);
    } catch (error) {
      console.error('Error updating project details:', error);
      setLoading(false);
      message.error('Failed to update project details. Please try again.');
    }
  };
  const columns = [
    { id: 'reviewer', label: 'Reviewer', minWidth: 170 },
    { id: 'contribution', label: 'Contribution', minWidth: 100 },
   
  
  ];
  
  function createData(reviewer, contribution) {
    return { reviewer,contribution};
  }
  
  const rows = [
      createData('Jamie Smith', 32),
      createData('Alice Brown', 24),
      createData('Rachel Jones', 12),
   
  ];
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <div className="IndividualSettings">
      Team Settings:
      <Paper className="TeamSettingsPaper" variant="outlined" sx={{backgroundColor: "rgb(243,243,243)", border: "0px solid"}}>
        <Form onFinish={handleSubmit} layout="vertical" >
      <>
            <Radio.Group className="RadioGroup" onChange={onChange} value={value}>
                    <Radio value={1}>Everyone can do anything</Radio>
                    <Radio value={2}>Manage Rules</Radio>
                    
            </Radio.Group>
        <Form.Item label="All studies must be screened by: John Doe" name="studyScreener" style={{ fontSize: '20pt' }}>
          <Select
              mode="multiple"
              style={{ width: '100%' }}
              placeholder="Select collaborators"
              value={projectData.collaborators}
              onChange={value => handleChange('collaborators', value)}
              showSearch
              filterOption={filterOptions}
          >
              {employeeOptions.map(option => (
                  <Option key={option.value} value={option.value}>
                      {option.label}
                  </Option>
              ))}
          </Select>
        </Form.Item>
        <Form.Item label="Conflicts can be resolved by: Jane Doe, John Doe" name="studyScreener" style={{ fontSize: '20pt' }}>
          <Select
              mode="multiple"
              style={{ width: '100%' }}
              placeholder="Select collaborators"
              value={projectData.collaborators}
              onChange={value => handleChange('collaborators', value)}
              showSearch
              filterOption={filterOptions}
          >
              {employeeOptions.map(option => (
                  <Option key={option.value} value={option.value}>
                      {option.label}
                  </Option>
              ))}
          </Select>
        </Form.Item>
        
      </>
      </Form>

    <Button variant="contained">Save Changes</Button>
    </Paper>
    <div>
      Team Progress:
    </div>
        <Paper className="TeamProgress" variant="outlined" sx={{ backgroundColor: "rgb(243,243,243)", border: "0px solid" }}>
          <div className="TeamProgressConflicts">
            37 done 7 conflicts
          </div>
      <TableContainer component={Paper}>
        <Table sx={{minWidth: 650}} aria-label="simple table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align="left"
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                    {columns.map((column) => {
                      const value = row[column.id];
                      return (
                        <TableCell key={column.id} align={column.align}>
                          {column.format && typeof value === 'number'
                            ? column.format(value)
                            : value}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
  

        </Paper>
    </div>


  );
};

export default ProjectForm;
