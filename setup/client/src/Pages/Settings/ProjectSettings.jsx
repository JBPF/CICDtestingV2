import React, { useState, useEffect } from 'react';
import { Box, Tab, Tabs } from '@mui/material';
import ReviewSettingsForm from './ReviewSettingsForm';
import TeamSettingsForm from './TeamSettingsForm';
import CriteriaSettingsForm from './CriteriaSettingsForm';
import TagsSettingsForm from './TagsSettingsForm';
import './ProjectSettings.css'

const ProjectSettings = () => {
  const [selectedTab, setSelectedTab] = useState(0);

  const handleChange = (event, newValue) => {
    setSelectedTab(newValue);
  };

  return (
    <div className="AppContainer">
    <div className="MainContent">
      <div className="ProjectSettingsPage">
    <Box>
    <Box sx={{ borderBottom: 1, borderColor: 'divider', width: '100%' }}>
      <Tabs 
        value={selectedTab} 
        onChange={handleChange} 
        aria-label="profile tabs" 
        variant="fullWidth" 
      >
        <Tab label="Review Settings" />
        <Tab label="Team Settings" />
        <Tab label="Criteria" />
        <Tab label="Tags" />
      </Tabs>
    </Box>
    {selectedTab === 0 && <ReviewSettingsForm />}
    {selectedTab === 1 && <TeamSettingsForm />}
    {selectedTab === 2 && <CriteriaSettingsForm />}
    {selectedTab === 3 && <TagsSettingsForm />}
  </Box>
  </div>
  </div>
  </div>

  );
};

export default ProjectSettings;
