import './ProjectSettings.css';
import './ReviewSettings.css';
import React, { useState, useEffect } from 'react';
import { Form, Input, message, DatePicker } from 'antd';
import { Button, Paper } from '@mui/material';
import axios from 'axios';
import '../../App.css';
import { Select, Space } from 'antd';
const { TextArea } = Input;


const ProjectForm = () => {
  const [projectData, setProjectData] = useState({
    title: '',
    searchStrat: '',
    searchDate: null,
    screenNo: '',
    fullNo: '',
  });

  const [loading, setLoading] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [currentReviewName, setCurrentReviewName] = useState('');

  useEffect(() => {
    const fetchReviewName = async () => {
      try {
        const response = await axios.get('/api/getCurrentReviewName');
        setCurrentReviewName(response.data.reviewName);
      } catch (error) {
        console.error('Error fetching current review name:', error);
      }
    };

    fetchReviewName();
  }, []);

  const handleEditClick = () => {
    setIsEditing(!isEditing);
  };

  const handleChange = (key, value) => {
    setProjectData({ ...projectData, [key]: value });
  };

  const handleScreeningChange = (value) => {
    console.log(`selected ${value}`);
  };

  const options = [
    { value: '1', label: 'Title' },
    { value: '2', label: 'Abstract' },
    { value: '3', label: 'Full Text Review' },
    { value: '4', label: 'Data Extraction' },
  ];

  const handleSubmit = async () => {
    setLoading(true);

    try {
      message.success('Project details updated successfully!');
      setLoading(false);
      setIsEditing(false);
    } catch (error) {
      console.error('Error updating project details:', error);
      setLoading(false);
      message.error('Failed to update project details. Please try again.');
    }
  };

  const generateStageFormItem = (stage) => (
    <Form.Item label={`Stage ${stage}:`} name={`stage${stage}`} style={{ fontSize: '20pt' }}>
    <Select
      mode="multiple"
      onChange={handleScreeningChange}
      optionLabelProp="label"
      options={options}
      optionRender={(option) => (
        <Space>
          <span role="img" aria-label={option.label}>
            {option.label}
          </span>
        </Space>
      )}
    />
  </Form.Item>
);
  

  return (
    <div className="IndividualSettings">
      Review Settings:

      <Form onFinish={handleSubmit} layout="vertical">
      <Paper className="ReviewSettingsPaper" variant="outlined" sx={{ backgroundColor: "rgb(243,243,243)", border: "0px solid" }}>

        
        
          <Form.Item label="Title" name="title" style={{ fontSize: '20pt' }}>
            {!isEditing && (<Input value={projectData.title} placeholder="Examining the role of cortisol on stress" disabled/> )}
            {isEditing && (<Input value={projectData.title} placeholder="Examining the role of cortisol on stress" onChange={(e) => handleChange('title', e.target.value)} />)}
          </Form.Item>
          <Form.Item label="Search Strategy" name="searchStrat">
            {!isEditing && (<TextArea value={projectData.searchStrat} placeholder="Healthcare data, cortisol" disabled/> )}
            {isEditing && (<TextArea value={projectData.searchStrat} placeholder="Healthcare data, cortisol" onChange={(e) => handleChange('searchStrat', e.target.value)} />)}
          </Form.Item>
          <Form.Item label="Date of last Search" name="searchDate">
            {!isEditing && (<DatePicker value={projectData.searchDate} placeholder="28/1/23" disabled/> )}
            {isEditing && (<DatePicker value={projectData.searchDate} placeholder="28/1/23" onChange={(date) => handleChange('searchDate', date)} />)}
          </Form.Item>
          <Form.Item label="Number of reviewers required for screen" name="screenNo">
            {!isEditing && (<Input value={projectData.screenNo} placeholder="45" disabled/> )}
            {isEditing && (<Input value={projectData.screenNo} placeholder="45" onChange={(e) => handleChange('screenNo', e.target.value)} />)}
          </Form.Item>
          <Form.Item label="Number of reviewers required for full text review" name="fullNo">
            {!isEditing && (<Input value={projectData.fullNo} placeholder="30" disabled/> )}
            {isEditing && (<Input value={projectData.fullNo} placeholder="30" onChange={(e) => handleChange('fullNo', e.target.value)} />)}
          </Form.Item>
          <Button variant="contained" onClick={handleEditClick}>
          {isEditing ? 'Save' : 'Edit'}
        </Button>

        </Paper>
        </Form>
        Screening Strategy:
          <Paper className="ScreeningStrategyPaper" variant="outlined" sx={{ backgroundColor: "rgb(243,243,243)", border: "0px solid" }}>
            {[1, 2, 3, 4].map((stage) => generateStageFormItem(stage))}
            <Button variant="contained">Save Changes</Button>
          </Paper>
          
        

    </div>
  );
};

export default ProjectForm;
