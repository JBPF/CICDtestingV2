import axios from 'axios';
const API_BASE_URL = process.env.REACT_APP_API_URL || 'http://localhost:3000'; // Use your actual API URL

export const sendInvitation = async (emailData) => {
    try {
        console.log('emailData', emailData);
        const response = await axios.post(`${API_BASE_URL}/users/invite`, emailData);
        console.log('here is response', response);
        return response.data; // Return the response data from the server
    } catch (error) {
        throw error;
    }
};

export const registerUser = async ({ token, name, email, password }) => {
    try {
        const response = await axios.post('/users/register', { token, name, email, password });

        if (response.status === 201) {
            return response.data;
        } else {
            throw new Error(`Registration failed: Status ${response.status}`);
        }
    } catch (error) {
        console.error('Error during registration:', error);
        throw error;
    }
};
export const fetchUsers = async () => {
    try {
        const response = await axios.get(`${API_BASE_URL}/users/users`);
        return response.data;
    } catch (error) {
        throw error;
    }
}
