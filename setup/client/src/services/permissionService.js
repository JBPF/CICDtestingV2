import axios from 'axios';

const API_BASE_URL = process.env.REACT_APP_API_URL || 'http://localhost:3000'; // Use your actual API URL

export const fetchPermissions = async () => {
    try {
        const response = await axios.get(`${API_BASE_URL}/api/permissions`);
        return response.data;
    } catch (error) {
        // Handle or throw the error appropriately
        throw error;
    }
};