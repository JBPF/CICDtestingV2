import axios from 'axios';


export const updateReviewDecision = async (reviewInstanceId, decision) => {
    const token = localStorage.getItem('authToken');
    return axios.patch(`http://localhost:3000/api/review-instances/${reviewInstanceId}/decision`, { decision }, {
        headers: {
            Authorization: `Bearer ${token}`,
        },
    });
};
