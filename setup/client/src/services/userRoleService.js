import axios from 'axios';
const API_BASE_URL = process.env.REACT_APP_API_URL || 'http://localhost:3000'; // Use your actual API URL

export const fetchUserRoleById = async (roleId) => {
  try {
    console.log("Error fetching user role from service:", roleId);

    const response = await axios.get(`${API_BASE_URL}/api/userroles/${roleId}`);
    if (response.status === 200) {
      return response.data;
    } else {
      console.error(`Failed to fetch user role: Status ${response.status}`);
      throw new Error(`Failed to fetch user role: Status ${response.status}`);
    }
  } catch (error) {
    console.error("Error fetching user role from service:", error);
    throw error;
  }
};

export const fetchUserRoles = async () => {
  try {
      const response = await axios.get(`${API_BASE_URL}/api/userroles`);
      return response.data;
  } catch (error) {
      throw error;
  }
}
export const listUserRoles= async (userId)=>{
  try{
    console.log("userid in listuserroles is", userId);
    const response= await axios.get(`${API_BASE_URL}/api/listRoles/${userId}`,{
      params: {userId},
      headers:{

      },
     
    });
    return response.data; 
  } catch(error){
    console.error("Failed to fetch user roles:", error.response.data);
    throw error;
  }
};