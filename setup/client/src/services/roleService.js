import axios from './axiosConfig'; // Adjust the path to where you've set up the Axios interceptor
const API_BASE_URL = process.env.REACT_APP_API_URL || 'http://localhost:3000';



export const createRole = async (roleData) => {
    try {
        const response = await axios.post(`${API_BASE_URL}/api/roles`, roleData);
        if (response.status === 201) {
            return response.data;
        } else {
            throw new Error(`Failed to create role: Status ${response.status}`);
        }
    } catch (error) {
        throw error;
    }
};


export const assignRoleToUser = async (roleData) => {
    try {
        const response = await axios.post(`${API_BASE_URL}/api/assignRole`, roleData);
        console.log(response);
        if (response.status === 201) {
            return response.data;
        } else {
            throw new Error(`Failed to assign role: Status ${response.status}`);
        }
    } catch (error) {
        throw error;
    }

}
export const updateRolePermissions = async (roleId, updatedData) => {
    try {
        const response = await axios.put(`${API_BASE_URL}/api/roles/${roleId}`, updatedData);
        if (response.status === 200) {
            return response.data;
        } else {
            throw new Error(`Failed to update role permissions: Status ${response.status}`);
        }
    } catch (error) {
        throw error;
    }
};

export const fetchRoles = async () => {
    try {
        const response = await axios.get('/api/roles'); // No need to specify the token here
        if (response.status === 200) {
            return response.data;
        } else {
            throw new Error(`Failed to fetch roles: Status ${response.status}`);
        }
    } catch (error) {
        throw error;
    }
};
export const fetchRoleById= async (roleId) => {
    try {
        const response = await axios.get(`${API_BASE_URL}/roles/${roleId}`); // No need to specify the token here
        if (response.status === 200) {
            return response.data;
        } else {
            throw new Error(`Failed to fetch roles: Status ${response.status}`);
        }
    } catch (error) {
        throw error;
    }
};
//deleteRole

export const deleteRole = async (roleId) => {
    try {
        console.log(`role in deleterole service: ${roleId}`);

        const response = await axios.delete(`${API_BASE_URL}/api/roles/${roleId}`);
        if (response.status === 200) {
            return response.data;
        } else {
            throw new Error(`Failed to delete role: Status ${response.status}`);
        }
    } catch (error) {
        throw error;
    }
}
export const deletePermission = async (roleId, permissionId) => {
    try {
        const response = await axios.delete(`/api/roles/${roleId}/permissions/${permissionId}`);
        console.log(response.data); // Handle the response as needed
    } catch (error) {
        console.error(`Failed to delete permission: ${error.message}`);
    }
};
export const addPermission = async (roleId, permissionId) => {
    try {
        const response = await axios.post(`/api/roles/${roleId}/permissions`, { permissionId });
        console.log(response.data); // Handle the response as needed
    } catch (error) {
        console.error(`Failed to add permission: ${error.message}`);
    }
};


