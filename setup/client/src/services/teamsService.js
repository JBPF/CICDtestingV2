import axios from './axiosConfig';
const API_BASE_URL = process.env.REACT_APP_API_URL || 'http://localhost:3000';


export const createTeam = async (teamData) => {
    try {
        const response = await axios.post(`${API_BASE_URL}/api/teams`, teamData);
        if (response.status === 201) {
            return response.data;
        } else {
            throw new Error(`Failed to create team: Status ${response.status}`);
        }
    } catch (error) {
        throw error;
    }
};

export const getTeamDetails = async (teamId) => {
    try {
        const response = await axios.get(`${API_BASE_URL}/api/teams/${teamId}`);
        if (response.status === 200) {
            return response.data;
        } else {
            throw new Error(`Failed to fetch team details: Status ${response.status}`);
        }
    } catch (error) {
        throw error;
    }
}

export const addMember = async (teamId, memberData) => {
    try {
        const response = await axios.post(`${API_BASE_URL}/api/teams/${teamId}/members`, memberData);
        if (response.status === 200) {
            return response.data;
        }
    } catch (error) {
        console.error('error:', error);
        throw error;
    }
}

export const removeMember = async (teamId, userId) => {
    try {
        const response = await axios.delete(`${API_BASE_URL}/api/teams/${teamId}/members/${userId._id}`);
        if (response.status === 200) {
            return response.data;
        } else {
            throw new Error(`Failed to remove member: Status ${response.status}`);
        }
    } catch (error) {
        throw error;
    }
}

export const updateTeam = async (teamId, teamData) => {
    try {
        const response = await axios.put(`${API_BASE_URL}/api/teams/${teamId}`, teamData);
        if (response.status === 200) {
            return response.data;
        } else {
            throw new Error(`Failed to update team: Status ${response.status}`);
        }
    } catch (error) {
        throw error;
    }
}

export const deleteTeam = async (teamId) => {
    try {
        const response = await axios.delete(`${API_BASE_URL}/api/teams/${teamId}`);
        if (response.status === 200) {
            return response.data;
        } else {
            throw new Error(`Failed to delete team: Status ${response.status}`);
        }
    } catch (error) {
        throw error;
    }
}









