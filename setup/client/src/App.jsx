import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import Login from './Pages/Login/Login'
import Dashboard from './Pages/Dashboard/Dashboard'
import Sidebar from './Components/Sidebar'
import OneProject from './Pages/Projects/OneProject'
import TitleAbstractScreening from './Pages/Review/TitleAbstractScreening'
import Footer from './Components/Footer'
import ExtractionPage from './Pages/Projects/ExtractionPage';
import Export from './Pages/Export/Export'
import FullTextReview from './Pages/Projects/FullTextReview';
import ProjectSettings from './Pages/Settings/ProjectSettings'
import AppSettings from './Pages/Settings/AppSettings'

import ProfilePage from './Pages/Profile/ProfilePage';
import Admin from './Pages/Admin/Admin';
import PrismaFlowchart from './Pages/Prisma/PrismaFlowchart';
import Register from './Pages/Register/register';
import MyTeams from './Pages/Profile/MyTeams';
import DynamicStageComponent from './Components/DynamicStageComponent';

function App() {
  return (
      <div>
        <BrowserRouter>
          <div className="AppContainer">
            <Routes>
              <Route path='/' element={<Login />} />
              <Route path='/login' element={<Login />} />
              <Route path='/dashboard' element={<><Sidebar /><Dashboard /></>} />
              <Route path='/oneproject/:projectId' element={<><Sidebar/><OneProject /></>} />
              <Route path='/projects/:projectId/extraction' element={<><Sidebar/><ExtractionPage /></>} />
              <Route path='/titleabstract' element={<><Sidebar/><TitleAbstractScreening /></>} />
              <Route path="/prisma" element={<><Sidebar/><PrismaFlowchart/></>} />
              <Route path='/profile' element={<><Sidebar/><ProfilePage /></>} />
              <Route path='/settings' element={<><Sidebar/><AppSettings /></>} />
              <Route path='/export' element={<><Sidebar/><Export /></>} />
              <Route path='/projectSettings' element={<><Sidebar/><ProjectSettings /></>} />
              <Route path='/admin' element={<><Sidebar/><Admin /></>} />
              <Route path="/register/:token" element={<Register />} />
              <Route path="/myteams" element={<><Sidebar/><MyTeams /></>} />
              <Route path='/projects/:projectId/:stage' element={<><Sidebar /><DynamicStageComponent /></>} />
              <Route path="/register" element={<Register />} />
            </Routes>
          </div>


          <Footer />
        </BrowserRouter>
      </div>
  );
}

export default App;
