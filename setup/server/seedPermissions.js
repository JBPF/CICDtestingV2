require('dotenv').config();
const mongoose = require('mongoose');
const Permission = require('./models/Permission');
const actions = require('./utils/actions');

async function seedPermissions() {
    await mongoose.connect(process.env.MONG_URI, { useNewUrlParser: true, useUnifiedTopology: true });

    const permissionsToSeed = Object.values(actions).map(action => ({
        action,
        description: `Description for ${action}`, 
    }));

    try {
        for (const permission of permissionsToSeed) {
            await Permission.updateOne(
                { action: permission.action },
                { $setOnInsert: permission },
                { upsert: true }
            );
        }

        console.log('Permissions seeded successfully');
    } catch (error) {
        console.error('Failed to seed permissions:', error);
    } finally {

        mongoose.connection.close();
    }
}

seedPermissions().catch(console.error);
