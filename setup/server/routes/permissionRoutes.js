const express = require('express');
const router = express.Router();
const permissionController = require('../controllers/permissionController');
const verifyPermission = require('../middleware/verifyPermissions');

// Route to list all permissions
router.get('/permissions', permissionController.getAllPermissions);

module.exports = router;