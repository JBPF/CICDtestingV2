const express = require('express');
const router = express.Router();
const UserController = require('../controllers/userController');
const verifyPermission = require('../middleware/verifyPermissions');
const actions = require('../utils/actions');
const authenticator = require("../middleware/authenticator");

// Route to create a new user
router.post('/users',authenticator, verifyPermission(actions.CREATE_USER), UserController.createUser);

//Route to login a user
router.post('/login', UserController.login);

//Route to invite a user
router.post('/invite',authenticator, verifyPermission(actions.INVITE_USER), UserController.inviteUser);

//Route to register a user
router.post('/register', UserController.registerUser);


// Route to update user details
router.put('/users/:userId',authenticator, verifyPermission(actions.UPDATE_USER), UserController.updateUser);

// Route to remove a role from a user
router.delete('/users/:userId',authenticator, verifyPermission(actions.DELETE_USER), UserController.deleteUser);

// Route to list all users

router.get('/users',authenticator, verifyPermission(actions.LIST_USERS), UserController.listUsers);




module.exports = router;