const express = require('express');
const router = express.Router();
const userRoleController = require('../controllers/userRoleController');
const verifyPermission = require('../middleware/verifyPermissions');
const actions = require('../utils/actions');
const authenticator = require("../middleware/authenticator");

// Route to assign a role to a user

router.post('/assignRole',authenticator, verifyPermission(actions.ASSIGN_ROLE_TO_USER), userRoleController.assignRoleToUser);
// router.post('/assignRole', async (req, res) => {
//     console.log('Assign role route hit');
//     res.status(200).json({ message: "Route hit successfully" });
// });
// Route to update a user's role
router.put('/updateRole/:userRoleId',authenticator, verifyPermission(actions.UPDATE_USER_ROLE), userRoleController.updateUserRole);

// Route to remove a role from a user
router.delete('/removeRole/:userRoleId', authenticator,verifyPermission(actions.REMOVE_ROLE_FROM_USER), userRoleController.removeRoleFromUser);

// Route to list all user roles
router.get('/listRoles/:userId',authenticator, verifyPermission(actions.LIST_USER_ROLES), userRoleController.listUserRoles);

// router.get('/fetchUserRoleById/:userRoleId',authenticator, verifyPermission(actions.LIST_USER_ROLES), userRoleController.fetchUserRoleById);


module.exports = router;