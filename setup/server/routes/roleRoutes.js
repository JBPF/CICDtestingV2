const express = require('express');
const router = express.Router();
const roleController = require('../controllers/roleController');
const verifyPermission = require('../middleware/verifyPermissions');
const actions = require('../utils/actions');
const authenticator = require("../middleware/authenticator");

// Route to create a new role
router.post('/roles',authenticator, verifyPermission(actions.CREATE_ROLE), roleController.createRole);

// Route to list all roles
router.get('/roles',authenticator, verifyPermission(actions.LIST_ROLES), roleController.listRoles);

// Route to get details of a specific role
router.get('/roles/:roleId',authenticator, verifyPermission(actions.GET_ROLE_DETAILS), roleController.getRoleDetails);

// Route to update a role
router.put('/roles/:roleId',authenticator, verifyPermission(actions.UPDATE_ROLE), roleController.updateRole);

router.delete('/roles/:roleId', authenticator, verifyPermission(actions.LIST_ROLES), roleController.deleteRole);
router.post('/roles/:roleId/permissions', authenticator, verifyPermission(actions.LIST_ROLES), roleController.addPermission);
router.delete('/roles/:roleId/permissions/:permissionId', authenticator, verifyPermission(actions.LIST_ROLES), roleController.deletePermission);


module.exports = router;

