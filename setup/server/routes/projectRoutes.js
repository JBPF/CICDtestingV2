const express = require('express');
const router = express.Router();
const projectController = require('../controllers/projectController');
const reviewInstanceController = require('../controllers/reviewInstanceController');
const multer = require('multer');
const authenticationMiddleware = require('../middleware/authenticator');
const verifyPermission = require('../middleware/verifyPermissions');
const actions = require('../utils/actions');

const fileFilter = (req, file, cb) => {
    const allowedTypes = /pdf|xml/;
    const isAccepted = allowedTypes.test(file.mimetype) && allowedTypes.test(file.originalname.toLowerCase());

    if (isAccepted) {
        return cb(null, true);
    } else {
        return cb(new Error('Only PDF and XML files are allowed'), false);
    }
};

const upload = multer({
    dest: 'uploads/',
    fileFilter: fileFilter,
    limits: { fileSize: 10 * 1024 * 1024 } // for 10MB
});

// Create a new project
router.post('/projects', authenticationMiddleware, verifyPermission(actions.CREATE_PROJECT), projectController.createProject);

// Import articles to a project with file uploads
router.post('/projects/:projectId/articles', [authenticationMiddleware, verifyPermission(actions.IMPORT_ARTICLES), upload.array('articles')], projectController.importArticles);

// Update an article's review status within a project
router.put('/projects/:projectId/articles/:articleId/reviewStatus', authenticationMiddleware, verifyPermission(actions.UPDATE_ARTICLE_REVIEW_STATUS), projectController.updateArticleReviewStatus);

//get criteria
router.get('/projects/:projectId/criteria', projectController.getProjectCriteria);


// This route seems to be planning to assign an article to a reviewer, but the controller method name was not provided in the code you shared.
// Ensure you have a corresponding controller method for this route.
// router.post('/projects/:projectId/articles/:articleId/reviewAssignments', authenticationMiddleware, verifyPermission(actions.ASSIGN_ARTICLE_TO_REVIEWER), projectController.assignReviewersToArticle);

// Update the status of a review stage within a project
router.put('/projects/:projectId/reviewStages', authenticationMiddleware, verifyPermission(actions.UPDATE_REVIEW_STAGE_STATUS), projectController.updateReviewStageStatus);

// Add a collaborator to a project
router.post('/projects/:projectId/collaborators', authenticationMiddleware, verifyPermission(actions.ADD_COLLABORATOR), projectController.addCollaborator);

// Delete a project
router.delete('/projects/delete/:projectId', authenticationMiddleware, verifyPermission(actions.DELETE_PROJECT), projectController.deleteProject);

// Fetch studies for a project
router.get('/projects/:projectId/articles', authenticationMiddleware, projectController.getStudies);

// Fetch projects
router.get('/projects/display', authenticationMiddleware, verifyPermission(actions.LIST_PROJECTS), projectController.getProjects);

// Fetch extraction variables for a project
router.get('/projects/:projectId/extractionVariables', authenticationMiddleware, projectController.getExtractionVariables);

// Fetch project by ID
router.get('/projects/:projectId', authenticationMiddleware, verifyPermission(actions.LIST_PROJECTS), projectController.getProjectById);

router.get('/projects/:projectId/review-instances/:stage',authenticationMiddleware, verifyPermission(actions.VIEW_REVIEW_INSTANCES), reviewInstanceController.getReviewInstances);

//fetch project reviewStageconfig
module.exports = router;
