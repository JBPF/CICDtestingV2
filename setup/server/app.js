//import modules
const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const cors = require('cors');
require('dotenv').config();
const bcrypt = require('bcrypt');
const validator = require('validator');
const projectRoutes = require('./routes/projectRoutes'); // Adjust the path based on your file structure
const Study = require('./models/Article');
const userRoutes = require('./routes/userRoutes'); // Adjust the path as necessary
const reviewInstanceRoutes = require('./routes/reviewInstanceRoutes');
const app = express();
const teamRoutes = require('./routes/teamRoutes'); // Adjust the path as necessary
const userRoleRoutes = require('./routes/userRoleRoutes'); // Adjust the path as necessary
const roleRoutes = require('./routes/roleRoutes');// Adjust the path as necessary
const permissionRoutes = require('./routes/permissionRoutes');// Adjust the path as necessary
const authenticationMiddleware = require('./middleware/authenticator');
//db
mongoose.connect(process.env.MONG_URI)
  .then(() => console.log('DB connected'))
  .catch(err => console.error('MongoDB connection error:', err));


app.use(express.json()); // Middleware to parse JSON bodies


//middleware
app.use(morgan('dev'));
app.use(cors({ origin: true, credentials: true}));
app.use(express.json());
app.use('/uploads', express.static('uploads'));

//error handling middleware
app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).json({ error: 'An error occurred' });
});


//routes
app.use('/project', projectRoutes);
app.use('/api', projectRoutes);
app.use('/api', reviewInstanceRoutes);
app.use(express.json());
app.use('/api', userRoleRoutes);
app.use('/api', teamRoutes);
app.use('/api', roleRoutes);
app.use('/users', userRoutes);
app.use('/api', permissionRoutes);





const port = process.env.PORT || 3001;

//listener

app.listen(port, (error) => {
  if (error) {
    console.error('Failed to start the server:', error);
    return;
  }
  console.log(`Server running on port ${port}`);
});