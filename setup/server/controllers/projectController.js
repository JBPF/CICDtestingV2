const Project = require('../models/Project');
const ReviewInstance = require('../models/ReviewInstance');
const User = require('../models/User');
const Article = require('../models/Article');
const ArticleReview = require('../models/ArticleReview');
const UserRole = require('../models/UserRole');
const Role = require('../models/Role');
// const fetchStudyFromPubMed = require('../utils/fetchStudyFromPubMed');
const extractMetadataFromPDF = require('../utils/extractMetadataFromPDF');
const { getEventListeners } = require('nodemailer/lib/xoauth2');




exports.createProject = async (req, res) => {
    try {
        const { title, description, startDate, manager, collaborators, teams, inclusionCriteria, exclusionCriteria, extractionVariables, reviewStageConfig,reviewersPerArticle } = req.body;

        const newProject = new Project({
            title,
            description,
            startDate,
            manager,
            collaborators,
            teams,
            inclusionCriteria,
            exclusionCriteria,
            extractionVariables,
            reviewStageConfig,
            reviewersPerArticle,
            stages: {}
        });

        if (newProject.reviewStageConfig === 'three-stage') {
            newProject.stages = {
                titleReview: { status: 'pending', deadline: null },
                abstractReview: { status: 'pending', deadline: null },
                fullArticleReview: { status: 'pending', deadline: null }
            };
        } else {
            newProject.stages = {
                titleAbstractReview: { status: 'pending', deadline: null },
                fullArticleReview: { status: 'pending', deadline: null }
            };
        }

        await newProject.save();

        res.status(201).json({
            message: 'Project created successfully',
            project: newProject
        });
        console.log(extractionVariables)
    } catch (error) {
        console.error('Failed to create project:', error);
        res.status(500).json({
            message: 'Failed to create project',
            error: error.message
        });
    }
};



exports.importArticles = async (req, res) => {
    const { projectId } = req.params;
    const { reviewersPerArticle } = req.body;
    const files = req.files;

    try {
        const project = await Project.findById(projectId);
        if (!project) {
            return res.status(404).json({ message: "Project not found" });
        }

        if (files && files.length > 0) {
            for (const file of files) {
                const { abstract } = await extractMetadataFromPDF(file.path);
                const title = file.originalname.replace(/\.[^/.]+$/, "");
                const newArticle = new Article({
                    title,
                    abstract,
                    documentLink: `http://localhost:3000/uploads/${file.filename}`, // Adjust according to your environment and file naming
                    documentType: file.mimetype.includes('pdf') ? 'pdf' : 'xml',
                });

                console.log("New article:", newArticle);
                await newArticle.save();
                project.studies.push(newArticle._id);
                await assignReviewersToArticle(newArticle._id, projectId);
            }
        }

        await project.save();
        res.status(200).json({ message: "Articles imported and reviewers assigned successfully", project });
    } catch (error) {
        console.error(`Failed to import articles for project ${projectId}:`, error);
        res.status(500).json({ message: "Failed to import articles and assign reviewers", error: error.message });
    }
};




exports.updateArticleReviewStatus = async (req, res) => {
    const { articleId, reviewStatus, projectId } = req.body;

    try {
        const reviewInstance = await ReviewInstance.findOne({ article: articleId, project: projectId });

        if (!reviewInstance) {
            return res.status(404).json({ message: "Review instance not found for this article and project" });
        }

        reviewInstance.reviewStatus = reviewStatus;
        await reviewInstance.save();

        res.status(200).json({ message: "Article review status updated successfully", reviewInstance });
    } catch (error) {
        res.status(500).json({ message: "Failed to update article review status", error: error.message });
    }
};


async function getReviewerRoleId() {
    try {
        const reviewerRole = await Role.findOne({ name: 'Reviewer' });
        if (!reviewerRole) {
            console.error("Reviewer role not found");
            return null;
        }
        return reviewerRole._id; // Assuming _id is the ObjectId that you want
    } catch (error) {
        console.error("Error fetching Reviewer role ID:", error);
        return null;
    }
}

async function getLeastBusyReviewers(projectId, roleName, numUsersNeeded) {

    const role = await Role.findOne({ name:roleName });
    if (!role) {
        console.error(`Role not found for name: ${roleName}`);
        return [];
    }
    const roleId = role._id;

    const project = await Project.findById(projectId).populate({
        path: 'collaborators',
        populate: { path: 'userRoles' }
    });

    if (!project) {
        console.error("Project not found");
        return [];
    }

    const usersWithRole = project.collaborators.filter(collaborator =>
        collaborator.userRoles.some(role =>
            role.role.toString() === roleId.toString() && role.project.toString() === projectId.toString()
        )
    );

    const userWorkloads = await Promise.all(usersWithRole.map(async (user) => {
        const count = await ReviewInstance.countDocuments({ project: projectId, reviewer: user._id });
        return { userId: user._id, workload: count };
    }));

    userWorkloads.sort((a, b) => a.workload - b.workload);
    const selectedUsers = userWorkloads.slice(0, numUsersNeeded).map(user => user.userId);

    console.log("number of reviewers per article:", numUsersNeeded);
    return selectedUsers;
}


async function assignReviewersToArticle(articleId, projectId) {
    const project = await Project.findById(projectId);
    if (!project) {
        throw new Error("Project not found");
    }
    const numberNeeded= project.reviewersPerArticle;
    const selectedReviewersIds = await getLeastBusyReviewers(projectId,'Reviewer', numberNeeded);

    const articleReview = new ArticleReview({
        article: articleId,
        project: projectId,
        overallStatus: 'pending',
        currentStage: project.reviewStageConfig === 'three-stage' ? 'titleReview' : 'titleAbstractReview',
        reviewInstances: [],
    });

    await articleReview.save();

    for (const reviewerId of selectedReviewersIds) {
        const reviewInstance = new ReviewInstance({
            project: projectId,
            article: articleId,
            reviewer: reviewerId,
            reviewStatus: 'pending',
            reviewStage: articleReview.currentStage,
            articleReview: articleReview._id,
        });
        console.log("Review instance stage:", reviewInstance.reviewStage);
        console.log("Review instance created:", reviewInstance);
        await reviewInstance.save();
        articleReview.reviewInstances.push(reviewInstance._id);
    }


    await articleReview.save();
    console.log("Reviewers assigned and ArticleReview created successfully.");
}

exports.updateReviewStageStatus = async (req, res) => {
    const { projectId, stage, status } = req.body;

    try {
        const project = await Project.findById(projectId);
        if (!project) {
            return res.status(404).json({ message: "Project not found" });
        }

        if (!project.stages[stage]) {
            return res.status(400).json({ message: "Invalid review stage" });
        }

        project.stages[stage].status = status;
        await project.save();

        res.status(200).json({ message: "Review stage status updated successfully", project });
    } catch (error) {
        res.status(500).json({ message: "Failed to update review stage status", error: error.message });
    }
};

exports.addCollaborator = async (req, res) => {
    const { projectId, collaboratorId } = req.body;

    try {
        const project = await Project.findById(projectId);
        const collaborator = await User.findById(collaboratorId);

        if (!project || !collaborator) {
            return res.status(404).json({ message: "Project or User not found" });
        }

        if (project.collaborators.includes(collaboratorId)) {
            return res.status(409).json({ message: "User is already a collaborator" });
        }

        project.collaborators.push(collaboratorId);
        await project.save();

        res.status(200).json({ message: "Collaborator added successfully", project });
    } catch (error) {
        res.status(500).json({ message: "Failed to add collaborator", error: error.message });
    }
};


exports.getProjectCriteria = async (req, res) => {
    const { projectId } = req.params;

    try {
        const project = await Project.findById(projectId).select('inclusionCriteria exclusionCriteria');
        if (!project) {
            return res.status(404).json({ message: 'Project not found' });
        }

        res.status(200).json({
            inclusionCriteria: project.inclusionCriteria,
            exclusionCriteria: project.exclusionCriteria
        });
    } catch (error) {
        console.error('Error fetching project criteria:', error);
        res.status(500).json({ message: 'Failed to fetch project criteria', error: error.message });
    }
};


exports.deleteProject = async (req, res) => {
    const { projectId } = req.params;

    try {
        const deletedProject = await Project.findByIdAndDelete(projectId);
        if (!deletedProject) {
            return res.status(404).json({ message: "Project not found" });
        }



        res.status(200).json({ message: "Project deleted successfully" });
    } catch (error) {
        res.status(500).json({ message: "Failed to delete project", error: error.message });
    }
};

exports.getStudies = async (req, res) => {
    const { projectId } = req.params;
    try {
        const project = await
            Project.findById(projectId)
                .populate('studies');
        if (!project) {
            return res.status(404).json({ message: "Project not found" });
        }
        res.status(200).json({ message: "Studies fetched successfully", studies: project.studies });
    }
    catch (error) {
        res.status(500).json({ message: "Failed to fetch studies", error: error.message });
    }
}

exports.getProjects = async (req, res) => {
    try {
        const projects = await Project.find();
        res.status(200).json({ message: "Projects fetched successfully", projects });
    } catch (error) {
        res.status(500).json({ message: "Failed to fetch projects", error: error.message });
    }
}

exports.getExtractionVariables = async (req, res) => {
    const { projectId } = req.params;
    try {
        const project = await
        Project.findById(projectId)
                .select('extractionVariables');
        if (!project) {
            return res.status(404).json({ message: "Project not found" });
        }
        res.status(200).json({
            extractionVariables: project.extractionVariables,
        });
        console.log(project.extractionVariables);
    }
    catch (error) {
        res.status(500).json({ message: "Failed to fetch extraction variables", error: error.message });
        console.log(error.message);
    }
}


exports.getProjectById = async (req, res) => {
    const { projectId } = req.params;

    try {
        const project = await
            Project.findById(projectId);
        if (!project) {
            return res.status(404).json({ message: "Project not found" });
        }
        res.status(200).json({ message: "Project fetched successfully", project });
    }
    catch (error) {
        res.status(500).json({ message: "Failed to fetch project", error: error.message });
    }
}



module.exports.getLeastBusyReviewers = getLeastBusyReviewers;


