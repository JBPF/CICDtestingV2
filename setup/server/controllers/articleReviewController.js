const ArticleReview = require('../models/ArticleReview');
const ReviewInstance = require('../models/ReviewInstance');
const Project = require('../models/Project');
const {status} = require("express/lib/response");
const{getLeastBusyReviewers} = require('./projectController');
async function updateArticleReviewStatus(reviewInstanceId, decision) {
    const reviewInstance = await ReviewInstance.findById(reviewInstanceId);
    console.log(reviewInstanceId);
    console.log(decision);
    if (!reviewInstance) {
        console.log("Review instance not found");
    }
    const articleReview = await ArticleReview.findById(reviewInstance.articleReview).populate('project');

    if (!reviewInstance.articleReview) {
        console.log("Article review not found");
        }

    const previousStage = articleReview.currentStage;

    if (decision === 'accept') {
        articleReview.reviewCount.accept += 1;
    } else if (decision === 'reject') {
        articleReview.reviewCount.reject += 1;
    }

    console.log(articleReview.reviewCount.accept);

    if (articleReview.reviewCount.accept + articleReview.reviewCount.reject === articleReview.reviewInstances.length) {
        if (articleReview.currentStage !== 'fullArticleReview') {

            if (articleReview.reviewCount.accept >articleReview.reviewCount.reject) {
            const nextStage = determineNextStage(articleReview.currentStage);
            console.log(nextStage);
            articleReview.overallStatus = 'pending';
            articleReview.currentStage = nextStage;
            articleReview.reviewCount.accept = 0;
            articleReview.reviewCount.reject = 0;
            articleReview.overallStatus = previousStage === 'fullArticleReview' ? 'advance' : 'pending';
            await articleReview.save();
            await reassignReviewersForNextStage(articleReview._id,previousStage);

            } else if(articleReview.reviewCount.accept === articleReview.reviewCount.reject){
            articleReview.overallStatus = 'conflict';
            await assignConflictResolver(articleReview);
            }
            else if (articleReview.reviewCount.accept < articleReview.reviewCount.reject) {
                articleReview.overallStatus = 'reject';
            }

            await articleReview.save();
        }
     else {
        if (articleReview.reviewCount.accept >articleReview.reviewCount.reject) {
            articleReview.overallStatus = 'advance';

        } else if (articleReview.reviewCount.accept === articleReview.reviewCount.reject) {

            articleReview.overallStatus = 'conflict';
            await assignConflictResolver(articleReview);
        } else {
            articleReview.overallStatus = 'reject';
        }
            await articleReview.save();
    }

    }
    await articleReview.save();
    console.log("Article review status updated successfully.:" + articleReview);


}
async function assignConflictResolver(articleReview) {

    const conflictResolverId = await getLeastBusyReviewers(articleReview.project._id, 'conflict resolver', 1);

    console.log(conflictResolverId);

        const newReviewInstance = new ReviewInstance({
            project: articleReview.project,
            article: articleReview.article,
            reviewer: conflictResolverId[0],
            reviewStatus: 'pending',
            reviewStage: articleReview.currentStage,
            articleReview: articleReview._id,
        });
        await newReviewInstance.save();
        articleReview.reviewInstances.push(newReviewInstance._id);
        await articleReview.save();

}
function determineNextStage(currentStage) {
    const stageMapping = {
        'titleReview': 'abstractReview',
        'abstractReview': 'fullArticleReview',
        'titleAbstractReview': 'fullArticleReview',
    };

    return stageMapping[currentStage] || 'fullArticleReview';
}

async function reassignReviewersForNextStage(articleReviewId, previousStage) {
    const articleReview = await ArticleReview.findById(articleReviewId).populate('reviewInstances');
    const project = await Project.findById(articleReview.project);

    if (!project) {
        throw new Error("Project not found");
    }
    const selectedReviewersIds = await getLeastBusyReviewers(articleReview.project._id,'Reviewers',project.reviewersPerArticle);


    articleReview.reviewInstances = articleReview.reviewInstances.filter(instance => instance.reviewStage !== previousStage).map(instance => instance._id);
    await articleReview.save();
    for (const reviewerId of selectedReviewersIds) {
        const reviewInstance = new ReviewInstance({
            project: articleReview.project,
            article: articleReview.article,
            reviewer: reviewerId,
            reviewStatus: 'pending',
            reviewStage: articleReview.currentStage,
            articleReview: articleReview._id,
        });

        await reviewInstance.save();
        articleReview.reviewInstances.push(reviewInstance._id);
    }

    await articleReview.save();
    console.log("Reviewers re-assigned for next stage successfully.");
}

module.exports = { reassignReviewersForNextStage };
module.exports = { updateArticleReviewStatus };
