const ReviewInstance = require('../models/ReviewInstance');
const Project = require('../models/Project');
const Article = require('../models/Article');
const User = require('../models/User');
const ArticleReview = require('../models/ArticleReview');
const ArticleReviewController = require('./articleReviewController');
const { updateArticleReviewStatus } = require('./articleReviewController');

exports.createReviewInstance = async (req, res) => {
    const { projectId, articleId, reviewerId } = req.body;

    const project = await Project.findById(projectId);
    if (!project) {
        return res.status(404).json({ message: "Project not found" });
    }

    const stages = project.reviewStageConfig === 'three-stage' ?
        ['titleReview', 'abstractReview', 'fullArticleReview'] :
        ['titleAbstractReview', 'fullArticleReview'];

    for (const stage of stages) {
        const reviewInstance = new ReviewInstance({
            project: projectId,
            article: articleId,
            reviewer: reviewerId,
            reviewStage: stage,
            reviewStatus: 'pending',
        });

        await reviewInstance.save();
    }

    res.status(201).json({ message: "Review Instances created successfully for selected stages." });
};



exports.updateReviewInstance = async (req, res) => {
    const { reviewInstanceId } = req.params;
    const { decision ,criteriaSelections} = req.body;

    try {
        const reviewInstance = await ReviewInstance.findById(reviewInstanceId);
        if (!reviewInstance) {
            return res.status(404).send('Review instance not found');
        }

        reviewInstance.reviewStatus = decision;
        reviewInstance.criteriaSelections = criteriaSelections;
        await reviewInstance.save();

        await updateArticleReviewStatus(reviewInstance, decision);

        res.json({ message: 'Review instance updated successfully' });
    } catch (error) {
        console.error('Error updating review instance:', error);
        res.status(500).send('Internal server error');
    }
};

exports.aggregateReviewResults = async (req, res) => {
    const { projectId } = req.params;

    try {
        const project = await Project.findById(projectId).populate({
            path: 'studies',
            populate: {
                path: 'reviews',
                model: 'ReviewInstance'
            }
        });

        if (!project) {
            return res.status(404).json({ message: "Project not found" });
        }

        // to do later ( Logic to aggregate review results for each study)

        res.status(200).json({ message: "Review results aggregated successfully", project });
    } catch (error) {
        res.status(500).json({ message: "Failed to aggregate review results", error: error.message });
    }
};

exports.getReviewInstances= async (req, res) => {
    const { projectId, stage } = req.params;
    const userId = req.user._id;

    try {
        const reviewInstances = await ReviewInstance.find({
            project: projectId,
            reviewStage: stage,
            reviewer: userId
        }).populate('article', 'title abstract documentLink authors');

        console.log({ projectId, stage, userId: req.user._id });
        console.log("Review Instances Found:", reviewInstances);

        res.json({ reviewInstances });
    } catch (error) {
        console.error('Error fetching review instances:', error);
        res.status(500).send('Internal server error');
    }
};


exports.getReviewDetails = async (req, res) => {
    const { reviewInstanceId } = req.params;

    try {
        const reviewInstance = await ReviewInstance.findById(reviewInstanceId)
            .populate('reviewer', 'name email')
            .populate('article', 'title authors');

        if (!reviewInstance) {
            return res.status(404).json({ message: "Review Instance not found" });
        }

        res.status(200).json({ reviewInstance });
    } catch (error) {
        res.status(500).json({ message: "Failed to fetch review details", error: error.message });
    }
};


