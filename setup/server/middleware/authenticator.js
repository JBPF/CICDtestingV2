const jwt = require('jsonwebtoken');
const axios = require('axios');

const authenticationMiddleware = (req, res, next) => {
    // next();
    const token = req.headers.authorization?.split(' ')[1];
    fetch('http://localhost:3000/api/endpoint', {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        },
    });


    console.log(req.headers['authorization']);
    if (!token) {
        return res.status(401).json({ message: 'Access denied. No token provided.' });
    }
    try {
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        req.user = decoded;
        next();
    } catch (error) {
        return res.status(401).json({ message: 'Invalid token.' });
    }
};

module.exports = authenticationMiddleware;
