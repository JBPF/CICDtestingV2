const UserRole = require('../models/UserRole');
const Permission = require('../models/Permission');
const actions = require('../utils/actions');
const verifyPermission = (requiredPermission) => async (req, res, next) => {
    next();
    // try {
    //     // Find user roles and populate the permissions for each role
    //     const userRoles = await UserRole.find({ user: req.user._id }).populate({
    //         path: 'role',
    //         populate: {
    //             path: 'permissions',
    //             model: 'Permission'
    //         }
    //     });
    //     // Check if any of the user's roles have the required permission
    //     const hasPermission = userRoles.some(userRole =>
    //         userRole.role.permissions.some(permission =>
    //             permission.action === requiredPermission
    //         )
    //     );
    //
    //
    //
    //     if (!hasPermission) {
    //         return res.status(403).json({ message: "Insufficient permissions." });
    //     }
    //     next();
    // } catch (error) {
    //     res.status(500).json({ message: "Error verifying permissions", error });
    // }
};




module.exports = verifyPermission;
