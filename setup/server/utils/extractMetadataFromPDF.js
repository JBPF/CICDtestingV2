const fs = require('fs');
const { OpenAI } = require("openai");
const pdfParse = require('pdf-parse');

const openai = new OpenAI(process.env.OPENAI_API_KEY);

const extractAbstractFromPDF = async (filePath) => {
    const dataBuffer = fs.readFileSync(filePath);
    const data = await pdfParse(dataBuffer, {
        max: 20 // Limit to the first 20 pages
    });


    const textContent = data.text.slice(0, 3000); // Adjust based on your needs

    try {
        const response = await openai.chat.completions.create({
            model: "gpt-4-turbo-preview",
            messages: [{
                role: "system",
                content: "Extract the abstract from the following text:"
            }, {
                role: "user",
                content: textContent
            }]
        });
        const abstractText = response.choices[0].message.content.trim();


        return { abstract: abstractText };
    } catch (error) {
        console.error("Error extracting abstract with OpenAI:", error);
        return { abstract: "Abstract extraction failed" };
    }
};

module.exports = extractAbstractFromPDF;
