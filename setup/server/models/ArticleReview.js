const mongoose = require('mongoose');

const articleReviewSchema = new mongoose.Schema({
    article: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Article',
        required: true
    },
    project: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Project',
        required: true
    },
    reviewInstances: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ReviewInstance'
    }],
    overallStatus: {
        type: String,
        enum: ['pending', 'advance', 'reject', 'conflict'],
        default: 'pending'
    },

    currentStage: {
        type: String,
        enum: ['titleReview', 'abstractReview', 'fullArticleReview', 'titleAbstractReview'],
        required: true
    },
    threshold: {
        type: Number,
        required: true,
        default: 2
    },
    reviewCount: {
        accept: { type: Number, default: 0 },
        reject: { type: Number, default: 0 }
    },

    conflictResolution: String,
}, {
    timestamps: true
});
module.exports = mongoose.model('ArticleReview', articleReviewSchema);