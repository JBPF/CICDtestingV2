const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const teamMemberSchema = new Schema({
    userId: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    userRole: { type: Schema.Types.ObjectId, ref: 'Role', required: true }
}, { _id: false });

const teamSchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    description: String,

    members: [teamMemberSchema],
    projects: [{
        type: Schema.Types.ObjectId,
        ref: 'Project',
        required: false
    }]
}, { timestamps: true });

teamSchema.pre('save', function(next) {
    const userIds = this.members.map(member => member.userId.toString());
    const uniqueUserIds = [...new Set(userIds)];
    if (uniqueUserIds.length !== userIds.length) {
        next(new Error('Duplicate team members detected'));
    } else {
        next();
    }
});

const Team = mongoose.model('Team', teamSchema);
module.exports = Team;
