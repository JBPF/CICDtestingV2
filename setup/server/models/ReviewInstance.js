const mongoose = require('mongoose');

const reviewInstanceSchema = new mongoose.Schema({
    project: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Project',
        required: true
    },
    article: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Article',
        required: true
    },
    reviewer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    articleReview: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ArticleReview',
        required: true
    },
    reviewStatus: {
        type: String,
        enum: ['pending', 'accept', 'reject'],
        default: 'pending'
    },
    reviewStage: {
        type: String,
        required: true
    },
    criteriaSelections: [{
        criterionId: Number,
        selected: Boolean,
        comment: String,
    }],
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    }
});

reviewInstanceSchema.pre('save', function(next) {
    this.updatedAt = Date.now();
    next();
});

module.exports = mongoose.model('ReviewInstance', reviewInstanceSchema);
