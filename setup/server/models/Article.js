const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const articleSchema = new Schema({
  title:{
    type: String,
    required: true,
    trim: true
  },
  authors: [String],
  abstract: {
    type: String,
    trim: true
  },
  publicationDate: Date,
  documentLink: {
    type: String,
    trim: true
  },
  documentType: {
    type: String,
    enum: ['pdf', 'xml', 'pubmed'],
    required: true
  },
  projects: [{
    type: Schema.Types.ObjectId,
    ref: 'Project'
  }],

  reviews: [{
        type: Schema.Types.ObjectId,
        ref: 'ArticleReview'
    }]
});

const Article = mongoose.model('Article', articleSchema);
module.exports = Article;
