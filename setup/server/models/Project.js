const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const projectSchema = new Schema({
    title: {
        type: String,
        trim: true,
        required: 'Title is required'
    },
    description: String,
    startDate: {
        type: Date,
        required: 'Start date is required',
        default: Date.now
    },
    manager: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
    collaborators: [{
        type: Schema.Types.ObjectId,
        ref: 'User',
        role: {
            type: String,
            default: 'collaborator'
        }
    }],
    reviewersPerArticle: {
        type: Number,
        required: true
    },

    teams: [{
        type: Schema.Types.ObjectId,
        ref: 'Team'
    }],
    studies: [{
        type: Schema.Types.ObjectId,
        ref: 'Article'
    }],
    inclusionCriteria: [{
        id: Number,
        value: String
    }],
    exclusionCriteria: [{
        id: Number,
        value: String
    }],
    extractionVariables: [{
        id: Number,
        value: String
    }],
    reviewStageConfig: {
        type: String,
        enum: ['three-stage', 'two-stage'],
        required: true
    },
    stages: Schema.Types.Mixed,
    flaggedDuplicates: [{ type: Schema.Types.ObjectId, ref: 'Article' }]
});

module.exports = mongoose.model('Project', projectSchema);