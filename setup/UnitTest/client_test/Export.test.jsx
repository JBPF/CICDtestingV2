import { describe, it, expect } from 'vitest';
import { render, fireEvent } from '@testing-library/react';
import StickyHeadTable from './StickyHeadTable';

describe('StickyHeadTable Component', () => {
  it('renders table with correct rows and columns', () => {
    const { getAllByRole } = render(<StickyHeadTable />);
    const columnHeaders = getAllByRole('columnheader');
    expect(columnHeaders.length).toBe(4); 

    const rows = getAllByRole('row');
    expect(rows.length).toBe(4); 
  });

  it('changes page when pagination is used', () => {
    const { getByText, getByRole } = render(<StickyHeadTable />);
    fireEvent.change(getByRole('combobox'), { target: { value: 25 } });
    fireEvent.click(getByText('Next page'));

    const rows = getAllByRole('row');
    expect(rows.length).toBe(1); 
  });

  it('exports data to CSV when export button is clicked', () => {
    const { getByText } = render(<StickyHeadTable />);
    const exportButton = getByText('Export to CSV');
    expect(exportButton).toBeInTheDocument();
  });
});
