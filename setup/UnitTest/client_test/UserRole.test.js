import { describe, it, expect, vi, beforeEach } from 'vitest';
import { render, fireEvent } from '@testing-library/react';
import Modal from 'react-modal';
import UserRoleModal from './UserRoleModal';
import { listUserRoles } from '../services/userRoleService';

vi.mock('react-modal');
vi.mock('../services/userRoleService', () => ({
  listUserRoles: vi.fn(),
}));

describe('UserRoleModal Component', () => {
  beforeEach(() => {
    vi.resetAllMocks();
    Modal.setAppElement(document.createElement('div'));
  });

  it('opens and displays user role details', async () => {
    const mockRoles = [{ projectName: 'Project 1', teamName: 'Team A', role: 'Developer' }];
    listUserRoles.mockResolvedValue(mockRoles);
    const { findByText, getByText } = render(
      <UserRoleModal isOpen={true} handleClose={vi.fn()} selectedUser={{ _id: 'user1', email: 'user1@example.com' }} />
    );

    expect(await findByText(/User Role Details/)).toBeInTheDocument();
    expect(getByText(/Project: Project 1/)).toBeInTheDocument();
    expect(listUserRoles).toHaveBeenCalledWith('user1');
  });

  it('closes modal on Close button click', () => {
    const handleClose = vi.fn();
    const { getByText } = render(
      <UserRoleModal isOpen={true} handleClose={handleClose} selectedUser={{ _id: 'user1', email: 'user1@example.com' }} />
    );

    fireEvent.click(getByText('Close'));
    expect(handleClose).toHaveBeenCalled();
  });

  it('opens assign role modal on button click', () => {
    const { getByText, getByLabelText } = render(
      <UserRoleModal isOpen={true} handleClose={vi.fn()} selectedUser={{ _id: 'user1', email: 'user1@example.com' }} />
    );

    fireEvent.click(getByText(/Assign new role to user/));
    expect(getByLabelText(/Assign Role/)).toBeInTheDocument();
  });
});
