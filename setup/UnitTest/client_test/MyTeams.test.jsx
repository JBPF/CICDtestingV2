import { describe, it, expect, vi, beforeEach } from 'vitest';
import { render, fireEvent, screen } from '@testing-library/react';
import MyTeams from './MyTeams';
import { getTeamDetails, fetchUsers, fetchRoles } from '../../services/teamsService';

vi.mock('../../services/teamsService', () => ({
  getTeamDetails: vi.fn(),
  fetchUsers: vi.fn(),
  fetchRoles: vi.fn(),
}));

describe('MyTeams Component', () => {
  const mockTeams = [
    { _id: '1', name: 'Team A', description: 'Description A' },
    { _id: '2', name: 'Team B', description: 'Description B' },
  ];

  const mockUsers = [
    { _id: 'u1', name: 'User 1' },
    { _id: 'u2', name: 'User 2' },
  ];

  const mockRoles = [
    { _id: 'r1', name: 'Role 1' },
    { _id: 'r2', name: 'Role 2' },
  ];

  beforeEach(() => {
    getTeamDetails.mockResolvedValue(mockTeams);
    fetchUsers.mockResolvedValue(mockUsers);
    fetchRoles.mockResolvedValue(mockRoles);
  });

  it('loads and displays teams', async () => {
    render(<MyTeams />);
    expect(await screen.findByText('Team A')).toBeInTheDocument();
    expect(await screen.findByText('Team B')).toBeInTheDocument();
  });

  it('opens CreateTeamModal when button is clicked', async () => {
    render(<MyTeams />);
    fireEvent.click(screen.getByText('My Teams'));
    expect(await screen.findByText('Create New Team')).toBeInTheDocument();
  });

  it('opens EditTeamModal when a team is clicked', async () => {
    render(<MyTeams />);
    fireEvent.click(await screen.findByText('Team A'));
    expect(await screen.findByText('Edit Team')).toBeInTheDocument();
  });
});
