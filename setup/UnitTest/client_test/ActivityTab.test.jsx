import { describe, it, expect } from 'vitest';
import { render } from '@testing-library/react';
import ActivityTab from './ActivityTab';

describe('ActivityTab Component', () => {
  it('renders activity list correctly', () => {
    const { getByText } = render(<ActivityTab />);

    expect(getByText('Accepted review for project A')).toBeInTheDocument();
    expect(getByText('Completed data extraction for project B')).toBeInTheDocument();
    expect(getByText('Resolved conflicts for project C')).toBeInTheDocument();
    
    expect(getByText('2023-02-10 14:00')).toBeInTheDocument();
    expect(getByText('2023-02-12 09:30')).toBeInTheDocument();
    expect(getByText('2024-02-12 11:30')).toBeInTheDocument();
  });
});
