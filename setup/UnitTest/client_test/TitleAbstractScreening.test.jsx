import { describe, it, expect, vi } from 'vitest';
import { render, fireEvent, screen } from '@testing-library/react';
import TitleAbstractReview from './TitleAbstractReview';
import axios from 'axios';
import * as reviewService from '../../services/reviewService';


vi.mock('axios');
vi.mock('../../services/reviewService');


const reviewInstances = [
  { _id: '1', article: { title: 'Title 1', abstract: 'Abstract 1' }, reviewStatus: 'pending', isExpanded: false },
  { _id: '2', article: { title: 'Title 2', abstract: 'Abstract 2' }, reviewStatus: 'pending', isExpanded: false }
];

describe('TitleAbstractReview Component', () => {
  beforeEach(() => {
    
    axios.get.mockResolvedValue({ data: { reviewInstances } });
  });

  it('renders review instances and handles decisions', async () => {
    render(<TitleAbstractReview />);

    
    expect(await screen.findAllByRole('article')).toHaveLength(reviewInstances.length);

    
    fireEvent.click(screen.getAllByText('Accept')[0]);
    expect(reviewService.updateReviewDecision).toHaveBeenCalledWith(reviewInstances[0]._id, 'accept');

    
    fireEvent.click(screen.getAllByText('Reject')[1]);
    expect(reviewService.updateReviewDecision).toHaveBeenCalledWith(reviewInstances[1]._id, 'reject');
  });

  
});
