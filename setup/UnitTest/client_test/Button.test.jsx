
import { describe, it, expect } from 'vitest';
import { createRoot } from 'react-dom/client';
import Button from '../../client/src/Components/MyTeamsForm/Button';

describe('Button Component', () => {
  it('renders correctly with custom style', () => {
    const container = document.createElement('div');
    const root = createRoot(container);
    root.render(<Button style={{ color: 'rgb(255, 255, 255)' }}>Test Button</Button>);

    const button = container.querySelector('button');
    expect(button).toBeTruthy(); 
    expect(button.textContent).toBe('Test Button');
    expect(button.style.backgroundColor).toBe('rgb(23, 23, 30)'); 
    expect(button.style.color).toBe('rgb(255, 255, 255)'); 
  });
});

