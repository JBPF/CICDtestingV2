import { describe, it, expect, vi } from 'vitest';
import { render, screen, waitFor } from '@testing-library/react';
import OneProject from './OneProject';
import axios from 'axios';
import { MemoryRouter, Route, Routes } from 'react-router-dom';

vi.mock('axios');

describe('OneProject Component', () => {
  const mockProjectData = {
    title: 'Test Project',
    reviewStageConfig: 'three-stage',
  };

  beforeEach(() => {
    axios.get.mockResolvedValue({ data: { project: mockProjectData } });
  });

  it('fetches and displays project title', async () => {
    render(
      <MemoryRouter initialEntries={['/projects/1']}>
        <Routes>
          <Route path="/projects/:projectId" element={<OneProject />} />
        </Routes>
      </MemoryRouter>
    );

    await waitFor(() => {
      expect(screen.getByText('Test Project')).toBeInTheDocument();
    });

    expect(axios.get).toHaveBeenCalledWith(expect.stringContaining('/api/projects/1'), expect.anything());
  });

  it('renders stages based on reviewStageConfig', async () => {
    render(
      <MemoryRouter initialEntries={['/projects/1']}>
        <Routes>
          <Route path="/projects/:projectId" element={<OneProject />} />
        </Routes>
      </MemoryRouter>
    );

    await waitFor(() => {
      expect(screen.getByText('Title Review')).toBeInTheDocument();
      expect(screen.getByText('Abstract Review')).toBeInTheDocument();
      expect(screen.getByText('Full Article Review')).toBeInTheDocument();
    });
  });

  it('navigates to the Prisma Flowchart section', async () => {
    render(
      <MemoryRouter initialEntries={['/projects/1']}>
        <Routes>
          <Route path="/projects/:projectId" element={<OneProject />} />
          <Route path="/prisma" element={<div>Prisma Flowchart</div>} />
        </Routes>
      </MemoryRouter>
    );

    await waitFor(() => {
      screen.getByText('Generate Prisma').click();
    });

    expect(screen.getByText('Prisma Flowchart')).toBeInTheDocument();
  });
});
