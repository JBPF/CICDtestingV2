import { describe, it, beforeEach, vi } from 'vitest';
import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';
import CreateTeamModal from './CreateTeamModal';


const mockData = {
  users: [{ _id: 'user1', name: 'User One' }, { _id: 'user2', name: 'User Two' }],
  roles: [{ _id: 'role1', name: 'Role One' }, { _id: 'role2', name: 'Role Two' }],
};


vi.mock('../../services/teamsService', () => ({
  createTeam: vi.fn(() => Promise.resolve()),
}));

describe('CreateTeamModal Component', () => {
  beforeEach(() => {
    
    vi.resetAllMocks();
  });

  it('renders correctly when open', () => {
    render(<CreateTeamModal isOpen={true} onClose={vi.fn()} data={mockData} />);
    expect(screen.getByText('Create New Team')).toBeInTheDocument();
  });

  it('calls onClose when modal is cancelled', () => {
    const mockOnClose = vi.fn();
    render(<CreateTeamModal isOpen={true} onClose={mockOnClose} data={mockData} />);
    fireEvent.click(screen.getByText('Cancel')); 
    expect(mockOnClose).toHaveBeenCalled();
  });

  
});
