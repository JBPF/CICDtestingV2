import { describe, it, expect, vi } from 'vitest';
import { render, fireEvent, screen } from '@testing-library/react';
import TodoListTab from './TodoListTab';

describe('TodoListTab Component', () => {
  it('adds a new todo item', () => {
    render(<TodoListTab />);
    const input = screen.getByLabelText(/Add To-Do Item/i);
    const addButton = screen.getByText(/Add Task/i);

    fireEvent.change(input, { target: { value: 'New Task' } });
    fireEvent.click(addButton);

    expect(screen.getByText('New Task')).toBeInTheDocument();
  });

  it('toggles completion of a todo item', () => {
    render(<TodoListTab />);
    const input = screen.getByLabelText(/Add To-Do Item/i);
    const addButton = screen.getByText(/Add Task/i);

    fireEvent.change(input, { target: { value: 'New Task' } });
    fireEvent.click(addButton);
    const todoCheckbox = screen.getByRole('checkbox');

    fireEvent.click(todoCheckbox);

    expect(todoCheckbox).toBeChecked();
  });

  it('deletes a todo item', () => {
    render(<TodoListTab />);
    const input = screen.getByLabelText(/Add To-Do Item/i);
    const addButton = screen.getByText(/Add Task/i);

    fireEvent.change(input, { target: { value: 'New Task' } });
    fireEvent.click(addButton);
    const deleteButton = screen.getByRole('button', { name: /delete/i });

    fireEvent.click(deleteButton);

    expect(screen.queryByText('New Task')).not.toBeInTheDocument();
  });
});
