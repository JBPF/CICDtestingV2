import { describe, it, expect, vi } from 'vitest';
import { render, fireEvent, screen } from '@testing-library/react';
import FullTextReview from './FullTextReview';
import axios from 'axios';
import { MemoryRouter } from 'react-router-dom';

vi.mock('axios');

describe('FullTextReview Component', () => {
  const mockReviewInstances = [
    {
      _id: '1',
      article: {
        documentLink: 'article1.pdf',
      },
      reviewStatus: 'pending',
    },
    {
      _id: '2',
      article: {
        documentLink: 'article2.pdf',
      },
      reviewStatus: 'pending',
    },
  ];

  beforeEach(() => {
    axios.get.mockResolvedValue({ data: { reviewInstances: mockReviewInstances } });
    axios.patch.mockResolvedValue({});
  });

  it('fetches and displays review instances', async () => {
    render(<MemoryRouter><FullTextReview /></MemoryRouter>);

    expect(await screen.findByText('Full Text Review')).toBeInTheDocument();
    expect(axios.get).toHaveBeenCalled();
  });

  it('handles accept decision', async () => {
    render(<MemoryRouter><FullTextReview /></MemoryRouter>);

    const acceptButton = await screen.findByText('Accept');
    fireEvent.click(acceptButton);

    expect(axios.patch).toHaveBeenCalledWith(expect.anything(), { decision: 'accept', criteriaSelections: [] }, expect.anything());
  });

  it('handles reject decision with selected criteria', async () => {
    render(<MemoryRouter><FullTextReview /></MemoryRouter>);

    const rejectButton = await screen.findByText('Reject');
    fireEvent.click(rejectButton);

    // Simulate selection of criteria before rejection
    fireEvent.change(screen.getByTestId('criteria-selection'), { target: { value: 'some-criteria-id' } });

    fireEvent.click(rejectButton);

    expect(axios.patch).toHaveBeenCalledWith(expect.anything(), { decision: 'reject', criteriaSelections: ['some-criteria-id'] }, expect.anything());
  });
});
