import { describe, it, expect, vi } from 'vitest';
import { render, fireEvent, screen } from '@testing-library/react';
import NotificationsTab from './NotificationsTab';

describe('NotificationsTab Component', () => {
  it('renders notifications', () => {
    render(<NotificationsTab />);

    expect(screen.getByText('Project is waiting for review')).toBeInTheDocument();
    expect(screen.getByText('New reviews added for extraction in the project')).toBeInTheDocument();
    expect(screen.getByText('You have screened 6 studies today')).toBeInTheDocument();
  });

  it('marks a notification as read', () => {
    render(<NotificationsTab />);
    const unreadNotificationsBefore = screen.getAllByRole('button');
    expect(unreadNotificationsBefore.length).toBe(2); // There should be 2 unread notifications initially

    fireEvent.click(unreadNotificationsBefore[0]); // Click on the first unread notification
    const unreadNotificationsAfter = screen.queryAllByRole('button');
    expect(unreadNotificationsAfter.length).toBe(1); // There should be only 1 unread notification left after marking the first one as read
  });
});
