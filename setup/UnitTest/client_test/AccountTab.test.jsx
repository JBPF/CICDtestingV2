import { describe, it, expect, vi } from 'vitest';
import { render, fireEvent, screen } from '@testing-library/react';
import AccountTab from './AccountTab';
import axios from 'axios';

vi.mock('axios');

describe('AccountTab Component', () => {
  beforeEach(() => {
    axios.get.mockResolvedValue({
      data: {
        email: 'user@example.com',
        name: 'John Doe',
        avatar: 'avatar.png',
      },
    });
  });

  it('loads and displays user info', async () => {
    render(<AccountTab />);

    expect(await screen.findByText(/John Doe/)).toBeInTheDocument();
    expect(await screen.findByText(/user@example.com/)).toBeInTheDocument();
  });

  it('updates interest when new interest is added', () => {
    render(<AccountTab />);

    const interestInput = screen.getByLabelText('Add New Interest');
    fireEvent.change(interestInput, { target: { value: 'React' } });
    fireEvent.click(screen.getByText('Add'));

    expect(screen.getByText('React')).toBeInTheDocument();
  });

  it('removes interest when delete icon is clicked', () => {
    render(<AccountTab />);

    const interestInput = screen.getByLabelText('Add New Interest');
    fireEvent.change(interestInput, { target: { value: 'React' } });
    fireEvent.click(screen.getByText('Add'));
    fireEvent.click(screen.getByLabelText('Delete React'));

    expect(screen.queryByText('React')).not.toBeInTheDocument();
  });

  it('submits new password', () => {
    const handleSubmit = vi.fn();
    render(<AccountTab onSubmit={handleSubmit} />);

    fireEvent.change(screen.getByLabelText('Current Password'), { target: { value: 'oldpassword' } });
    fireEvent.change(screen.getByLabelText('New Password'), { target: { value: 'newpassword' } });
    fireEvent.change(screen.getByLabelText('Confirm New Password'), { target: { value: 'newpassword' } });
    fireEvent.click(screen.getByText('Submit'));

    expect(handleSubmit).toHaveBeenCalledWith({
      currentPassword: 'oldpassword',
      newPassword: 'newpassword',
      confirmNewPassword: 'newpassword',
    });
  });
});
