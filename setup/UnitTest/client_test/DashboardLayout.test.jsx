import { describe, it, expect } from 'vitest';
import { DashboardLayout } from '../../client/src/Components/MyTeamsForm/DashboardLayout';

describe('DashboardLayout Component', () => {
  it('is defined', () => {
    expect(DashboardLayout).toBeDefined();
  });
});
