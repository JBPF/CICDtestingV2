import { describe, it, expect, vi } from 'vitest';
import { render, screen } from '@testing-library/react';
import TitleAbstract from './TitleAbstract';

vi.mock('!file-loader!../../../public', () => {
  return {
    keys: () => ['./file1.txt', './file2.txt'],
    './file1.txt': 'Title: Test Title 1\nAbstract: Test Abstract 1',
    './file2.txt': 'Title: Test Title 2\nAbstract: Test Abstract 2',
  };
});

vi.mock('axios', () => ({
  get: vi.fn(() => Promise.resolve({ data: 'Test Title\nTest Abstract' })),
}));

describe('TitleAbstract Component', () => {
  it('renders titles and abstracts', async () => {
    render(<TitleAbstract />);

    
    expect(await screen.findByText('Test Title 1')).toBeInTheDocument();
    expect(screen.getByText('Test Abstract 1')).toBeInTheDocument();
    expect(screen.getByText('Test Title 2')).toBeInTheDocument();
    expect(screen.getByText('Test Abstract 2')).toBeInTheDocument();
  });

  
});
