import { describe, it, expect, vi } from 'vitest';
import { render, fireEvent, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import Projects from './Projects';
import axios from 'axios';

vi.mock('axios');

describe('Projects Component', () => {
  const mockProjects = [
    { _id: '1', title: 'Project 1', progression: 30 },
    { _id: '2', title: 'Project 2', progression: 60 },
    
  ];

  beforeEach(() => {
    axios.get.mockResolvedValue({ data: { projects: mockProjects } });
  });

  it('renders projects and handles pagination', async () => {
    render(<MemoryRouter><Projects /></MemoryRouter>);

    t
    expect(await screen.findByText('Project 1')).toBeInTheDocument();
    expect(screen.getByText('Project 2')).toBeInTheDocument();

    
    fireEvent.click(screen.getByText('Next'));


    fireEvent.click(screen.getByText('Previous'));

    
  });

});
