import { describe, it, expect, vi } from 'vitest';
import { render, fireEvent } from '@testing-library/react';
import Dashboard from './Dashboard';
import Modal from 'react-modal';

vi.mock('react-modal', () => ({
  ...vi.importActual('react-modal'),
  setAppElement: vi.fn(),
}));

describe('Dashboard Component', () => {
  it('changes tabs correctly', () => {
    const { getByText } = render(<Dashboard />);
    const completedProjectsTab = getByText('Completed Projects');
    fireEvent.click(completedProjectsTab);
    expect(getByText('Completed Projects')).toBeInTheDocument();
  });

  it('opens and closes modal correctly', () => {
    const { getByText, queryByText } = render(<Dashboard />);
    const createProjectButton = getByText('Create Project');
    fireEvent.click(createProjectButton);
    expect(queryByText('Create New Project')).toBeInTheDocument();
    
    const closeModalButton = getByText('Close');
    fireEvent.click(closeModalButton);
    expect(queryByText('Create New Project')).not.toBeInTheDocument();
  });

  it('displays projects or completed projects based on selected tab', () => {
    const { getByText, queryByText } = render(<Dashboard />);
    expect(queryByText('Projects')).toBeInTheDocument();
    expect(queryByText('Completed Projects')).not.toBeInTheDocument();

    const completedProjectsTab = getByText('Completed Projects');
    fireEvent.click(completedProjectsTab);
    expect(queryByText('Completed Projects')).toBeInTheDocument();
    expect(queryByText('Projects')).not.toBeInTheDocument();
  });
});
