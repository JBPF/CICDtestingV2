import { describe, it, expect, vi } from 'vitest';
import { render, fireEvent, screen } from '@testing-library/react';
import axios from 'axios';
import { MemoryRouter } from 'react-router-dom';
import Login from './Login';

vi.mock('axios');
vi.mock('react-router-dom', () => ({
  ...vi.importActual('react-router-dom'),
  useNavigate: vi.fn(),
}));

describe('Login Component', () => {
  it('submits login credentials and navigates on success', async () => {
    axios.post.mockResolvedValue({ data: { token: 'fake-token' } });
    const navigate = vi.fn();
    vi.mocked(useNavigate).mockImplementation(() => navigate);

    render(
      <MemoryRouter>
        <Login />
      </MemoryRouter>
    );

    fireEvent.change(screen.getByLabelText(/Email address/), { target: { value: 'test@example.com' } });
    fireEvent.change(screen.getByLabelText(/Password/), { target: { value: 'password' } });
    fireEvent.click(screen.getByText(/Log in/));

    expect(axios.post).toHaveBeenCalledWith('http://localhost:3000/users/login', {
      email: 'test@example.com',
      password: 'password',
    });

    await vi.waitFor(() => expect(navigate).toHaveBeenCalledWith('/dashboard'));
  });

  it('displays error message on login failure', async () => {
    axios.post.mockRejectedValue({ response: { data: { message: 'Invalid credentials' } } });

    render(
      <MemoryRouter>
        <Login />
      </MemoryRouter>
    );

    fireEvent.change(screen.getByLabelText(/Email address/), { target: { value: 'wrong@example.com' } });
    fireEvent.change(screen.getByLabelText(/Password/), { target: { value: 'wrongpassword' } });
    fireEvent.click(screen.getByText(/Log in/));

    expect(axios.post).toHaveBeenCalledWith('http://localhost:3000/users/login', {
      email: 'wrong@example.com',
      password: 'wrongpassword',
    });

    await vi.waitFor(() => expect(screen.getByRole('alert')).toHaveTextContent('Invalid credentials'));
  });
});
