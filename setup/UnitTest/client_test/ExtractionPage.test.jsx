import { describe, it, expect, vi } from 'vitest';
import { render, fireEvent, screen } from '@testing-library/react';
import ExtractionPage from './ExtractionPage';

vi.mock('@react-pdf-viewer/core', () => ({
  Worker: ({ children }) => <div>{children}</div>,
  Viewer: () => <div />,
}));

vi.mock('@react-pdf-viewer/highlight', () => ({
  highlightPlugin: () => ({}),
  Trigger: { None: 'None' },
}));

describe('ExtractionPage Component', () => {
  it('shows the select button when text is selected', async () => {
    render(<ExtractionPage />);
    
    // Mock text selection
    const mockRange = document.createRange();
    const mockSelection = {
      rangeCount: 1,
      getRangeAt: () => mockRange,
      toString: () => 'Selected text',
    };
    window.getSelection = () => mockSelection;
    document.execCommand = vi.fn();

    // Trigger selection change event
    fireEvent.mouseUp(window);

    expect(await screen.findByText('Select')).toBeInTheDocument();
  });

  it('hides the select button when clicking outside', async () => {
    render(<ExtractionPage />);
    
    // Trigger selection change event to show the button
    fireEvent.mouseUp(window);

    // Click outside to hide the button
    fireEvent.mouseDown(document);

    expect(screen.queryByText('Select')).not.toBeInTheDocument();
  });

  it('fills the form with selected text', async () => {
    render(<ExtractionPage />);
    
    // Mock text selection
    const mockRange = document.createRange();
    const mockSelection = {
      rangeCount: 1,
      getRangeAt: () => mockRange,
      toString: () => 'Selected text',
    };
    window.getSelection = () => mockSelection;
    document.execCommand = vi.fn();

    // Trigger selection change event to show the button
    fireEvent.mouseUp(window);

    // Click the select button to show options
    fireEvent.click(screen.getByText('Select'));

    // Mock clicking an option to insert the text
    fireEvent.click(screen.getByText('Option Name'));

    expect(screen.getByDisplayValue('Selected text')).toBeInTheDocument();
  });
});
