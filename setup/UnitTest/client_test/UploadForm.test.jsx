import { describe, it, expect, vi } from 'vitest';
import { render, fireEvent, screen } from '@testing-library/react';
import axios from 'axios';
import CreateProjectForm from './CreateProjectForm';
import { ToastContainer } from 'react-toastify';

vi.mock('axios');
vi.mock('react-toastify', () => ({
  ToastContainer: vi.fn(() => null),
  toast: vi.fn(),
}));

describe('CreateProjectForm', () => {
  it('submits form and sends project data', async () => {
    axios.post.mockResolvedValueOnce({ data: { project: { _id: '1' }, message: 'Project created' } });
    axios.post.mockResolvedValueOnce({ data: { message: 'Reviewer invited' } });

    render(<CreateProjectForm />);
    fireEvent.change(screen.getByLabelText('Title:'), { target: { value: 'Test Project' } });
    fireEvent.change(screen.getByLabelText('Description:'), { target: { value: 'Test Description' } });
    fireEvent.change(screen.getByLabelText('Start Date:'), { target: { value: '2023-01-01' } });
    fireEvent.change(screen.getByLabelText('Owner:'), { target: { value: '123' } });
    fireEvent.change(screen.getByLabelText('PubMed IDs:'), { target: { value: '123456' } });
    fireEvent.change(screen.getByLabelText('Select team:'), { target: { value: 'Team 1- John, Alice' } });
    fireEvent.change(screen.getByLabelText('Invite Reviewer:'), { target: { value: 'reviewer@example.com' } });
    fireEvent.change(screen.getByLabelText('Reviewer Role:'), { target: { value: 'Reviewer' } });

    fireEvent.click(screen.getByText('Create Project'));

    expect(axios.post).toHaveBeenCalledWith('http://localhost:3000/project/', expect.any(Object));
    expect(axios.post).toHaveBeenCalledWith(`http://localhost:3000/project/1/invite`, expect.any(Object));
  });
});
