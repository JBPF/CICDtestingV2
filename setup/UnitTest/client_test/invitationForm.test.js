import { describe, it, expect, beforeEach, vi } from 'vitest';
import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import InviteUserForm from './InviteUserForm';
import { sendInvitation } from '../services/userService';


vi.mock('../services/userService', () => ({
  sendInvitation: vi.fn(),
}));

describe('InviteUserForm Component', () => {
  beforeEach(() => {
    
    vi.resetAllMocks();
  });

  it('submits the form and sends an invitation', async () => {
    const { getByLabelText, getByText } = render(<InviteUserForm closeModal={vi.fn()} />);
    const emailInput = getByLabelText('Email');
    const sendButton = getByText('Send invitation');

    
    fireEvent.change(emailInput, { target: { value: 'test@example.com' } });

   
    fireEvent.click(sendButton);

    
    expect(sendInvitation).toHaveBeenCalledWith({ email: 'test@example.com' });
  });
});
