import { describe, it, expect } from 'vitest';
import React from 'react';
import { renderToString } from 'react-dom/server';
import PageHeader from './PageHeader';

describe('PageHeader Component', () => {
  it('is defined', () => {
    expect(PageHeader).toBeDefined();
  });

  it('renders the title correctly', () => {
    const title = 'Test Title';
    const component = renderToString(<PageHeader title={title} />);
    expect(component.includes(title)).toBe(true);
  });

  it('renders a button when onButtonClick is provided', () => {
    const onButtonClick = () => {};
    const component = renderToString(<PageHeader title="Test" onButtonClick={onButtonClick} />);
    expect(component.includes('Create Team')).toBe(true);
  });

  it('does not render a button when onButtonClick is not provided', () => {
    const component = renderToString(<PageHeader title="Test" />);
    expect(component.includes('Create Team')).toBe(false);
  });
});
