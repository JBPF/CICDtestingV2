import { describe, it, expect, vi } from 'vitest';
import { render, fireEvent, screen } from '@testing-library/react';
import PrismaFlowchart from './PrismaFlowchart';
import { Packer } from "docx";
import { saveAs } from "file-saver";

vi.mock("docx");
vi.mock("file-saver", () => ({
  saveAs: vi.fn(),
}));

describe('PrismaFlowchart Component', () => {
  it('renders project title and stages', () => {
    const { projectId } = '1'; // Mock project ID
    render(<PrismaFlowchart projectId={projectId} />);

    expect(screen.getByText(/PRISMA for 1/)).toBeInTheDocument();
    expect(screen.getByText(/Identification/)).toBeInTheDocument();
    expect(screen.getByText(/Duplicates/)).toBeInTheDocument();
  });

  it('downloads DOCX file when download button is clicked', async () => {
    Packer.toBlob = vi.fn(() => Promise.resolve(new Blob()));
    render(<PrismaFlowchart projectId="1" />);

    fireEvent.click(screen.getByText('Download as DOCX'));

    await vi.waitFor(() => expect(Packer.toBlob).toHaveBeenCalled());
    expect(saveAs).toHaveBeenCalled();
  });
});
