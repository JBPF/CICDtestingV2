import { describe, it, expect, vi } from 'vitest';
import { getStageComponent } from './DynamicStageComponent'; 


vi.mock('../Pages/Review/TitleAbstractScreening', () => ({
  default: () => <div>TitleAbstractScreening Component</div>
}));

vi.mock('../Pages/Projects/FullTextReview', () => ({
  default: () => <div>FullTextReview Component</div>
}));

describe('DynamicStageComponent', () => {
  it('renders TitleAbstractScreening for title-abstract-review stage', () => {
    const component = renderToString(getStageComponent('title-abstract-review'));
    expect(component).toContain('TitleAbstractScreening Component');
  });

  it('renders FullTextReview for full-article-review stage', () => {
    const component = renderToString(getStageComponent('full-article-review'));
    expect(component).toContain('FullTextReview Component');
  });

  it('renders unknown stage message for invalid stage', () => {
    const component = renderToString(getStageComponent('invalid-stage'));
    expect(component).toContain('Unknown stage');
  });
});
