import { describe, it, expect, vi } from 'vitest';
import { render, fireEvent, screen } from '@testing-library/react';
import FormPlaceholder from './FormPlaceholder';

describe('FormPlaceholder Component', () => {
  const initialFormValues = {
    sponsorshipValue: 'Sponsorship Test',
    countryValue: 'Country Test',
    settingValue: 'Setting Test',
    commentsValue: 'Comments Test',
    authorNameValue: 'Author Name Test',
    institutionValue: 'Institution Test',
    emailValue: 'Email Test',
    addressValue: 'Address Test',
  };

  const handleChange = vi.fn();

  it('renders form fields with initial values', () => {
    render(<FormPlaceholder handleChange={handleChange} formValues={initialFormValues} />);

    expect(screen.getByLabelText('Sponsorship').value).toBe('Sponsorship Test');
    expect(screen.getByLabelText('Country').value).toBe('Country Test');
    expect(screen.getByLabelText('Setting').value).toBe('Setting Test');
    expect(screen.getByLabelText('Comments').value).toBe('Comments Test');
    expect(screen.getByLabelText("Author's name").value).toBe('Author Name Test');
    expect(screen.getByLabelText('Institution').value).toBe('Institution Test');
    expect(screen.getByLabelText('Email').value).toBe('Email Test');
    expect(screen.getByLabelText('Address').value).toBe('Address Test');
  });

  it('calls handleChange when a form field is changed', () => {
    render(<FormPlaceholder handleChange={handleChange} formValues={initialFormValues} />);

    fireEvent.change(screen.getByLabelText('Sponsorship'), { target: { value: 'New Sponsorship' } });
    expect(handleChange).toHaveBeenCalledWith('New Sponsorship', 'sponsorshipValue');
  });
});
