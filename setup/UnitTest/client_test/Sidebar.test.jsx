import { describe, it, expect, vi } from 'vitest';
import { render, fireEvent } from '@testing-library/react';
import { MemoryRouter, Route, Routes } from 'react-router-dom';
import Sidebar from './Sidebar';
import { SidebarData } from './SidebarData';

vi.mock('react-router-dom', () => ({
  ...vi.importActual('react-router-dom'),
  useNavigate: vi.fn(),
}));

describe('Sidebar Component', () => {
  it('renders all sidebar items', () => {
    const { getByText } = render(
      <MemoryRouter>
        <Sidebar />
      </MemoryRouter>
    );

    SidebarData.forEach((item) => {
      expect(getByText(item.title)).toBeInTheDocument();
    });
  });

  it('navigates to login on logout', async () => {
    localStorage.setItem('token', 'dummy-token');
    const navigate = vi.fn();
    vi.mocked(useNavigate).mockImplementation(() => navigate);

    const { getByText } = render(
      <MemoryRouter>
        <Sidebar />
      </MemoryRouter>
    );

    fireEvent.click(getByText('Logout'));
    expect(navigate).toHaveBeenCalledWith('/login');
  });

  it('shows login when not authenticated', () => {
    localStorage.removeItem('token');
    const { getByText } = render(
      <MemoryRouter>
        <Sidebar />
      </MemoryRouter>
    );

    expect(getByText('Login')).toBeInTheDocument();
  });
});
