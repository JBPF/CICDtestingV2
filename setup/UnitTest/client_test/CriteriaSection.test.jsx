import { describe, it, expect, vi } from 'vitest';
import { render, fireEvent, screen } from '@testing-library/react';
import CriteriaSection from './CriteriaSection';
import axios from 'axios';

vi.mock('axios');

describe('CriteriaSection Component', () => {
  const mockCriteria = {
    inclusionCriteria: [
      { id: 1, value: 'Inclusion Criterion 1' },
      { id: 2, value: 'Inclusion Criterion 2' },
    ],
    exclusionCriteria: [
      { id: 1, value: 'Exclusion Criterion 1' },
      { id: 2, value: 'Exclusion Criterion 2' },
    ],
  };

  beforeEach(() => {
    axios.get.mockResolvedValue({ data: mockCriteria });
  });

  it('renders inclusion and exclusion criteria', async () => {
    render(<CriteriaSection projectId="1" onCriteriaChange={() => {}} />);

    expect(await screen.findByText('Inclusion Criterion 1')).toBeInTheDocument();
    expect(await screen.findByText('Inclusion Criterion 2')).toBeInTheDocument();
    expect(await screen.findByText('Exclusion Criterion 1')).toBeInTheDocument();
    expect(await screen.findByText('Exclusion Criterion 2')).toBeInTheDocument();
  });

  it('updates exclusion criteria selection', async () => {
    const handleCriteriaChange = vi.fn();
    render(<CriteriaSection projectId="1" onCriteriaChange={handleCriteriaChange} />);

    const checkbox = await screen.findByLabelText('Exclusion Criterion 1');
    fireEvent.click(checkbox);

    expect(handleCriteriaChange).toHaveBeenCalledWith([{ criterionId: 1, selected: true }]);
  });
});
