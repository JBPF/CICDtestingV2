import { describe, it, expect } from 'vitest';
import { render, fireEvent } from '@testing-library/react';
import ProfilePage from './ProfilePage';

describe('ProfilePage Component', () => {
  it('renders and switches tabs correctly', () => {
    const { getByText } = render(<ProfilePage />);
    
    
    expect(getByText('Account')).toBeInTheDocument();

    
    fireEvent.click(getByText('Notifications'));
    expect(getByText('Notifications')).toBeInTheDocument();

    
    fireEvent.click(getByText('To-Do List'));
    expect(getByText('To-Do List')).toBeInTheDocument();

    
    fireEvent.click(getByText('Activity'));
    expect(getByText('Activity')).toBeInTheDocument();
  });
});
