import { describe, it, expect } from 'vitest';


const generateFooterText = (year, companyName) => {
  return `© ${year} ${companyName}. All rights reserved.`;
};

describe('Footer Component Logic', () => {
  it('generates the correct footer text', () => {
    const text = generateFooterText(2024, 'TeamProject');
    expect(text).toBe('© 2024 TeamProject. All rights reserved.');
  });
});
