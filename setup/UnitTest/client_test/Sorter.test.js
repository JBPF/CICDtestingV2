import { describe, it, expect, vi } from 'vitest';
import { render, fireEvent } from '@testing-library/react';
import Sorter from './Sorter';

describe('Sorter Component', () => {
  it('renders with initial sort order', () => {
    const { getByRole } = render(<Sorter sortOrder="asc" handleSortChange={vi.fn()} />);
    const select = getByRole('button');
    expect(select.textContent).toBe('Ascending');
  });

  it('calls handleSortChange on sort order change', () => {
    const handleSortChange = vi.fn();
    const { getByRole, getByText } = render(<Sorter sortOrder="asc" handleSortChange={handleSortChange} />);

    fireEvent.mouseDown(getByRole('button'));
    fireEvent.click(getByText('Descending'));

    expect(handleSortChange).toHaveBeenCalled();
  });
});
