import { describe, it, expect, vi } from 'vitest';
import { render, fireEvent } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import CompletedProjects, { completedProjectsData } from './CompletedProjects';

describe('CompletedProjects Component', () => {
  it('renders completed projects', () => {
    const { getByText } = render(<MemoryRouter><CompletedProjects /></MemoryRouter>);
    completedProjectsData.slice(0, 7).forEach((project) => {
      expect(getByText(project.title)).toBeInTheDocument();
    });
  });

  it('navigates to next page of projects', () => {
    const { getByText } = render(<MemoryRouter><CompletedProjects /></MemoryRouter>);
    const nextPageButton = getByText('Next');
    fireEvent.click(nextPageButton);

    completedProjectsData.slice(7, 14).forEach((project) => {
      expect(getByText(project.title)).toBeInTheDocument();
    });
  });

  it('navigates to previous page of projects', () => {
    const { getByText } = render(<MemoryRouter><CompletedProjects /></MemoryRouter>);
    const nextPageButton = getByText('Next');
    fireEvent.click(nextPageButton);

    const prevPageButton = getByText('Previous');
    fireEvent.click(prevPageButton);

    completedProjectsData.slice(0, 7).forEach((project) => {
      expect(getByText(project.title)).toBeInTheDocument();
    });
  });
});
