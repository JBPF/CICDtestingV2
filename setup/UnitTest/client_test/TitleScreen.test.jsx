import { describe, it, expect, vi } from 'vitest';
import { render, fireEvent, screen } from '@testing-library/react';
import App from './App';


vi.mock('./data.json', () => ([
  { id: 1, name: 'Item 1' },
  { id: 2, name: 'Item 2' },
]));

describe('App Component', () => {
  it('renders items from data and handles actions', async () => {
    render(<App />);

    
    expect(await screen.findByText('Item 1')).toBeInTheDocument();
    expect(screen.getByText('Item 2')).toBeInTheDocument();

    
    fireEvent.click(screen.getAllByText(/Delete/i)[0]);

    
    expect(screen.queryByText('Item 1')).toBeNull();

    
    fireEvent.click(screen.getAllByText(/Accept/i)[0]);

    
    expect(screen.queryByText('Item 2')).toBeNull();
  });

  
});
