import { describe, it, expect, vi, beforeEach } from 'vitest';
import { render, fireEvent } from '@testing-library/react';
import { Select } from 'antd';
import RoleAssignmentForm from './RoleAssignmentForm';
import { fetchRoles, assignRoleToUser } from '../services/roleService';

vi.mock('../services/roleService', () => ({
  fetchRoles: vi.fn(),
  assignRoleToUser: vi.fn(),
}));

describe('RoleAssignmentForm Component', () => {
  const mockRoles = [
    { _id: '1', name: 'Role 1' },
    { _id: '2', name: 'Role 2' },
  ];
  const mockUserData = { _id: 'user1' };
  const mockCloseModal = vi.fn();

  beforeEach(() => {
    fetchRoles.mockResolvedValue(mockRoles);
    assignRoleToUser.mockResolvedValue({});
    vi.clearAllMocks();
  });

  it('fetches roles on component mount', async () => {
    render(<RoleAssignmentForm userData={mockUserData} closeModal={mockCloseModal} />);
    expect(fetchRoles).toHaveBeenCalled();
  });

  it('displays fetched roles in select options', async () => {
    const { findByText } = render(<RoleAssignmentForm userData={mockUserData} closeModal={mockCloseModal} />);
    const roleOption1 = await findByText('Role 1');
    const roleOption2 = await findByText('Role 2');
    expect(roleOption1).toBeInTheDocument();
    expect(roleOption2).toBeInTheDocument();
  });

  it('submits selected role and assigns it to user', async () => {
    const { getByRole, getByText } = render(<RoleAssignmentForm userData={mockUserData} closeModal={mockCloseModal} />);
    fireEvent.mouseDown(getByRole('combobox'));
    fireEvent.click(getByText('Role 1'));
    fireEvent.click(getByText('Assign new role'));
    expect(assignRoleToUser).toHaveBeenCalledWith({
      userId: mockUserData._id,
      roleId: mockRoles[0]._id,
    });
  });

  it('calls closeModal after successful role assignment', async () => {
    const { getByText } = render(<RoleAssignmentForm userData={mockUserData} closeModal={mockCloseModal} />);
    fireEvent.click(getByText('Assign new role'));
    expect(mockCloseModal).toHaveBeenCalled();
  });
});
