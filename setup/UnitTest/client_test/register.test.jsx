import { describe, it, expect, vi } from 'vitest';
import { render, fireEvent, screen } from '@testing-library/react';
import Register from './Register';
import * as userService from '../../services/userService';


vi.mock('../../services/userService', () => ({
  registerUser: vi.fn(),
}));

describe('Register Component', () => {
  it('renders form and handles registration', async () => {
    render(<Register />);

    
    expect(screen.getByLabelText(/Name/i)).toBeInTheDocument();
    expect(screen.getByLabelText(/Email address/i)).toBeInTheDocument();
    expect(screen.getByLabelText(/Password/i)).toBeInTheDocument();

    
    fireEvent.change(screen.getByLabelText(/Name/i), { target: { value: 'John Doe' } });
    fireEvent.change(screen.getByLabelText(/Email address/i), { target: { value: 'john.doe@example.com' } });
    fireEvent.change(screen.getByLabelText(/Password/i), { target: { value: 'password123' } });

    
    fireEvent.submit(screen.getByText(/Register/i));

    
    expect(userService.registerUser).toHaveBeenCalledWith({
      token: '', 
      name: 'John Doe',
      email: 'john.doe@example.com',
      password: 'password123',
    });

    
  });

  
});
