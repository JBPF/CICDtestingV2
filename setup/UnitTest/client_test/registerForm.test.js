// Register.test.jsx
import React from 'react';
import { describe, it, expect, vi, beforeEach } from 'vitest';
import { render, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Register from './Register'; 
import { registerUser } from '../../services/userService'; 
import { useParams, useNavigate } from 'react-router-dom';


vi.mock('../../services/userService', () => ({
  registerUser: vi.fn(),
}));
vi.mock('react-router-dom', () => ({
  useParams: vi.fn(),
  useNavigate: vi.fn(),
}));

describe('Register Component', () => {
  beforeEach(() => {
    
    vi.resetAllMocks();
    useParams.mockReturnValue({ token: 'dummy-token' });
    useNavigate.mockReturnValue(vi.fn());
  });

  it('submits the form and registers a user', async () => {
    
    registerUser.mockResolvedValue({});
    
    
    const { getByLabelText, getByRole } = render(<Register />);

    
    await userEvent.type(getByLabelText(/name/i), 'John Doe');
    await userEvent.type(getByLabelText(/password/i), 'password123');

    
    fireEvent.submit(getByRole('button'));

    
    expect(registerUser).toHaveBeenCalledWith({
      token: 'dummy-token',
      name: 'John Doe',
      password: 'password123',
    });

    
    expect(useNavigate()).toHaveBeenCalledWith('/login');
  });

  // 可以在这里添加更多测试用例
});
