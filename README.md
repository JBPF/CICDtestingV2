# Main



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://stgit.dcs.gla.ac.uk/team-project-h/2023/sh14/sh14-main.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://stgit.dcs.gla.ac.uk/team-project-h/2023/sh14/sh14-main/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***



## Name
Literature review platform

## Description
This is a literature review web app. The app allows project managers to upload articles as PDFs, and reviewers can move through the process, rejecting and accepting articles at each stage depending on if the articles meet the inclusion and exclusion criteria set by a project manager.  

Once they have a set of articles which meet the criteria, reviewers can start the data extraction process. This is where reviewers read the article and extract the required data for the fields set by the project manager. For example, the project manager may want to know the number of participants in the study. This has been made easier by a highlight function in which users can select relevant text in the article. Then a drop down menu of potential fields come up and the user can select the correct one. For example, the user highlights the number 50, then a drop down comes up with options like age of participants, number of participants, concentration of vitamin C.
Once the data has been extracted by at least 2 reviewers, the project manager can export this data to a handy CSV file, which lists the data which has been extracted.

After the review process, a PRISMA is available to view, which displays number of articles accepted/ rejected at each stage. 




## Installation
1. Make sure you have the following software installed on your machiine:

- [Node.js](https://nodejs.org/)
- [npm](https://www.npmjs.com/) or [Yarn](https://yarnpkg.com/)

2. Clone the repository:
git clone https://stgit.dcs.gla.ac.uk/team-project-h/2023/sh14/sh14-main.git


3. Navigate to the directory
cd sh14-main

4. Install dependencies for both client and server
cd setup/client
npm install
cd ../server
npm install




## Usage
1. Navigate to the client 
cd setup/client

2. Start the client
npm start

3. Navigate to the server in another terminal 
cd setup/server

4. Start the server
npm start

## Folder Structure
sh14-main/
|-- setup/
|   |-- client/         # React frontend
|   |-- server/         # Express.js backend
|-- public/             # Public assets
|-- .env                # Environment variables
|-- package.json        # Node.js dependencies and scripts
|-- README.md           # Project documentation

## Technologies used
MongoDB
Express.js
React
Node.js

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
